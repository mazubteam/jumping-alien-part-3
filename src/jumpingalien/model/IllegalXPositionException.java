package jumpingalien.model;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * 
 * @author Lemmens U. & Schuermans J.
 * @version 1.0
 * 
 * A class for signaling illegal x-positions for WorldObjects.
 */
@SuppressWarnings("serial")
public class IllegalXPositionException extends Exception {

	/**
	 * Initialize this new illegal position exception with the given position
	 * @param 	xPos
	 * 			the position for this illegal x-position exception
	 */
	public IllegalXPositionException(double xPos) {
		this.xPosition = xPos;
	}
	
	/**
	 * Returns the x-position registered for this illegal x-position exception.
	 */
	@Basic @Immutable
	public double getXPosition() {
		return this.xPosition;
	}
	
	private final double xPosition;
	
}
