package jumpingalien.program.statements;

import jumpingalien.model.BreakException;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
import jumpingalien.program.expressions.Boolean;
import jumpingalien.program.expressions.Expression;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of IfThenElse extending the abstract type Statement.
 * 
 */

public class IfThenElse extends Statement {

	public IfThenElse(Statement body1, Statement body2, Expression condition, SourceLocation sourceLocation) {
		super(sourceLocation);
		this.body1 = body1;
		this.body2 = body2;
		this.condition = condition;
	}

	public Statement getBody1() {
		return body1;
	}
	
	public Statement getBody2() {
		return body2;
	}

	public Expression getCondition() {
		return condition;
	}

	private final Statement body1;
	private final Statement body2;
	private final Expression condition;
	
	@Override
	public double executeStatement(Program program, double time) throws BreakException {
		if  (((Boolean) (getCondition().evaluate(program))).getBooleanValue() == true) {
			time = getBody1().executeStatement(program, time);
			if (! getBody1().isFinished()) {
				return 0.0;
			}
		}
		else {
				if (getBody2() != null) {
					time = body2.executeStatement(program, time);
				if (! getBody2().isFinished()) {
					return 0.0;
				}
			}
		}
		this.setFinished(true);
		time -= 0.001;
		return time;
	}

	@Override
	public void reset() {
		setFinished(false);
		getBody1().reset();
		if (getBody2() != null) {
			getBody2().reset();
		}
	}

}
