package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of IsMagmas extending the abstract class UnaryOperator.
*/
public class IsMagma extends UnaryOperator {

	public IsMagma(Expression expr, SourceLocation location) {
		super(expr, location);
	}
	
	@Override
	public Object evaluate(Program program) {		
		return new Boolean( ((Boolean) (new IsTerrain(getOperand(), null)).evaluate(program)).getBooleanValue() && 3==((jumpingalien.model.Tile) (Object) getOperand()).getFeature(),null);
	}

}
