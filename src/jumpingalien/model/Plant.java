package jumpingalien.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.util.Sprite;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Plants extending the abstract type WorldObject.
 * 
 * @invar	...
 * 			| isValidVerticalVelocity(getVerticalVelocity())
 * @invar	...
 * 			| isValidHorizontalVelocity(getHorizontalVelocity())
 * @invar	...
 * 			| isValidVerticalAcceleration(getVerticalAcceleration())
 * @invar	...
 * 			| isValidHorizontalAcceleration(getHorizontalAcceleration())
 */
public class Plant extends WorldObject {
	
	/**
	 * @param 	sprite
	 * 			...
	 * @effect 	...
	 * 			| setNbHitPoints(1)
	 * @effect 	...
	 * 			| setImages(sprite)
	 * @effect	...
	 * 			| moveLeft()
	 */
	public Plant(Sprite[] sprite) {
		this.setNbHitPoints(1);
		this.setImages(sprite);
		moveLeft();
	}
	
	/**
	 * @effect 	...
	 * 			| setOrientation(Orientation.LEFT)
	 * @effect 	...
	 * 			| setHorizontalVelocity(0.5)
	 * @effect	...
	 * 			| setSpriteIndex(0)
	 */
	void moveLeft() {
		setOrientation(Orientation.LEFT);
		setHorizontalVelocity(0.5);
		setSpriteIndex(0);
	}
	
	/**
	 * @effect 	...
	 * 			| setOrientation(Orientation.RIGHT)
	 * @effect 	...
	 * 			| setHorizontalVelocity(0.5)
	 * @effect	...
	 * 			| setSpriteIndex(1)
	 * 
	 */
	void moveRight() {
		setOrientation(Orientation.RIGHT);
		setHorizontalVelocity(0.5);
		setSpriteIndex(1);
	}
	
	@Override
	/**
	 * @param	dt
	 * 			...
	 * @effect 	...
	 * 			| updateXPosition(dt)
	 * @effect	...
	 * 			| if (timer >= 0.5) 
	 *			|	if (getOrientation() == Orientation.RIGHT) 
	 *			|		then moveLeft();
	 *			|			 timer = 0.0;
	 *			|	else
	 *			|		then moveRight();
	 *			|			 timer = 0.0;
	 *
	 */
	public void advanceTime(double dt) throws IllegalXPositionException {
		updateXPosition(dt);
		
		if (! hasProgram()) {
			setTimer(getTimer() + dt);
			
			if (getTimer() >= 0.5) {
				if (getOrientation() == Orientation.RIGHT) {
					moveLeft();
					setTimer(0.0);
				}
				else {
					moveRight();
					setTimer(0.0);
				}
			}
		}
		
	}
	
	/**
	 * @return	...
	 * 			| timer
	 */
	@Basic
	private double getTimer() {
		return timer;
	}
	
	/**
	 * @param 	t
	 * 			...
	 * @post	...
	 * 			| new.getTimer() == t
	 */
	private void setTimer(double t) {
		timer = t;
	}
	private double timer;

	/**
	 *
	 * @param 	deltaT
	 * 			...
	 * @throws 	IllegalXPositionException
	 * 			...
	 * 			| if (! isValidXPosition(getXPositionDouble() + distanceTravelled))
	 * @effect 	...
	 * 			| if ((getOrientation() == Orientation.RIGHT))
	 * 			| 	then setXPosition(getXPositionDouble() + 100 * getHorizDistTravelled(deltaT))
	 * @effect	...
	 * 			| if ((getOrientation() == Orientation.LEFT))
	 * 			| 	then setXPosition(getXPositionDouble() - 100 * getHorizDistTravelled(deltaT))
	 */
	private void updateXPosition(double deltaT) throws IllegalXPositionException {

		if (getOrientation() == Orientation.RIGHT) {
			double distanceTravelled = (100 * getDistTravelled(deltaT));
			if (! featureOverlapsRight(1) ) {
				setXPosition(getXPositionDouble() + distanceTravelled);
			}
		}
		if (getOrientation() == Orientation.LEFT) {
			double distanceTravelled = - 100 * getDistTravelled(deltaT);
			if (!featureOverlapsLeft(1) ) {
				setXPosition( getXPositionDouble() + distanceTravelled);
			}
		}	
	}
	
	/**
	 * @param 	deltaT
	 * 			...
	 * @return	...
	 * 			| getHorizontalVelocity() * deltaT
	 */
	private double getDistTravelled(double deltaT) { 
		return (getHorizontalVelocity() * deltaT);
	}

	/**
	 * @param 	velocity
	 * 			...
	 * @return 	...
	 * 			| velocity == 0.5
	 */
	@Override
	protected boolean isValidHorizontalVelocity(double velocity) {
		if (velocity == Double.POSITIVE_INFINITY || velocity == Double.NEGATIVE_INFINITY || velocity == Double.NaN) {
			 return false;
		}
		return velocity == 0.5;
	}

	/**
	 * @param 	velocity
	 * 			...
	 * @return 	...
	 * 			| velocity == 0
	 */
	@Override
	protected boolean isValidVerticalVelocity(double velocity) {
		if (velocity == Double.POSITIVE_INFINITY || velocity == Double.NEGATIVE_INFINITY || velocity == Double.NaN) {
			 return false;
		}
		return velocity == 0;
	}

	/**
	 * @param 	acc
	 * 			...
	 * @return 	...
	 * 			| acc == 0
	 */
	@Override
	protected boolean isValidHorizontalAcceleration(double acc) {
		if (acc == Double.POSITIVE_INFINITY || acc == Double.NEGATIVE_INFINITY || acc == Double.NaN) {
			 return false;
		}
		return acc == 0;
	}
	
	/**
	 * @param 	acc
	 * 			...
	 * @return 	...
	 * 			| acc == 0
	 */
	@Override
	protected boolean isValidVerticalAcceleration(double acc) {
		if (acc == Double.POSITIVE_INFINITY || acc == Double.NEGATIVE_INFINITY || acc == Double.NaN) {
			 return false;
		}
		return acc == 0;
	}

	/**
	 * @return	...
	 * 			| maxNbHitPoints
	 */
	@Override
	@Basic
	@Immutable
	protected int getMaxNbHitPoints() {
		return maxNbHitPoints;
	}
	private static final int maxNbHitPoints = 1;
	
	
	void endMove() {
		setHorizontalVelocity(0);
	}
	
}
