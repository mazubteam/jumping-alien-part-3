package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/** 
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of GreaterThan's extending the abstract class BinaryOperator.
*/
public class GreaterThan extends BinaryOperator {

	public GreaterThan(Expression left, Expression right, SourceLocation location) {
		super(left, right, location);
	}

	@Override
	public Expression evaluate(Program program) {
		return new Boolean( ((DoubleConstant) getLeftOperand().evaluate(program)).getValue() > ( (DoubleConstant) getRightOperand().evaluate(program)).getValue(),null);
	}

}
