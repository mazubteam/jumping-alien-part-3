package jumpingalien.program.statements;

import jumpingalien.model.Alien;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of StartDuck extending the abstract type Statement.
 * 
 */

public class StartDuck extends Statement {

	public StartDuck(SourceLocation loc) {
		super(loc);
	}

	@Override
	public double executeStatement(Program program, double time) {
		if (program.getWorldObject() instanceof Alien) { 
			try {
				((Alien) program.getWorldObject()).startDuck();
			} catch (Exception e) {
				//
			}
		}
		this.setFinished(true);
		return time - 0.001;
	}

	@Override
	public void reset() {
		setFinished(false);		
	}

}
