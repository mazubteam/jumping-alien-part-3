package jumpingalien.model;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * 
 * @author Lemmens U. & Schuermans J.
 * @version 1.0
 * 
 * A class for signaling illegal x-positions for WorldObjects.
 */
@SuppressWarnings("serial")
public class IllegalYPositionException extends Exception {

	/**
	 * Initialize this new illegal position exception with the given position
	 * @param 	yPos
	 * 			the position for this illegal y-position exception
	 */
	public IllegalYPositionException(double yPos) {
		this.yPosition = yPos;
	}
	
	/**
	 * Returns the y-position registered for this illegal y-position exception.
	 */
	@Basic @Immutable
	public double getYPosition() {
		return this.yPosition;
	}
	
	private final double yPosition;
	
}
