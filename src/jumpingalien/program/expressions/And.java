package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/** 
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of Ands extending the abstract class BinaryOperator.
*/
public class And extends BinaryOperator {

	public And(Expression left, Expression right, SourceLocation location) {
		super(left, right, location);
	}

	@Override
	public Expression evaluate(Program program) {
		if ((getRightOperand() instanceof BinaryOperator) && ((BinaryOperator) getRightOperand()).getLeftOperand() instanceof Variable ) {
		}
		return new Boolean((((Boolean) getLeftOperand().evaluate(program)).getBooleanValue() && ((Boolean) getRightOperand().evaluate(program)).getBooleanValue()),null);
	}


}
