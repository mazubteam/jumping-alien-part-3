package jumpingalien.program.types;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of ObjType extending the abstract type MyType.
 * 
 */

public class ObjType extends MyType {

	public ObjType(Object o) {
		setObject(o);
	}
	
	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	private Object object;
}
