package jumpingalien.program.statements;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jumpingalien.model.BreakException;
import jumpingalien.model.Buzam;
import jumpingalien.model.Mazub;
import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.IProgramFactory.Kind;
import jumpingalien.part3.programs.IProgramFactory.SortDirection;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
import jumpingalien.program.expressions.Expression;
import jumpingalien.program.expressions.Boolean;
import jumpingalien.program.expressions.DoubleConstant;



/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of ForEach extending the abstract type Statement.
 * 
 */

public class ForEach extends Statement {
	
	
	public ForEach(String variableName,
			jumpingalien.part3.programs.IProgramFactory.Kind variableKind,
			Expression where,
			Expression sort,
			jumpingalien.part3.programs.IProgramFactory.SortDirection sortDirection,
			Statement body, SourceLocation loc) {
		super(loc);
		this.sortDirection = sortDirection;
		this.where = where;
		this.sortingExpression = sort;
		this.variableName = variableName;
		this.variableKind = variableKind;
		this.body = body;
	}

	private SortDirection getSortDirection() {
		return sortDirection;
	}
	private final SortDirection sortDirection;
	
	private Expression getWhere() {
		return where;
	}
	private final Expression where;
	
	private Expression getSortingExpression() {
		return this.sortingExpression;
	}
	private final Expression sortingExpression;
	
	private String getVariableName() {
		return variableName;
	}
	private final String variableName;

	private Kind getVariableKind() {
		return variableKind;
	}
	private final Kind variableKind;
	
	Statement getBody() {
		return body;
	}
	private final Statement body;

	private boolean checkWhere(Object obj, Program program) {
		program.getGlobalVars().get(getVariableName()).setValue(obj);
		return (((Boolean) getWhere().evaluate(program)).getBooleanValue());
	}
	
	private double getSortingValue(Object obj, Program program) {
		program.getGlobalVars().get(getVariableName()).setValue(obj);
		return ((DoubleConstant) getSortingExpression().evaluate(program) ).getValue();
	}
	
	@Override
	public double executeStatement(Program program, double time) {
		Stream<?> originalStream;
		if (getVariableKind() == Kind.PLANT) {
			originalStream = program.getWorldObject().getWorld().getPlants().stream();
		}
		else if (getVariableKind() == Kind.MAZUB) {
			ArrayList<Mazub> mazubList = new ArrayList<Mazub>();
			mazubList.add(program.getWorldObject().getWorld().getMazub());
			originalStream = mazubList.stream();
		}
		else if (getVariableKind() == Kind.BUZAM) {
			ArrayList<Buzam> buzamList = new ArrayList<Buzam>();
			buzamList.add(program.getWorldObject().getWorld().getBuzam());
			originalStream = buzamList.stream();
		}
		else if (getVariableKind() == Kind.SLIME) {
			originalStream = program.getWorldObject().getWorld().getSlimes().stream();
		}
		else if (getVariableKind() == Kind.SHARK) {
			originalStream = program.getWorldObject().getWorld().getSharks().stream();
		}
		else if (getVariableKind() == Kind.ANY) {
			List<WorldObject> newList = new ArrayList<WorldObject>(program.getWorldObject().getWorld().getAllWorldObjects());
			newList.addAll(program.getWorldObject().getWorld().getTiles());
			originalStream =newList.stream();
		}
		else {
			originalStream = program.getWorldObject().getWorld().getTiles().stream();
		}
		
		@SuppressWarnings("resource")
		Stream<?> filteredStream = originalStream.filter(obj->checkWhere(obj, program));
		Stream<?> sortedStream;
		
		if (getSortingExpression() != null) {
		
			if (getSortDirection() == jumpingalien.part3.programs.IProgramFactory.SortDirection.ASCENDING) {
			sortedStream = filteredStream.sorted( (obj1,obj2) -> Double.compare(getSortingValue(obj1, program), getSortingValue(obj2, program) ));	
			}
			else {
			sortedStream = filteredStream.sorted( (obj1,obj2) -> Double.compare(getSortingValue(obj2, program), getSortingValue(obj1, program) ));
			}
		} else sortedStream = filteredStream;
		
		ArrayList<?> objList =  (ArrayList<?>) sortedStream.collect(Collectors.toList());
				
		for	(int i = 0; i<objList.size(); i++) {			
			if ( getNextIndex() != 0) {
				i = getNextIndex();
				setNextIndex(0);
			}
			Object obj = objList.get(i);
			if (time >= 0.001) {
				
				program.getGlobalVars().get(getVariableName()).setValue(obj);
				try {
					time = getBody().executeStatement(program, time);
				} catch (BreakException e) {
					break;
				}
				if(! getBody().isFinished()) {
					this.setFinished(false);
					setNextIndex(i);
					return time;
				}
			}
			else {
				this.setFinished(false);
				setNextIndex(i);
				return time;
			}
		}
		this.setFinished(true);
		return time-0.001;
	}

	@Override
	public void reset() {
		setFinished(false);
		getBody().reset();
	}
	
	private int getNextIndex() {
		return nextIndex;
	}

	private void setNextIndex(int nextIndex) {
		this.nextIndex = nextIndex;
	}
	private int nextIndex = 0;


}
