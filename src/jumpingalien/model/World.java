package jumpingalien.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of Worlds involving dimensions, tiles, WorldObjects, ...
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 * 
 * @version 1.0
 * 
 * @invar	...
 * 			| hasProperWorldObjects()
 * @invar 	...
 * 			| getNbAliens() == 0 || getNbAliens() == 1
 * @invar	...
 * 			| for each x in { 0 : getXDimension()/getTileLength() }
 * 			| 	for each y in { 0 : getYDimension()/getTileLength() }
 * 			| 		isValidTile(x,y)
 */
public class World {

	public World() {
	}
	
	// Dimensions, Pixels, Tiles & visibleWindow
	/**
	 * @return	...
	 * 			| XDimension
	 */
	@Basic
	public int getXDimension() {
		return this.XDimension;
	}
	private int XDimension;
	
	/**
	 * @return	...
	 * 			| YDimension
	 */
	@Basic
	public int getYDimension() {
		return this.YDimension;
	}
	private int YDimension;

	/**
	 * @param 	xDim
	 * 			...
	 * @post	...
	 * 			| new.getXDimension() == xDim
	 */
	public void setXDimension(int xDim) {
		XDimension = xDim;
	}
	
	/**
	 * @param 	yDim
	 * 			...
	 * @post	...
	 * 			| new.getYDimension() == yDim
	 */
	public void setYDimension(int yDim) {
		YDimension = yDim;
	}
	
	/**
	 * @return	...
	 * 			| if (getAlien().getXPosition() > 200)
	 * 			|	then getAlien().getXPosition() - windowPositionBorder;
	 * 			| else 0
	 */
	public int getWindowLeft() {
		try {
			if (getMazub().getXPosition() > 200) {
				return getMazub().getXPosition() - windowPositionBorder; 
			}
			else return 0;
		} catch (Exception e) {
			return 0;
		}
	}
	
	/**
	 * @return	...
	 * 			| if (getAlien().getYPosition() > 200)
	 * 			|	then getAlien().getYPosition() - windowPositionBorder;
	 * 			| else 0
	 */
	public int getWindowBottom() {
		try {
			if (getMazub().getYPosition() > 200) {
			return getMazub().getYPosition() - windowPositionBorder;
			}
			else return 0;
		} catch (Exception e) {
			return 0;
		}
	}
	private static final int windowPositionBorder = 200;
	
	/**
	 * @return	...
	 * 			| tileLength
	 */
	@Basic
	@Immutable
	public int getTileLength() {
		return tileLength;
	}
	private static int tileLength;
	
	/** 
	 * @param 	length
	 * 			...
	 * @post	...
	 * 			| new.getTileLength() == length
	 */
	public void setTileLength(int length) {
		tileLength = length;
	}
	
	/**
	 * @param 	pixelX
	 * 			...
	 * @param 	pixelY
	 * 			...
	 * @return	...
	 * 			| getFeatureAtTile(list[0], list[1])
	 */
	public int getFeatureAtPixel(int pixelX, int pixelY) {
		int[] list = getTile(pixelX,pixelY);
		return getFeatureAtTile(list[0], list[1]);
	}
	
	/**
	 * 
	 * @param 	tileX
	 * 			...
	 * @param 	tileY
	 * 			...
	 * @return	...
	 * 			| getFeatureMatrix()[tileX][tileY].getFeature()
	 * @throws 	IllegalArgumentException
	 * 			never
	 * 			| if (! isValidTile(tileX, tileY))
	 */
	private int getFeatureAtTile(int tileX, int tileY) throws IllegalArgumentException {
		if (isValidTile(tileX, tileY)) {
			return getFeatureMatrix()[tileX][tileY].getFeature();
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * @return	...
	 * 			| tileFeatureMatrix.clone()
	 */
	@Basic
	private Tile[][] getFeatureMatrix() {
		return tileFeatureMatrix.clone();
	}
	private Tile[][] tileFeatureMatrix;
	
	/**
	 * 
	 * @param 	x
	 * 			...
	 * @param 	y
	 * 			...
	 * @param 	val
	 * 			...
	 * @post	...
	 * 			| if (isValidTile(x,y) && val >= 0 && val <= 3) 
	 * 			| 	then new.getFeatureMatrix()[x][y].setFeature(val)
	 */
	private void setFeatureMatrix(int x, int y, int val) {
		if (isValidTile(x,y) && val >= 0 && val <= 3) {
			tileFeatureMatrix[x][y].setFeature(val);
		}
	}
	
	/**
	 * 
	 * @param 	nbTilesX
	 * 			...
	 * @param 	nbTilesY
	 * 			...
	 * @post	...
	 * 			| new.getFeatureMatrix() = Tile[nbTilesX][nbTilesY]
	 */
	public void createFeatureMatrix(int nbTilesX, int nbTilesY) {
		tileFeatureMatrix = new Tile[nbTilesX][nbTilesY];
		
		for (int i = 0 ; i < nbTilesX;i++) {
			for (int j = 0 ; j < nbTilesY;j++) {
				tileFeatureMatrix[i][j] = new Tile(0,i,j);
			}
		}
	}
	
	/**
	 * @param tileX
	 * 			...
	 * @param tileY
	 * 			...
	 * @param value
	 * 			...
	 * @effect	...
	 * 			| setFeatureMatrix(tileX, tileY, value)
	 * @throws 	IllegalArgumentException
	 * 			| if !((value <= 3) && (value >= 0) && isValidTile(tileX, tileY))
	 */
	public void setFeatureAtTile(int tileX, int tileY, int value) throws IllegalArgumentException {
		if ((value <= 3) && (value >= 0) && isValidTile(tileX, tileY)) {
			setFeatureMatrix(tileX, tileY, value);
		}
		else throw new IllegalArgumentException();
	}
	
	/**
	 * @param tileX
	 * 			...
	 * @param tileY
	 * 			...
	 * @return 	...
	 * 			| ((tileX < getXDimension() / getTileLength()) && (tileX >= 0) && (tileY < getYDimension() / getTileLength()) && (tileY >= 0))
	 */
	private boolean isValidTile(int tileX, int tileY) {
		return ( (tileX < getXDimension() / getTileLength()) && (tileX >= 0) && (tileY < getYDimension() / getTileLength()) && (tileY >= 0) );
	}
	
	/**
	 * @param pixelX
	 * 			...
	 * @param pixelY
	 * 			...
	 * @return	...
	 * 			| { (pixelX) / getTileLength(), (pixelY) / getTileLength() }
	 */
	public int[] getTile(int pixelX, int pixelY) {
		return new int[] { (pixelX) / getTileLength(), (pixelY) / getTileLength() }; 
	}
	
	public Tile getTileAtPixel(int pixelX, int pixelY) {
		return getFeatureMatrix()[getTile(pixelX, pixelY)[0]][getTile(pixelX, pixelY)[1]];
	}
	
	/**
	 * @param tileX
	 * 			...
	 * @param tileY
	 * 			...
	 * @return	...
	 * 			| {tileX * getTileLength() , tileY * getTileLength()}
	 */
	public int[] getBottomLeftPixelOfTile(int tileX, int tileY) {
		return new int[] {tileX * getTileLength() , tileY * getTileLength()};
	}
	
	/**
	 * @param pixelLeft
	 * 			...
	 * @param pixelBottom
	 * 			...
	 * @param pixelRight
	 * 			...
	 * @param pixelTop
	 * 			...
	 * @return	...
	 * 			| tilePositionsMatrix
	 */
	public int[][] getTilePositionsIn(int pixelLeft, int pixelBottom, int pixelRight, int pixelTop) {
		int left = getTile(pixelBottom,pixelLeft)[0];
		int bottom = getTile(pixelBottom,pixelLeft)[1];
		int right = getTile(pixelTop,pixelRight)[0];
		int top = getTile(pixelTop,pixelRight)[1];
		
		int[][] tilePositionsMatrix = new int[(right-left+1)*(top-bottom+1)][2];
		int pos = 0;
		for(int j = left; j <= right; j++) {
			for (int i = bottom; i <= top; i++) {
				tilePositionsMatrix[pos][1] = j;
				tilePositionsMatrix[pos][0] = i;
				pos += 1;
			}
		}
		return tilePositionsMatrix;
	}
	
	/**
	 * @return	...
	 * 			| targetX		
	 */
	@Basic
	public int getTargetTileX() {
		return targetX;
	}
	private int targetX;
	
	/**
	 * @return	...
	 * 			| targetY		
	 */
	@Basic
	public int getTargetTileY() {
		return targetY;
	}
	private int targetY;
	
	/**
	 * @param 	x
	 * 			...
	 * @param 	y
	 * 			...
	 * @post	...
	 * 			| new.targetX = x;
	 * @post	...
	 * 			| new.targetY = y;
	 */
	public void setTargetTile(int x, int y) {
		if (isValidTile(x,y)) {
			targetX = x;
			targetY = y;
		}
	}
	
	/**
	 * @return	...
	 * 			| visibleWindowWidth
	 */
	public int getVisibleWindowWidth() {
		return visibleWindowWidth;
	}
	private int visibleWindowWidth;
	
	/**
	 * @post	...
	 * 			| new.getVisibleWindowWidth() == width;
	 */
	public void setVisibleWindowWidth(int width) {
		 this.visibleWindowWidth = width;
	}

	/**
	 * @return	...
	 * 			| visibleWindowHeight
	 */
	@Basic
	public int getVisibleWindowHeight() {
		return this.visibleWindowHeight;
	}
	private int visibleWindowHeight;
	
	/**
	 * @post	...
	 * 			| new.getVisibleWindowHeight() == height;
	 */
	public void setVisibleWindowHeight(int height) {
		 this.visibleWindowHeight = height;
	}

	// advanceTime
	/**
	 * @param 	deltaT
	 * 			...
	 * @effect 	...
	 * 			| advanceObject(deltaT, getAlien())
	 * @effect	...
	 * 			| for each obj in getAllWorldObjects()
	 * 			| 	if (!(obj instanceof Alien) )
	 * 			|		then advanceObject(deltaT, obj)
	 */
	public void advanceTime(double deltaT) {
		advanceObject(deltaT, getMazub());
		
		for (WorldObject obj: getAllWorldObjects()) {
			if (!(obj instanceof Mazub) && (!obj.hasProgram())) {
//				System.out.println("dit obj heeft GEEN programma:");
//				System.out.println(obj);
				advanceObject(deltaT, obj);
			}
			else if (obj.hasProgram()) {
//				System.out.println("dit obj heeft een programma:");
//				System.out.println(obj);
				obj.getProgram().run(deltaT);
				advanceObject(deltaT, obj);
			}
		}
	}

	private void advanceObject(double deltaT, WorldObject obj) {

		try {
			if ((obj != null)) {
				
				double time = 0;
				try {
					double dt = obj.getDt(deltaT);
					obj.setObjOverlapTimer(obj.getObjOverlapTimer() + dt);
					while (time < deltaT && !obj.isTerminated() ) {
						collidesWithObjects(obj);
						collidesWithMagmaOrWater(obj, dt);
						obj.advanceTime(dt);
						time += dt;
					}
				} catch (NotMovingException e) {
					if ( ! obj.isTerminated()) {
						obj.advanceTime(deltaT);
						obj.setObjOverlapTimer(obj.getObjOverlapTimer() + deltaT);
						collidesWithObjects(obj);
						collidesWithMagmaOrWater(obj, deltaT);
					}
				}
			}
		}
		catch (IllegalXPositionException | IllegalYPositionException e) {
			if (! obj.isTerminated()) {
				obj.terminate();
			}
		}
	}
	
	// 2 subfuncties van advanceObject()
	/**
	 * @param 	obj
	 * 			...
	 * @param 	dt
	 * 			...
	 * @effect 	...
	 * 			| if (!obj.isTerminated() && obj.objectOverlapsWithMagma(obj))
	 * 			|	if (obj.getTimeInMagma() == 0.0)
	 * 			|		then obj.addHP(-50); 
	 * 			|		if (obj instanceof Slime)
	 * 			|			then updateSchoolAfterSlimeDamage(obj);
	 * 			|		obj.zeroHpKills();
	 * @effect  ...
	 * 			| if (obj.getTimeInMagma() >= 0.2)
	 * 			|	then obj.setTimeInMagma(0.0)
	 * @effect	...
	 * 			| if (obj.isTerminated() && ! (obj.objectOverlapsWithMagma(obj)))
	 * 			| 	then obj.setTimeInMagma(0.0)
	 * @effect	...
	 * 			| if (!obj.isTerminated() && obj.objectOverlapsWithWater(obj))
	 * 			|	if (obj.getTimeInWater() >= 0.2 && !(obj instanceof Shark))
	 * 			|		then obj.addHP(-2); 
	 * 			|		if (obj instanceof Slime)
	 * 			|			then updateSchoolAfterSlimeDamage(obj);
	 * 			|		obj.zeroHpKills();
	 * 			|		obj.setTimeInWater(0.0)
	 */
	private void collidesWithMagmaOrWater(WorldObject obj, double dt) {
		if (!obj.isTerminated() && obj.objectOverlapsWithMagma(obj)) {
			if (obj.getTimeInMagma() == 0.0) {
				obj.addHP(-50);
				if (obj instanceof Slime) {
					updateSchoolAfterSlimeDamage(obj);
				}
				obj.zeroHpKills();
			}
			obj.updateTimeInMagma(dt);
			if (obj.getTimeInMagma() >= 0.2) {
				obj.setTimeInMagma(0.0);
			}
		}
		else obj.setTimeInMagma(0.0);
		
		if (!obj.isTerminated() && obj.objectOverlapsWithWater(obj)) {
			obj.updateTimeInWater(dt);
			if (obj.getTimeInWater() >= 0.2 && !(obj instanceof Shark) ) {
				obj.addHP(-2);
				if (obj instanceof Slime) {
					updateSchoolAfterSlimeDamage(obj);
				}
				obj.zeroHpKills();
				obj.setTimeInWater(0.0);
			}
		}
	}

	/**
	 * @param 	obj
	 * 			...
	 * @effect	...	
	 * 			| if ((overlappingObject instanceof Plant && obj instanceof Alien) || (obj instanceof Plant && overlappingObject instanceof Alien)) 
	 * 			|	then mazubPlantCollision(obj);
	 * @effect	...	
	 * 			| if (overlappingObject instanceof Slime && obj instanceof Slime)
	 * 			|	then slimeSlimeCollision(obj);
	 * @effect	...	
	 * 			| if (overlappingObject != null && !(overlappingObject instanceof Plant) && !(obj instanceof Plant))
	 * 			|	then damageOverlappingObjects(obj)
	 * 			|	if (obj.objectOverlapsBottom(overlappingObject))
	 * 			|		then speedUpdateOverlapObjBottom(obj)
	 * 			|	else if (obj.objectOverlapsTop(overlappingObject))
	 * 			|		then SpeedUpdateOverlapObjTop(obj)
	 * 			|	else if (obj.objectOverlapsRight(overlappingObject) || obj.objectOverlapsLeft(overlappingObject))
	 * 			|		then speedUpdateOverlapObjRightLeft(obj)
	 * @post	...	
	 * 			| if ((obj instanceof Alien))
	 * 			|	then  !((new)((Alien) obj)).objectOverlapsBottom
	 */
	private void collidesWithObjects(WorldObject obj) {
		overlappingObject = obj.getOverlappingWorldObject();
		
		if ((overlappingObject instanceof Plant && obj instanceof Alien) || (obj instanceof Plant && overlappingObject instanceof Alien)) {
			alienPlantCollision(obj);
		}
		else if (overlappingObject instanceof Slime && obj instanceof Slime) {
			slimeSlimeCollision(obj);
		}
		else if (overlappingObject != null && !(overlappingObject instanceof Plant) && !(obj instanceof Plant)) {
			
			damageOverlappingObjects(obj);

			if (obj.objectOverlapsBottom(overlappingObject)) {			
				speedUpdateOverlapObjBottom(obj);
			}
			else if (obj.objectOverlapsTop(overlappingObject)) {			
				speedUpdateOverlapObjTop(obj);
			}
			else if (obj.objectOverlapsRight(overlappingObject) || obj.objectOverlapsLeft(overlappingObject)) {
				speedUpdateOverlapObjRightLeft(obj);
			}	
		}
		else if ((obj instanceof Alien)) {
			((Alien) obj).objectOverlapsBottom = false;
		}
	}
	
	// 5 Subfuncties van collidesWithObjects(WorldObject obj)
	/**
	 * @param 	obj
	 * 			...
	 * @effect	...
	 * 			| overlappingObject.addHP(-1)
	 * @effect	...	
	 * 			| overlappingObject.zeroHpKills()
	 * @effect	...	
	 * 			| obj.addHP(50)
	 */
	private void alienPlantCollision(WorldObject obj) {
		overlappingObject.addHP(-1);
		overlappingObject.zeroHpKills();
		obj.addHP(50);
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @effect	...	
	 * 			| if (! ((Slime) obj).getSchool().equals(((Slime)overlappingObject).getSchool()))
	 * 			|	if (((Slime) obj).getSchool().getNbSlimes() < (((Slime)overlappingObject).getSchool()).getNbSlimes())
	 * 			|		then ((Slime) obj).getSchool().addHpOldSchool((Slime)obj)
	 * 			|			 ((Slime) obj).changeToSchool(((Slime)overlappingObject).getSchool())
	 * 			|			 ((Slime) obj).getSchool().addHpNewSchool((Slime)obj);
	 * 			|	else if (((Slime) obj).getSchool().getNbSlimes() > (((Slime)overlappingObject).getSchool()).getNbSlimes())
	 * 			|		then ((Slime) overlappingObject).getSchool().addHpOldSchool((Slime)overlappingObject)
	 * 			|			 ((Slime) overlappingObject).changeToSchool(((Slime)obj).getSchool())
	 * 			|			 ((Slime) overlappingObject).getSchool().addHpNewSchool((Slime)overlappingObject)
	 * @effect	...	
	 * 			| if (obj.objectOverlapsRight(overlappingObject))
	 * 			|	then 	obj.setSpriteIndex(0)
	 * 			|			overlappingObject.setSpriteIndex(1)
	 * 			|		 	obj.setOrientation(Orientation.LEFT)
	 * 			|			overlappingObject.setOrientation(Orientation.RIGHT)
	 * @effect	...	
	 * 			| if ! (obj.objectOverlapsRight(overlappingObject))
	 * 			|	then 	obj.setSpriteIndex(1)
	 * 			|			overlappingObject.setSpriteIndex(0)
	 * 			|		 	obj.setOrientation(Orientation.RIGHT)
	 * 			|			overlappingObject.setOrientation(Orientation.LEFT)
	 */
	private void slimeSlimeCollision(WorldObject obj) {
		
		if (! ((Slime) obj).getSchool().equals(((Slime)overlappingObject).getSchool())) {
			if (((Slime) obj).getSchool().getNbSlimes() < (((Slime)overlappingObject).getSchool()).getNbSlimes()) {
				((Slime) obj).getSchool().addHpOldSchool((Slime)obj);
				((Slime) obj).changeToSchool(((Slime)overlappingObject).getSchool());
				((Slime) obj).getSchool().addHpNewSchool((Slime)obj);
				
			}
			else if (((Slime) obj).getSchool().getNbSlimes() > (((Slime)overlappingObject).getSchool()).getNbSlimes()) {
				((Slime) overlappingObject).getSchool().addHpOldSchool((Slime)overlappingObject);
				((Slime) overlappingObject).changeToSchool(((Slime)obj).getSchool());
				((Slime) overlappingObject).getSchool().addHpNewSchool((Slime)overlappingObject);
			}
		}
		if (obj.objectOverlapsRight(overlappingObject)) {
			obj.setSpriteIndex(0);
			overlappingObject.setSpriteIndex(1);
			obj.setOrientation(Orientation.LEFT);
			overlappingObject.setOrientation(Orientation.RIGHT);
		}
		else {
			obj.setSpriteIndex(1);
			overlappingObject.setSpriteIndex(0);
			overlappingObject.setOrientation(Orientation.LEFT);
			obj.setOrientation(Orientation.RIGHT);
		}
	}
	private WorldObject overlappingObject;
	
	/**
	 * @param 	obj
	 * 			...
	 * @effect	...	
	 * 			| if ((obj.getObjOverlapTimer() > 0.6) && (overlappingObject.getObjOverlapTimer() > 0.6) && ((obj instanceof Alien && overlappingObject instanceof Shark) || 
	 *			|	 (obj instanceof Shark && overlappingObject instanceof Alien) || 
	 *			|	 (obj instanceof Alien && overlappingObject instanceof Slime) || 
	 *			|	 (obj instanceof Mazub && overlappingObject instanceof Buzam) ||
	 *			|	 (obj instanceof Shark && overlappingObject instanceof Slime) || 
	 *			|	 (obj instanceof Slime && overlappingObject instanceof Alien) || 
	 *			|	 (obj instanceof Slime && overlappingObject instanceof Shark)) )
	 *			|			if ((obj instanceof Alien) && obj.objectOverlapsBottom(overlappingObject))
	 *			|				then 	overlappingObject.addHP(-50)
	 * @effect	...	
	 * 			|			if ((overlappingObject instanceof Alien) && obj.objectOverlapsTop(overlappingObject))
	 * 			|				then obj.addHP(-50)		
	 * @effect	...	
	 * 			|			else 
	 * 			|			 	obj.addHP(-50)
	 *			|				overlappingObject.addHP(-50)
	 * @effect	...	
	 * 			|			if (obj instanceof Slime)
	 * 			|				then updateSchoolAfterSlimeDamage(obj)
	 * @effect	...	
	 * 			|			else if (overlappingObject instanceof Slime)
 	 *			| 				updateSchoolAfterSlimeDamage(overlappingObject)
	 * @effect	...	
	 * 			|			obj.setObjOverlapTimer(0.0)
	 * @effect	...	
	 * 			|			overlappingObject.setObjOverlapTimer(0.0)
	 * @effect	...
	 * 			|			obj.zeroHpKills()
	 * @effect	...
	 * 			|			overlappingObject.zeroHpKills()
	 */
	private void damageOverlappingObjects(WorldObject obj) {
		if ((obj.getObjOverlapTimer() > 0.6) && (overlappingObject.getObjOverlapTimer() > 0.6) && ((obj instanceof Alien && overlappingObject instanceof Shark) || 
				(obj instanceof Shark && overlappingObject instanceof Alien) || 
				(obj instanceof Mazub && overlappingObject instanceof Buzam) ||
				(obj instanceof Alien && overlappingObject instanceof Slime) || 
				(obj instanceof Shark && overlappingObject instanceof Slime) || 
				(obj instanceof Slime && overlappingObject instanceof Alien) || 
				(obj instanceof Slime && overlappingObject instanceof Shark)) ) {
			if ((obj instanceof Alien) && obj.objectOverlapsBottom(overlappingObject)) {
				overlappingObject.addHP(-50);
			}
			else if ((overlappingObject instanceof Alien) && obj.objectOverlapsTop(overlappingObject)) {
				obj.addHP(-50);
			}
			else {
				obj.addHP(-50);
				overlappingObject.addHP(-50);
			}
			
			if (obj instanceof Slime) {
				updateSchoolAfterSlimeDamage(obj);
			}
			else if (overlappingObject instanceof Slime) {
				updateSchoolAfterSlimeDamage(overlappingObject);
			}
			
			obj.setObjOverlapTimer(0.0);
			overlappingObject.setObjOverlapTimer(0.0);
			obj.zeroHpKills();
			overlappingObject.zeroHpKills();
		}
	}

	/**
	 * @param	obj
	 * 			...
	 * @effect	...	
	 * 			| for each slime in ((Slime) obj).getSchool().getAllSlimes()
	 * 			| 	if (! slime.equals(obj) && ! slime.isTerminated())
	 * 			| 		then slime.addHP(-1)
	 */
	private void updateSchoolAfterSlimeDamage(WorldObject obj) {
		for (Slime slime: ((Slime) obj).getSchool().getAllSlimes()) {
			if (! slime.equals(obj) && ! slime.isTerminated()) {
				slime.addHP(-1);
			}
		}
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @effect	...	
	 * 			| if (! (obj instanceof Alien && obj.getVerticalVelocity()>0))
	 * 			|	then obj.setVerticalAcceleration(0.0)
	 * 			|		 obj.setVerticalVelocity(0.0)
	 * @post	...	
	 * 			| if (obj instanceof Alien)
	 * 			|	then ((new) ((Alien) obj)).objectOverlapsBottom
	 * @effect	...
	 * 			| if (overlappingObject.getVerticalVelocity() > 0)
	 * 			|	then overlappingObject.setVerticalVelocity(0.0)
	 */
	private void speedUpdateOverlapObjBottom(WorldObject obj) {
		if (! (obj instanceof Alien && obj.getVerticalVelocity()>0)) {
			obj.setVerticalVelocity(0.0);
			obj.setVerticalAcceleration(0.0);
		}
		if (obj instanceof Alien) {
			((Alien) obj).objectOverlapsBottom = true;
		}
		if (overlappingObject.getVerticalVelocity() > 0) {
			overlappingObject.setVerticalVelocity(0.0);
		}
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @post	...
	 * 			| obj.setVerticalVelocity(0.0)
	 * @effect	...	
	 * 			| if (overlappingObject.getVerticalVelocity() < 0)
	 * 			| 	then overlappingObject.setVerticalVelocity(0.0)
	 * @post	...
	 * 			| if (obj instanceof Alien)
	 * 			| 	then ! ((new) ((Alien) obj)).objectOverlapsBottom
	 * @effect	...
	 * 			| if (obj instanceof Alien)
	 * 			| 	then obj.setVerticalAcceleration(-10.0)
	 * 			
	 */
	private void speedUpdateOverlapObjTop(WorldObject obj) {
		obj.setVerticalVelocity(0.0);
		if (overlappingObject.getVerticalVelocity() < 0) {
			overlappingObject.setVerticalVelocity(0.0);
		}
		if (obj instanceof Alien) {
			((Alien) obj).objectOverlapsBottom = false;
			obj.setVerticalAcceleration(-10.0);
		}
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @effect	...
	 * 			| obj.setHorizontalVelocity(0.0)
	 * @effect	...	
	 * 			| obj.setHorizontalAcceleration(0.0)
	 * @effect	...	
	 * 			| if (overlappingObject.getOrientation() != obj.getOrientation())	
	 * 			|	then	overlappingObject.setHorizontalVelocity(0.0)
	 * 			|			overlappingObject.setHorizontalAcceleration(0.0)
	 * @effect	...	
	 * 			| if (obj instanceof Alien)
	 * 			|	then obj.setVerticalAcceleration(-10.0)
	 * @post	...
	 * 			| if (obj instanceof Alien)
	 * 			|	then ! ((new)((Alien) obj)).objectOverlapsBottom
	 */
	private void speedUpdateOverlapObjRightLeft(WorldObject obj) {
		if ((obj.getOrientation()==Orientation.RIGHT && obj.objectOverlapsRight(overlappingObject) ) || (obj.getOrientation()==Orientation.LEFT && obj.objectOverlapsLeft(overlappingObject)) ) {
			obj.setHorizontalVelocity(0.0);
			obj.setHorizontalAcceleration(0.0);
		}
		
		if (overlappingObject.getOrientation() != obj.getOrientation()) {
			overlappingObject.setHorizontalVelocity(0.0);
			overlappingObject.setHorizontalAcceleration(0.0);
		}

		if (obj instanceof Alien) {
			((Alien) obj).objectOverlapsBottom = false;
			obj.setVerticalAcceleration(-10.0);
		}
	}

	//relaties met WorldObjects
	/**
	 * @param 	obj
	 * 			...
	 * @return	...
	 * 			| worldObjects.contains(obj);
	 */
	boolean hasAsWorldObject(WorldObject obj) {
		return worldObjects.contains(obj);
	}
	private final List<WorldObject> worldObjects = new ArrayList<WorldObject>();
	
	/**
	 * @param	obj
	 * 			...
	 * @pre		...
	 * 			| canHaveAsWorldObject(obj) && obj.hasAsWorld(this)
	 * @post	...
	 * 			| new.hasAsWorldObject(obj)
	 */
	public void addAsWorldObject(@Raw WorldObject obj) {
		assert canHaveAsWorldObject(obj);
		assert obj.hasAsWorld(this);
		worldObjects.add(obj);
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @pre		...
	 * 			| obj.hasAsWorld(this)
	 * @effect	...
	 * 			| if (obj.isTerminated())
	 * 			| 	then worldObjects.remove(obj)
	 */
	void removeAsWorldObject(@Raw WorldObject obj) {
		assert obj.hasAsWorld(this);
		if (obj.isTerminated()) {
			worldObjects.remove(obj);
		}
	}

	/**
	 * @param	obj
	 * 			...
	 * @return	...
	 * 			| (! obj.isTerminated()) && (obj != null)
	 */
	private boolean canHaveAsWorldObject(WorldObject obj) {
		return (! obj.isTerminated()) && (obj != null);
	}
	
	/**
	 * @return	...
	 * 			| for each obj in getAllWorldObjects()
	 * 			| 	if ( (!canHaveAsWorldObject(obj)) || (! obj.hasAsWorld(this)) )
	 * 			|		then false
	 * @return 	...
	 * 			| if (worldObjects.size() > 102 || getNbAliens() > 1)
	 * 			| 	then false
	 * 			| else true
	 */
	@SuppressWarnings("unused")
	private boolean hasProperWorldObjects() {
		for (WorldObject obj: getAllWorldObjects()) {
			if ( (!canHaveAsWorldObject(obj)) || (! obj.hasAsWorld(this)) )
				return false;
		}
		if (worldObjects.size() > 102 || getNbMazubs() > 1) {
			return false;
		}
		else
			return true;
	}
	
	/**
	 * @return	...
	 * 			| new ArrayList<WorldObject>(worldObjects)
	 */
	 public List<WorldObject> getAllWorldObjects() {
		return new ArrayList<WorldObject>(worldObjects);
	}
	
	/**
	 * @return	...
	 * 			| coll
	 * 			| 	for each obj in getAllWorldObjects()
	 * 			|		if (obj instanceof Plant)
	 * 			| 			coll.contains(obj)
	 */
	public Collection<Plant> getPlants() {
		ArrayList<Plant> coll = new ArrayList<Plant>();
		for (WorldObject obj: getAllWorldObjects()) {
			if (obj instanceof Plant) {
				coll.add((Plant) obj);
			}
		}
		return coll;
	}

	/**
	 * @return	...
	 * 			| coll
	 * 			| 	for each obj in getAllWorldObjects()
	 * 			|		if (obj instanceof Shark)
	 * 			| 			coll.contains(obj)
	 */
	public ArrayList<Shark> getSharks() {
		ArrayList<Shark> coll = new ArrayList<Shark>();
		for (WorldObject obj: getAllWorldObjects()) {
			if (obj instanceof Shark) {
				coll.add((Shark) obj);
			}
		}
		return coll;
	}
	
	/**
	 * @return	...
	 * 			| coll
	 * 			| 	for each obj in getAllWorldObjects()
	 * 			|		if (obj instanceof Slime)
	 * 			| 			coll.contains(obj)
	 */
	public ArrayList<Slime> getSlimes() {
		ArrayList<Slime> coll = new ArrayList<Slime>();
		for (WorldObject obj: getAllWorldObjects()) {
			if (obj instanceof Slime) {
				coll.add((Slime) obj);
			}
		}
		return coll;
	}

	/**
	 * @return	...
	 * 			| for each obj in getAllWorldObjects()
	 * 			|	if (obj instanceof Mazub)
	 * 			|		return obj
	 */
	public Mazub getMazub() {
		for (WorldObject obj: getAllWorldObjects()) {
			if (obj instanceof Mazub) {
				return (Mazub) obj;
				}
		}
		return null;
	}
	
	/**
	 * @return	...
	 * 			| for each obj in getAllWorldObjects()
	 * 			|	if (obj instanceof Buzam)
	 * 			|		return obj
	 */
	public Buzam getBuzam() {
		for (WorldObject obj: getAllWorldObjects()) {
			if (obj instanceof Buzam) {
				return (Buzam) obj;
				}
		}
		return null;
	}
	
	/**
	 * @return	...
	 * 			| count
	 * 			| 	for each obj in getAllWorldObjects()
	 * 			|		if (obj instanceof Mazub)
	 * 			|			count +=1
	 */
	public int getNbMazubs() {
		int count = 0;
		for (WorldObject obj : getAllWorldObjects()) {
			if (obj instanceof Mazub) {
				count += 1;
			}
		}
		return count;
	}
	
	/**
	 * @return	...
	 * 			| gameStarted
	 */
	@Basic
	public boolean isGameStarted() {
		return gameStarted;
	}
	private boolean gameStarted = false;

	/**
	 * @param	gameStarted
	 * 			...
	 * @post	...
	 * 			|	new.isGameStarted() == gameStarted
	 */
	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}

	public ArrayList<Tile> getTiles() {
		ArrayList<Tile> coll = new ArrayList<Tile>();
		for (Tile[] tileRow: getFeatureMatrix()) {
			for (Tile tile: tileRow) {
				coll.add((Tile) tile);
			}
		}
		return coll;
	}
}
