package jumpingalien.program.statements;

import jumpingalien.model.BreakException;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
import jumpingalien.program.expressions.Expression;
import jumpingalien.program.expressions.Boolean;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of While's extending the abstract type Statement.
 * 
 */
public class While extends Statement {

	public While(Statement body, Expression condition, SourceLocation sourceLocation) {
		super(sourceLocation);
		this.body = body;
		this.condition = condition;
	}

	public Statement getBody() {
		return body;
	}
	private final Statement body;

	private Expression getCondition() {
		return condition;
	}
	private final Expression condition;
	
	private boolean started;

	private boolean hasStarted() {
		return this.started;
	}
	
	private void setStarted(boolean b) {
		this.started = b;
	}
	
	@Override
	public double executeStatement(Program program, double time) {
		if (time >= 0.001) {
			
			while (((Boolean) (getCondition().evaluate(program))).getBooleanValue() == true && time>=0.001) {
				
				time = time - 0.001;
				if (hasStarted() && !getBody().isFinished()) {
					time = time + 0.001; 
 				}
				try {
					time = getBody().executeStatement(program, time);
				} catch (BreakException e) {
					break;
				}
				if (! getBody().isFinished()) { 
					this.setFinished(false);
					setStarted(true); 
					return 0.0;
				}
			}
			this.setFinished(true);
			return time -0.001;
		}
		else {
			return time; 
		}
	}
	
	@Override
	public void reset() {
		setFinished(false);
		getBody().reset();
	}

}
