package jumpingalien.program.expressions;

import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of GetHeight's extending the abstract class UnaryOperator.
*/
public class GetHeight extends UnaryOperator {

	public GetHeight(Expression operand, SourceLocation location) {
		super(operand, location);
	}

	@Override
	public Expression evaluate(Program program) {
		return new DoubleConstant(((WorldObject) (getOperand().evaluate(program))).getHeight(),null);
	}

}
