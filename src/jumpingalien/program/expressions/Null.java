package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Null extending the abstract type Expression.
 * 
 */

public class Null extends Expression {

	public Null(SourceLocation location) {
		super(location);
	}
	
	public Object getValue() {
		return null; 
	}

	@Override
	public Expression evaluate(Program program) {
		return this;
	}
	
	@Override
	public String toString() {
		return "null";
	}

}
