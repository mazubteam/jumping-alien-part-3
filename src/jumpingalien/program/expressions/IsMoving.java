package jumpingalien.program.expressions;

import jumpingalien.model.Orientation;
import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of IsMovings extending the abstract class UnaryOperator.
*/
public class IsMoving extends UnaryOperator {

	private Direction direction;

	public IsMoving(Expression operand, Direction d, SourceLocation location) {
		super(operand, location);
		setDirection(d);
	}

	private void setDirection(Direction d) {
		this.direction = d;
	}

	@Override
	public Expression evaluate(Program program) {
		if (getOperand().evaluate(program) instanceof Self) {
			WorldObject obj = program.getWorldObject();
			
			if ( direction.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.DOWN && obj.getVerticalVelocity()<0 ) {
				return new Boolean(true,null);
			}
			else if ( direction.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.UP && obj.getVerticalVelocity()>0 ) {
				return new Boolean(true,null);
			}
			else if ( direction.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT && obj.getOrientation() == Orientation.RIGHT && obj.getHorizontalVelocity()>0 ) {
				return new Boolean(true,null);
			}
			else if ( direction.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT && obj.getOrientation() == Orientation.LEFT && obj.getHorizontalVelocity()>0 ) {
				return new Boolean(true,null);
			}
			else return new Boolean(false,null);
		}
		
		else {
			
			if ( direction.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.DOWN && ((WorldObject) (getOperand().evaluate(program))).getVerticalVelocity()<0 ) {
				return new Boolean(true,null);
			}
			else if ( direction.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.UP && ((WorldObject) (getOperand().evaluate(program))).getVerticalVelocity()>0 ) {
				return new Boolean(true,null);
			}
			else if ( direction.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT && ((WorldObject) (getOperand().evaluate(program))).getOrientation() == Orientation.RIGHT && ((WorldObject) (getOperand().evaluate(program))).getHorizontalVelocity()>0 ) {
				return new Boolean(true,null);
			}
			else if ( direction.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT && ((WorldObject) (getOperand().evaluate(program))).getOrientation() == Orientation.LEFT && ((WorldObject) (getOperand().evaluate(program))).getHorizontalVelocity()>0 ) {
				return new Boolean(true,null);
			}
			else return new Boolean(false,null);
		}
	}
}
