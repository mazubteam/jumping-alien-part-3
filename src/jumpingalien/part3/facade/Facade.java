package jumpingalien.part3.facade;

import java.util.Collection;
import java.util.Optional;


import jumpingalien.model.Buzam;
import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.School;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.part3.programs.ParseOutcome;
import jumpingalien.part3.programs.ProgramParser;
import jumpingalien.program.Program;
import jumpingalien.program.ProgramFactory;
import jumpingalien.program.expressions.Expression;
import jumpingalien.program.statements.Statement;
import jumpingalien.program.types.MyType;
import jumpingalien.util.ModelException;
import jumpingalien.util.Sprite;

public class Facade implements IFacadePart3 {

	public Facade(){
		
	}
	
	@Override
	public Mazub createMazub(int pixelLeftX, int pixelBottomY, Sprite[] sprites) {
		Mazub alien = new Mazub(sprites);
		alien.setInitialXPosition(pixelLeftX);
		alien.setInitialYPosition(pixelBottomY);
		return alien;
	}

	@Override
	public int[] getLocation(Mazub alien) {
		int[] pos = { alien.getXPosition(), alien.getYPosition() };
		return pos;
	}

	@Override
	public double[] getVelocity(Mazub alien) {
		double[] vel = { alien.getHorizontalVelocity(), alien.getVerticalVelocity() };
		return vel;
	}

	@Override
	public double[] getAcceleration(Mazub alien) {
		double[] acc = { alien.getHorizontalAcceleration(), alien.getVerticalAcceleration() };
		return acc;
	}

	@Override
	public int[] getSize(Mazub alien) {
		int[] siz = { alien.getWidth(), alien.getHeight() };
		return siz;
	}

	@Override
	public Sprite getCurrentSprite(Mazub alien) {
		return alien.getCurrentSprite();
	}

	@Override
	public void startJump(Mazub alien) {
		try {
			alien.startJump();
		} catch (Exception e) {
		}		
	}
	
	@Override
	public void endJump(Mazub alien) {
		try {
			alien.endJump();
		}
		catch (Exception e) {
		}
	}

	@Override
	public void startMoveLeft(Mazub alien) {
		if (alien.isMovingRight()) {
			alien.endMoveRight();
		}
		alien.startMoveLeft();
	}

	@Override
	public void endMoveLeft(Mazub alien) {
		if (! alien.isMovingRight()) {
			alien.endMoveLeft();	
		}
		
	}

	@Override
	public void startMoveRight(Mazub alien) {
		if (alien.isMovingLeft()) {
			alien.endMoveLeft();
		}
		alien.startMoveRight();	
		
	}

	@Override
	public void endMoveRight(Mazub alien) {
		if (! alien.isMovingLeft()) {
			alien.endMoveRight();	
		}
	}

	@Override
	public void startDuck(Mazub alien) {
		try { 
			alien.startDuck();
		}
		catch (Exception e) {
		}
	}

	@Override
	public void endDuck(Mazub alien) {
		try {
			alien.endDuck();
		}
		catch (Exception e) {
		}
	}

	@Override
	public void advanceTime(Mazub alien, double dt) {		
	}

	@Override
	public int getNbHitPoints(Mazub alien) {
		return alien.getNbHitPoints();
	}

	@Override
	public World createWorld(int tileSize, int nbTilesX, int nbTilesY,
			int visibleWindowWidth, int visibleWindowHeight, int targetTileX,
			int targetTileY) {
		World myWorld = new World();
		myWorld.setTileLength(tileSize);
		myWorld.setXDimension(tileSize * nbTilesX);
		myWorld.setYDimension(tileSize * nbTilesY);
		myWorld.setVisibleWindowWidth(visibleWindowWidth);
		myWorld.setVisibleWindowHeight(visibleWindowHeight);
		myWorld.setTargetTile(targetTileX,targetTileY);
		myWorld.createFeatureMatrix(nbTilesX, nbTilesY);
		return myWorld;
	}

	@Override
	public int[] getWorldSizeInPixels(World world) {
		int[] a = {world.getXDimension(), world.getYDimension()};
		return a;
	}

	@Override
	public int getTileLength(World world) {
		return world.getTileLength();
	}

	@Override
	public void startGame(World world) {
		world.setGameStarted(true);
	}

	@Override
	public boolean isGameOver(World world) {
		if ( ( world.getNbMazubs() == 0) || ((( world.getTile(world.getMazub().getXPosition()-1,world.getMazub().getYPosition()+1)[0] == world.getTargetTileX())
				&& (world.getTile(world.getMazub().getXPosition()-1,world.getMazub().getYPosition()+1)[1] == world.getTargetTileY()))) ) {
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean didPlayerWin(World world) {
		
		if ( world.getNbMazubs() == 0) {
				return false;
			}
			else 
				return true;
	}

	@Override
	public void advanceTime(World world, double dt) {
		world.advanceTime(dt);
	}

	@Override
	public int[] getVisibleWindow(World world) {
		int a[] = {world.getWindowLeft(),world.getWindowBottom(),world.getWindowLeft()+world.getVisibleWindowWidth(), world.getWindowBottom()+world.getVisibleWindowHeight()};
		return a;
	}

	@Override
	public int[] getBottomLeftPixelOfTile(World world, int tileX, int tileY) {
		return world.getBottomLeftPixelOfTile(tileX, tileY);
	}

	@Override
	public int[][] getTilePositionsIn(World world, int pixelLeft,
			int pixelBottom, int pixelRight, int pixelTop) {
		return world.getTilePositionsIn(pixelLeft, pixelBottom, pixelRight, pixelTop);
	}

	@Override
	public int getGeologicalFeature(World world, int pixelX, int pixelY)
			throws ModelException {
		try {
			return world.getFeatureAtPixel(pixelX, pixelY);
		}
		catch (IllegalArgumentException exc) {
			throw new ModelException("Illegal arguments:" + pixelX  + ", " + pixelY);
		}
	}

	@Override
	public void setGeologicalFeature(World world, int tileX, int tileY,
			int tileType) {
		assert ! world.isGameStarted();
		world.setFeatureAtTile(tileX, tileY, tileType);
		
	}

	@Override
	public void setMazub(World world, Mazub alien) {
		assert ! world.isGameStarted();
		alien.setWorld(world);
		world.addAsWorldObject(alien);
	}

	@Override
	public boolean isImmune(Mazub alien) {
		return false;
	}

	@Override
	public Plant createPlant(int x, int y, Sprite[] sprites) {
		Plant plant = new Plant(sprites);
		plant.setInitialXPosition(x);
		plant.setInitialYPosition(y);
		return plant;
	}

	@Override
	public void addPlant(World world, Plant plant) {
		assert ! world.isGameStarted();
		plant.setWorld(world);
		world.addAsWorldObject(plant);
	}

	@Override
	public Collection<Plant> getPlants(World world) {
		return world.getPlants();
	}

	@Override
	public int[] getLocation(Plant plant) {
		int[] pos = { plant.getXPosition(), plant.getYPosition() };
		return pos;
	}

	@Override
	public Sprite getCurrentSprite(Plant plant) {
		return plant.getCurrentSprite();
	}

	@Override
	public Shark createShark(int x, int y, Sprite[] sprites) {
		Shark shark = new Shark(sprites);
		shark.setInitialXPosition(x);
		shark.setInitialYPosition(y);
		return shark;
	}

	@Override
	public void addShark(World world, Shark shark) {
		assert ! world.isGameStarted();
		shark.setWorld(world);
		world.addAsWorldObject(shark);
	}

	@Override
	public Collection<Shark> getSharks(World world) {
		return world.getSharks();
	}

	@Override
	public int[] getLocation(Shark shark) {
		int[] pos = { shark.getXPosition(), shark.getYPosition() };
		return pos;
	}

	@Override
	public Sprite getCurrentSprite(Shark shark) {
		return shark.getCurrentSprite();
	}

	@Override
	public School createSchool() {
		School school = new School();
		return school;
	}

	@Override
	public Slime createSlime(int x, int y, Sprite[] sprites, School school) {
		Slime slime = new Slime(sprites);
		slime.setInitialXPosition(x);
		slime.setInitialYPosition(y);
		slime.setSchool(school);
		school.addAsSlime(slime);
		return slime;
	}

	@Override
	public void addSlime(World world, Slime slime) {
		assert ! world.isGameStarted();
		slime.setWorld(world);
		world.addAsWorldObject(slime);
	}

	@Override
	public Collection<Slime> getSlimes(World world) {
		return world.getSlimes();
	}

	@Override
	public int[] getLocation(Slime slime) {
		int[] pos = { slime.getXPosition(), slime.getYPosition() };
		return pos;
	}

	@Override
	public Sprite getCurrentSprite(Slime slime) {
		return slime.getCurrentSprite();
	}

	@Override
	public School getSchool(Slime slime) {
		return slime.getSchool();
	}



	@Override
	public Buzam createBuzam(int pixelLeftX, int pixelBottomY, Sprite[] sprites) {
		Buzam alien = new Buzam(sprites);
		alien.setInitialXPosition(pixelLeftX);
		alien.setInitialYPosition(pixelBottomY);
		return alien;
	}



	@Override
	public Buzam createBuzamWithProgram(int pixelLeftX, int pixelBottomY,
			Sprite[] sprites, Program program) {
		Buzam alien = createBuzam(pixelLeftX, pixelBottomY,sprites);
		alien.setProgram(program);
		return alien;
	}

	@Override
	public Plant createPlantWithProgram(int x, int y, Sprite[] sprites,
			Program program) {
		Plant plant = createPlant(x, y,sprites);
		plant.setProgram(program);
		return plant;
	}

	@Override
	public Shark createSharkWithProgram(int x, int y, Sprite[] sprites,
			Program program) {
		Shark shark = createShark(x, y,sprites);
		shark.setProgram(program);
		return shark;
	}

	@Override
	public Slime createSlimeWithProgram(int x, int y, Sprite[] sprites,
			School school, Program program) {
		Slime slime = createSlime(x, y,sprites, school);
		slime.setProgram(program);
		return slime;
	}

	@Override
	public ParseOutcome<?> parse(String text) {
		ProgramParser<Expression, Statement, MyType, Program> parser = new ProgramParser<> (new ProgramFactory());
		Optional<Program> program = parser.parseString(text);
		if (program.isPresent()) {
			return ParseOutcome.success(program.get());
		}
		else return ParseOutcome.failure(parser.getErrors());
	}

	@Override
	public boolean isWellFormed(Program program) {
		return program.isWellFormed();
	}

	@Override
	public void addBuzam(World world, Buzam buzam) {
		assert ! world.isGameStarted();
		buzam.setWorld(world);
		world.addAsWorldObject(buzam);
	}


	@Override
	public int[] getLocation(Buzam alien) {
		int[] pos = { alien.getXPosition(), alien.getYPosition() };
		return pos;
	}

	@Override
	public double[] getVelocity(Buzam alien) {
		double[] vel = { alien.getHorizontalVelocity(), alien.getVerticalVelocity() };
		return vel;
	}

	@Override
	public double[] getAcceleration(Buzam alien) {
		double[] acc = { alien.getHorizontalAcceleration(), alien.getVerticalAcceleration() };
		return acc;
	}

	@Override
	public int[] getSize(Buzam alien) {
		int[] siz = { alien.getWidth(), alien.getHeight() };
		return siz;
	}

	@Override
	public Sprite getCurrentSprite(Buzam alien) {
		return alien.getCurrentSprite();
	}

	@Override
	public int getNbHitPoints(Buzam alien) {
		return alien.getNbHitPoints();
	}
	
}
