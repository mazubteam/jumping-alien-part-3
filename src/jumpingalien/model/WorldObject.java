package jumpingalien.model;

import jumpingalien.program.Program;
import jumpingalien.program.expressions.Direction;
import jumpingalien.util.Sprite;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 * @version 1.0
 *
 * A class of WorldObjects with a position, dimension, orientation, speed, acceleration, ...
 * 
 * 
 * @invar 	The horizontal velocity of each WorldObject must be a valid horizontal velocity for any WorldObject.
 * 			| isValidHorizontalVelocity(this.getHorizontalVelocity())
 * @invar	The horizontal acceleration of each WorldObject must be a valid horizontal acceleration for any WorldObject.
 * 			| isValidHorizontalAcceleration(this.getHorizontalAcceleration())
 * @invar	The vertical acceleration of each WorldObject must be a valid vertical acceleration for any WorldObject.
 * 			| isValidVerticalAcceleration(this.getVerticalAcceleration())
 * @invar	The x-position of each WorldObject must be a valid x-position for any WorldObject.
 * 			| isValidXPosition(this.getXPosition())
 * @invar	The y-position of each WorldObject must be a valid y-position for any WorldObject.
 * 			| isValidYPosition(this.getYPosition())
 * @invar 	The orientation of each WorldObject must be a valid orientation for any WorldObject.
 * 			| isValidOrientation(this.getOrientation())
 */
public abstract class WorldObject {

	// binding met World
	/**
	 * @return 	...
	 * 			| world
	 */
	@Basic
	public World getWorld() {
		return this.world;
	}
	private World world;

	/**
	 * @param 	world
	 * 			...
	 * @pre		...
	 * 			| canHaveAsWorld(world)
	 * @post	...
	 * 			| new.getWorld() == world
	 */
	@Raw
	public void setWorld(World world) {
		assert canHaveAsWorld(world);
		this.world = world;
	}

	/**
	 * @param	world
	 * 			...
	 * @return	...
	 * 			| getWorld() == world
	 */
	boolean hasAsWorld(World world) {
		return this.getWorld() == world;
	}

	/**
	 * @param 	world
	 * 			...
	 * @return	...
	 * 			| if (this.isTerminated())
	 *			|	then world == null
	 *			| else 
	 *			|	then world != null			
	 */
	@Raw
	boolean canHaveAsWorld(World world) {
		if (this.isTerminated()) {
			return (world == null);
		}
		else 
			return (world != null);
	}

	/**
	 * @return	...
	 * 			| (this.canHaveAsWorld(this.getWorld()) && this.getWorld().hasAsWorldObject(this))
	 */
	boolean hasProperWorld() {
		return (this.canHaveAsWorld(this.getWorld()) && this.getWorld().hasAsWorldObject(this));
	}

	// Termination
	/**
	 * @return	...
	 * 			| isTerminated
	 */
	@Basic
	@Raw
	public boolean isTerminated() {
		return this.isTerminated;
	}
	private boolean isTerminated;

	/**
	 * @effect	...
	 * 			| this.isTerminated = true;
	 * @effect	...
	 * 			| getWorld().removeAsWorldObject(this);
	 * @effect	...
	 * 			| setWorld(null);
	 */			
	public void terminate() {
		this.isTerminated = true;
		getWorld().removeAsWorldObject(this);
		setWorld(null);
	}

	//HP
	/**
	 *
	 * @return	...
	 * 			| this.nbHitPoints
	 * @return	...
	 * 			| nbHitPoints
	 */
	@Basic
	public int getNbHitPoints() {
		return this.nbHitPoints;	
	}
	private int nbHitPoints;

	/**
	 * 
	 * @param points
	 * @post	...	
	 * 			| new.getNbHitPoints = points
	 * @param 	points
	 * 			...
	 * @post	...
	 * 			| new.getNbHitPoints() == points
	 */
	protected void setNbHitPoints(int points) {
		this.nbHitPoints = points;
	}

	/**
	 * @param 	nb
	 * 			...
	 * @effect	...
	 * 			| if (getNbHitPoints() + nb < getMaxNbHitPoints() && (getNbHitPoints() + nb > 0))
	 * 			|		then setNbHitPoints(getNbHitPoints() + nb)
	 * 			| else if (getNbHitPoints() + nb <= 0)
	 * 			|		then setNbHitPoints(0)
	 * 			| else if (getNbHitPoints() + nb >= getMaxNbHitPoints())
	 * 			| 		then setNbHitPoints(getMaxNbHitPoints())
	 */
	public void addHP(int nb) {
		if (getNbHitPoints() + nb < getMaxNbHitPoints() && (getNbHitPoints() + nb > 0)) {
			setNbHitPoints(getNbHitPoints() + nb);
		} 
		else if (getNbHitPoints() + nb <= 0) {
			setNbHitPoints(0);
		}
		else if (getNbHitPoints() + nb >= getMaxNbHitPoints()) {
			setNbHitPoints(getMaxNbHitPoints());
		}
	}
	
	// get/set Images
	/**
	 * @param 	sprites
	 * 			...
	 * @post 	...
	 * 			| new.getImages() == sprites
	 */
	protected void setImages(Sprite[] sprites) {
		this.images = sprites;
	}
	private Sprite[] images;

	/**
	 * @return 	...
	 * 			| images
	 */
	@Basic
	public Sprite[] getImages() {
		return this.images;
	}

	// get/set Sprite Index
	/**
	 * @return	...
	 * 			| spriteIndex
	 */
	@Basic
	public int getSpriteIndex() {
		return this.spriteIndex;
	}
	private int spriteIndex;

	/**
	 * @param 	index
	 * 			...
	 * @post	...
	 * 			| new.getSpriteIndex() == index
	 */
	protected void setSpriteIndex(int index) {
		this.spriteIndex = index;
	}

	// get/set Position
	/**
	 * @return	...
	 * 			| xPosition
	 */
	@Basic
	public double getXPositionDouble() {
		return this.xPosition;
	}
	
	protected double xPosition;

	/**
	 * @return 	...
	 * 			| yPosition
	 */
	@Basic
	protected double getYPositionDouble() {
		return this.yPosition;
	}
	protected double yPosition;
	
	/**
	 * 
	 * @return	...
	 * 			xPosition
	 * @return	...
	 * 			| (int) xPosition
	 */
	@Basic
	public int getXPosition() {
		return (int) xPosition;
	}
	
	/**
	 * 
	 * @return	...
	 * 			yPosition
	 * @return	...
	 * 			| (int) yPosition
	 */
	@Basic
	public int getYPosition() {
		return (int) yPosition;
	}

	/**
	 * @param 	xPos
	 * 			...
	 * @post	...
	 * 			| new.getXPosition() == xPos
	 * @throws 	IllegalXPositionException(xPos)
	 * 			...
	 * 			| (! isValidXPosition(xPos))
	 */
	protected void setXPosition(double xPos) throws IllegalXPositionException {
		if (! isValidXPosition(xPos)) {
			throw new IllegalXPositionException(xPos);
		}
		this.xPosition = xPos;
	}

	/**
	 * @param 	yPos
	 * 			...
	 * @post	...
	 * 			| new.getYPosition() == yPos
	 * @throws 	IllegalYPositionException(yPos)
	 * 			...
	 * 			| (! isValidXPosition(yPos))
	 */
	protected void setYPosition(double yPos) throws IllegalYPositionException {
		if (! isValidYPosition(yPos)) {
			throw new IllegalYPositionException(yPos);
		}
		this.yPosition = yPos;
	}
	
	/**
	 * @param 	xPos
	 * 			...
	 * @post	...
	 * 			| new.getXPosition() == xPos
	 */
	public void setInitialXPosition(double xPos) {
		this.xPosition = xPos;
	}

	/**
	 * @param yPos
	 * 			...
	 * @post	...
	 * 			| new.getYPosition() == yPos
	 */
	public void setInitialYPosition(double yPos) {
		this.yPosition = yPos;
	}
	
	// get/set Velocity
	/**
	 * @return 	...
	 * 			| horizontalVelocity
	 */
	@Basic
	public double getHorizontalVelocity() {
		return this.horizontalVelocity;
	}
	private double horizontalVelocity;
	
	/**
	 * @param 	HorizVel 
	 * 			...
	 * @post 	...
	 * 			| if (isValidHorizontalVelocity(HorizVel))
	 * 			| 	then new.getHorizontalVelocity() == HorizVel
	 */
	protected void setHorizontalVelocity(double HorizVel) {
		if (isValidHorizontalVelocity(HorizVel)) {
			this.horizontalVelocity = HorizVel;
		}
	}
		
	/**
	 * @return	...
	 * 			| verticalVelocity
	 */
	@Basic
	public double getVerticalVelocity() {
		return this.verticalVelocity;
	}
	protected double verticalVelocity;
	
	/**
	 * @param vertVelocity 
	 * 			...
	 * @post 	...
	 * 			| new.getVerticalVelocity() == vertVelocity	
	 */
	protected void setVerticalVelocity(double vertVelocity) {
		if (isValidVerticalVelocity(vertVelocity)) {
			this.verticalVelocity = vertVelocity;
		}
	}
	
	// get/set Acceleration
	/**
	 * @return 	...
	 * 			| horizontalAcceleration
	 */
	@Basic
	public double getHorizontalAcceleration() {
		return this.horizontalAcceleration;
	}
	protected double horizontalAcceleration;
	
	/**
	 * @param 	acc
	 * 			...
	 * @post 	...
	 * 			| if (isValidHorizontalAcceleration(acc))
	 * 			|	then new.getHorizontalAcceleration() == acc
	 */
	protected void setHorizontalAcceleration(double acc) {
		if (isValidHorizontalAcceleration(acc))
			this.horizontalAcceleration = acc;
	}
	
	/**
	 * @return 	...
	 * 			| verticalAcceleration
	 */
	@Basic
	public double getVerticalAcceleration() {
		return this.verticalAcceleration;
	}
	protected double verticalAcceleration;
	
	/**
	 * @param 	acc
	 * 			...
	 * @post 	...
	 * 			| if isValidVerticalAcceleration(acc)
	 * 			| 	then new.getVerticalAcceleration() == acc
	 */
	protected void setVerticalAcceleration(double acc) {
		if (isValidVerticalAcceleration(acc))
			this.verticalAcceleration = acc;
	}

	// get/set Initial/Max Velocity & Acceleration
	/**
	 * @return 	...
	 * 			| initialHorizontalVelocity
	 */
	@Basic @Immutable
	protected double getInitialHorizontalVelocity() {
		return this.initialHorizontalVelocity;
	}
	protected double initialHorizontalVelocity;

	/**
	 * @param 	vStart
	 * 			...
	 * @post 	...
	 * 			| new.getInitialHorizontalVelocity() == vStart
	 */
	protected void setInitialHorizontalVelocity(double vStart) {
		this.initialHorizontalVelocity = vStart;
	}

	/**
	 * @return	...
	 * 			| maxHorizontalVelocity
	 */
	@Basic @Immutable
	protected double getMaxHorizontalVelocity() {
		return this.maxHorizontalVelocity;
	}
	protected double maxHorizontalVelocity;

	/**
	 * @param	maxHorizVel
	 * 			...
	 * @post 	...
	 * 			| new.getMaxHorizontalVelocity() == maxHorizVel
	 */
	protected void setMaxHorizontalVelocity(double maxHorizVel) {
		this.maxHorizontalVelocity = maxHorizVel;
	}

	// get Dimensions
	/**
	 * @return	...
	 * 			| ((this.getImages())[this.getSpriteIndex()]).getWidth()
	 */
	public int getWidth() {
		return ((this.getImages())[this.getSpriteIndex()]).getWidth();
	}

	/**
	 * @return 	...
	 * 			| ((this.getImages())[this.getSpriteIndex()]).getHeight()
	 */
	public int getHeight() {
		return ((this.getImages())[this.getSpriteIndex()]).getHeight();
	}

	// get/set Orientation
	/**
	 * @return	...
	 * 			| orientation
	 */
	@Basic
	public Orientation getOrientation() { 
		return this.orientation;
	}
	private Orientation orientation;

	/**
	 * @param 	orientation
	 * 			...
	 * @post 	...
	 * 			| new.getOrientation() = orientation
	 */
	protected void setOrientation(Orientation orientation) { 
		this.orientation = orientation;
	}

	// Sprite & Sprite Index
	/**
	 * @return 	...
	 * 			| (this.getImages())[this.getSpriteIndex()]
	 */
	public Sprite getCurrentSprite() {
		return (this.getImages())[this.getSpriteIndex()];
	}

	// checkers
	/**
	 * @param 	xPos
	 * 			...
	 * @return 	...
	 * 			| (xPos >= 0 && xPos < getWorld().getXDimension()-1)
	 */
	private boolean isValidXPosition(double xPos) {
		if (! isTerminated()) {
			return (xPos >= 0 && xPos < getWorld().getXDimension()-1);}
		else return false;
	}

	/**
	 * @param 	yPos
	 * 			...
	 * @return 	...
	 * 			| (yPos >= 0 && yPos < getWorld().getYDimension()-1)
	 */
	private boolean isValidYPosition(double yPos) {
		if (! isTerminated()) {
			return (yPos >= 0 && yPos < getWorld().getYDimension()-1);
		}
		else return false;
	}

	/**
	 * @param 	velocity
	 * 			...
	 * @return	...
	 * 			| if (velocity != Double.POSITIVE_INFINITY && velocity != Double.NEGATIVE_INFINITY && velocity != Double.NaN)
	 * 			| 	return ...
	 * 			| else 
	 * 			| 	return false
	 */
	protected abstract boolean isValidHorizontalVelocity(double velocity);
	
	/**
	 * @param	 velocity
	 * 			...
	 * @return	...
	 * 			| if (velocity != Double.POSITIVE_INFINITY && velocity != Double.NEGATIVE_INFINITY && velocity != Double.NaN)
	 * 			| 	return ...
	 * 			| else 
	 * 			| 	return false
	 */
	protected abstract boolean isValidVerticalVelocity(double velocity);
	
	/**
	 * @param 	acc
	 * 			...
	 * @return	...
	 * 			| if (acc != Double.POSITIVE_INFINITY && acc != Double.NEGATIVE_INFINITY && acc != Double.NaN)
	 * 			| 	return ...
	 * 			| else 
	 * 			| 	return false
	 */
	protected abstract boolean isValidHorizontalAcceleration(double acc);
	
	/**
	 * @param 	acc
	 * 			...
	 * @return	...
	 * 			| if (acc != Double.POSITIVE_INFINITY && acc != Double.NEGATIVE_INFINITY && acc != Double.NaN)
	 * 			| 	return ...
	 * 			| else 
	 * 			| 	return false
	 */
	protected abstract boolean isValidVerticalAcceleration(double acc);
	
	/**
	 * @param 	orientation
	 * 			...
	 * @return	...
	 * 			| ( (orientation == Orientation.RIGHT) || (orientation == Orientation.LEFT) )
	 */
	public static boolean isValidOrientation(Orientation orientation) {
		return ( (orientation == Orientation.RIGHT) || (orientation == Orientation.LEFT) );
	}
	
	/**
	 * @param	ft
	 * 			...
	 * @return	...
	 * 			| (!isTerminated() && getWorld().getFeatureAtPixel(xpos, ypos) == ft)
	 */
	public boolean featureOverlapsRight(int ft) {
		int bottom = getYPosition()+1;
		int top = getYPosition()+getHeight()-2;
		int xpos = getXPosition()+getWidth()-1;

		for (int ypos=bottom; ypos < top; ypos++) {
			if (!isTerminated() && getWorld().getFeatureAtPixel(xpos, ypos) == ft) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param 	ft
	 * 			...
	 * @return	...
	 * 			| (!isTerminated() && getWorld().getFeatureAtPixel(xpos, ypos) == ft)
	 */
	public boolean featureOverlapsLeft(int ft) {
		int bottom = getYPosition()+1;
		int top = getYPosition()+getHeight()-2;
		int xpos = getXPosition();

		for (int ypos=bottom; ypos < top; ypos++) {
			if (!isTerminated() && getWorld().getFeatureAtPixel(xpos, ypos) == ft) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param 	ft
	 * 			...
	 * @return	...
	 * 			| (!isTerminated() && getWorld().getFeatureAtPixel(xpos, ypos) == ft)
	 */
	public boolean featureOverlapsTop(int ft) {
		int left = getXPosition() + 1;
		int right = getXPosition() + getWidth() - 2;
		int ypos = getYPosition() + getHeight() + 1;
		

		for (int xpos=left; xpos < right; xpos++) {
			if (!isTerminated() && getWorld().getFeatureAtPixel(xpos, ypos) == ft) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param 	ft
	 * 			...
	 * @return	...
	 * 			| for (int xpos=getXPosition() + 1; xpos <= getXPosition() + getWidth() - 2; xpos++)
	 * 			| 	if (!isTerminated() && getWorld().getFeatureAtPixel(xpos, ypos) == ft)
	 * 			|		then true
	 * @return	...
	 * 			| for (int xpos=getXPosition() + 1; xpos <= getXPosition() + getWidth() - 2; xpos++)
	 * 			| 	if ! (!isTerminated() && getWorld().getFeatureAtPixel(xpos, ypos) == ft)
	 * 			|		then false		
	 */
	public boolean featureOverlapsBottom(int ft) {
		int left = getXPosition() + 1;
		int right = getXPosition() + getWidth() - 2;
		int ypos = getYPosition();
		
		for (int xpos=left; xpos <= right; xpos++) {
			if (!isTerminated() && getWorld().getFeatureAtPixel(xpos, ypos) == ft) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @return	...
	 * 			| if ( ((getXPosition() + getWidth() - 1) < obj.getXPosition()) || ((obj.getXPosition() + obj.getWidth() - 1) < getXPosition()) 
	 *			|	|| ((getYPosition() + getHeight() - 1) < obj.getYPosition()) || ((obj.getYPosition() + obj.getHeight() - 1) < getYPosition()) )
	 *			| 		then false
	 *			| else true
	 */
	public boolean overlapsWith(WorldObject obj) {
		if ( ((getXPosition() + getWidth() - 1) < obj.getXPosition()) || ((obj.getXPosition() + obj.getWidth() - 1) < getXPosition()) 
				|| ((getYPosition() + getHeight() - 1) < obj.getYPosition()) || ((obj.getYPosition() + obj.getHeight() - 1) < getYPosition()) ) {
			return false;
		}
		else
			return true;
	}
	
	/**
	 * @return	...
	 * 			|	for each obj in getWorld().getAllWorldObjects()
	 * 			| 		if (! (obj == this) && overlapsWith(obj) )
	 * 			|			then return obj
	 */
	public WorldObject getOverlappingWorldObject() {
		for (WorldObject obj : getWorld().getAllWorldObjects()) {
			if (! (obj == this) ) {
				if (overlapsWith(obj)) {
					return obj;
				}
			}
		}
		return null;
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @return	...
	 * 			| ((getYPosition()) == obj.getYPosition() + obj.getHeight() - 1))
	 */
	public boolean objectOverlapsBottom(WorldObject obj) {
		if ((getYPosition()) == obj.getYPosition() + obj.getHeight() - 1)  {
			return true;
		}
		else 
			return false;
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @return	...
	 * 			| ((getYPosition() + getHeight() - 1) == obj.getYPosition())
	 */
	public boolean objectOverlapsTop(WorldObject obj) {
		if ((getYPosition() + getHeight() - 1) == obj.getYPosition())  {
			return true;
		}
		else 
			return false;
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @return	...
	 * 			| ((getXPosition() + getWidth() - 1) == obj.getXPosition())
	 */
	public boolean objectOverlapsRight(WorldObject obj) {
		if ((getXPosition() + getWidth() - 1) == obj.getXPosition())  {
			return true;
		}
		else 
			return false;
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @return	...
	 * 			| ((getXPosition()) == obj.getXPosition() + obj.getWidth() - 1)
	 */
	public boolean objectOverlapsLeft(WorldObject obj) {
		if ((getXPosition()) == obj.getXPosition() + obj.getWidth() - 1)  {
			return true;
		}
		else 
			return false;
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @return	...
	 * 			| (obj.featureOverlapsTop(3) || obj.featureOverlapsBottom(3) || obj.featureOverlapsLeft(3) || obj.featureOverlapsRight(3))
	 */
	public boolean objectOverlapsWithMagma(WorldObject obj) {
		if (obj.featureOverlapsTop(3) || obj.featureOverlapsBottom(3) || obj.featureOverlapsLeft(3) || obj.featureOverlapsRight(3)) {
				return true;
		}
		return false;
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @return	...
	 * 			| (obj.featureOverlapsTop(2) || obj.featureOverlapsBottom(2) || obj.featureOverlapsLeft(2) || obj.featureOverlapsRight(2))
	 */
	public boolean objectOverlapsWithWater(WorldObject obj) {
		if (obj.featureOverlapsTop(2) || obj.featureOverlapsBottom(2) || obj.featureOverlapsLeft(2) || obj.featureOverlapsRight(2)) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param 	obj
	 * 			...
	 * @return	...
	 * 			| (featureOverlapsBottom(0) || featureOverlapsLeft(0) || featureOverlapsRight(0))
	 */
	public boolean objectOverlapsWithAir() {
		if (featureOverlapsBottom(0) || featureOverlapsLeft(0) || featureOverlapsRight(0)) {
			return true;
		}
		return false;
	}
	
	/**
	 * @return	...
	 * 			if (! (a==0 && v0 ==0))
	 * 				then 0.01 /(v0 + a*deltaT)
	 * @throws 	NotMovingException
	 * 			...
	 * 			| if  (a==0 && v0 ==0)
	 */
	public double getDt(double deltaT) throws NotMovingException {		
		double vy = getVerticalVelocity();
		double ay = getVerticalAcceleration();
		double vx = getHorizontalVelocity();
		double ax = getHorizontalAcceleration();
		
		double v0 = Math.sqrt(Math.pow(vx, 2)+Math.pow(vy,2));
		double a = Math.sqrt(Math.pow(ax, 2)+Math.pow(ay,2));
		
		if (! (a==0 && v0 ==0)) {
			return 0.01 /(v0 + a*deltaT);
		}
		else 
			throw new NotMovingException();
	}
	
	/**
	 * @param 	time
	 * 			...
	 * @post	new.getObjOverlapTimer() == time
	 */
	protected void setObjOverlapTimer(double time) {
		objOverlapTimer = time;
	}
	
	/**
	 * @return	...
	 * 			| objOverlapTimer
	 */
	@Basic
	protected double getObjOverlapTimer() {
		return objOverlapTimer;
	}
	protected double objOverlapTimer;
	
	/**
	 * @param 	deltaT
	 * 			...
	 */
	protected abstract void advanceTime(double deltaT) throws IllegalXPositionException, IllegalYPositionException;

	/**
	 * @return	...
	 * 			| maxNbHitPoints
	 */
	@Basic
	@Immutable
	protected abstract int getMaxNbHitPoints();
	
	/**
	 * @effect 	...
	 * 			| if (getNbHitPoints() <= 0) {
	 *			|	then terminate();
	 */
	protected void zeroHpKills() {
		if (getNbHitPoints() <= 0) {
			terminate();
		}
	}
	
	/**
	 * @return	...	
	 * 			| timeInMagma	
	 */
	@Basic
	double getTimeInMagma(){
		return this.timeInMagma;
	}
	
	/**
	 * @param 	time
	 * 			...
	 * @pre		...
	 * 			| ( time >= 0 )
	 * @post	...
	 * 			| new.getTimeInMagma() = time
	 */
	protected void setTimeInMagma(double time){
		assert( time >= 0);
		this.timeInMagma = time;
	}
	private double timeInMagma;
	
	/**
	 * 
	 * @param 	time
	 * 			...
	 * @pre		...
	 * 			| ( time >= 0 )
	 * @effect	...
	 * 			| new.getTimeInMagma() += time);
	 */
	protected void updateTimeInMagma(double time){
		assert( time >= 0);
		setTimeInMagma(getTimeInMagma() + time);
	}
	
	/**
	 * @return	...	
	 * 			| timeInWater
	 */
	@Basic
	protected double getTimeInWater(){
		return this.timeInWater;
	}
	
	/**
	 * @param 	time
	 * 			...
	 * @pre		...
	 * 			| ( time >= 0 )
	 * @post	...
	 * 			| new.getTimeInWater() = time
	 */
	protected void setTimeInWater(double time){
		assert( time >= 0);
		this.timeInWater = time;
	}
	
	/**
	 * @param 	time
	 * 			...
	 * @pre		...
	 * 			| ( time >= 0 )
	 * @effect	...
	 * 			| new.getTimeInWater() += time);
	 */
	protected void updateTimeInWater(double time){
		assert( time >= 0);
		setTimeInWater(getTimeInWater() + time);
	}
	private double timeInWater;
	
	/**
	 * 
	 * @param program
	 */
	public void setProgram(Program program) {
		if (! (this instanceof Mazub)) {
			this.program = program;
			program.setWorldObject(this);
		}
	}
	protected Program program;
	
	/**
	 * 
	 * @return
	 */
	protected Program getProgram() {
		return this.program;
	}

	public boolean hasProgram() {
		if (getProgram() != null) {
			return true;
		}
		else return false;
	}
	
	
	public void startRun(Direction d) {
		if (this instanceof Alien) {
			if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT) {
				((Alien)this).startMoveLeft();
			}
			else if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT) {
				((Alien)this).startMoveRight();
			}
		}
		else if ( this instanceof Plant) {
			if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT) {
				((Plant)this).moveLeft();
			}
			else if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT) {
				((Plant)this).moveRight();
			}
		}
		else if (this instanceof Shark) {
			if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT) {
				((Shark)this).moveLeft();
			}
			else if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT) {
				((Shark)this).moveRight();
			}
		}
		else if (this instanceof Slime) {
			if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT) {
				((Slime)this).moveLeft();
			}
			else if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT) {
				((Slime)this).moveRight();
			}
		}
	}
	
	public void stopRun(Direction d) {
		
		if (this instanceof Alien) {
			if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT) {
				((Alien)this).endMoveLeft();
			}
			else if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT) {
				((Alien)this).endMoveRight();
			}
		}
		else if ( this instanceof Plant && (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT || d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT)) {
				((Plant)this).endMove();
			}
		else if (this instanceof Shark) {
			if (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT || d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT) {
				((Shark)this).stopMoveH();
			}
			else {
				((Shark)this).stopMoveV();
			}
		}
		else if (this instanceof Slime && (d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT || d.getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT)) {
				((Slime)this).stopMove();
		}
	}
	
	
}
	



