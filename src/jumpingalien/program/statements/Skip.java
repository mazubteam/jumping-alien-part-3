package jumpingalien.program.statements;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Skip extending the abstract type Statement.
 * 
 */

public class Skip extends Statement {

	public Skip(SourceLocation loc) {
		super(loc);
	}

	@Override
	public double executeStatement(Program program, double time) {
		this.setFinished(true);
		return time - 0.001;
	}

	@Override
	public void reset() {
		setFinished(false);		
	}

}
