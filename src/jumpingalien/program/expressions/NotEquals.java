package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of NotEquals's extending the abstract class BinaryOperator.
*/
public class NotEquals extends BinaryOperator {

	public NotEquals(Expression left, Expression right, SourceLocation location) {
		super(left, right, location);
	}

	@Override
	public Expression evaluate(Program program) {
		if (getRightOperand().evaluate(program) == null || getLeftOperand().evaluate(program) == null) {
			return new Boolean(false, null);
		}
		if ((getLeftOperand().evaluate(program) instanceof Direction) && (getRightOperand().evaluate(program) instanceof Direction)) {
			return new Boolean(((((Direction) getLeftOperand().evaluate(program)).getDirectionValue()) != (((Direction) getRightOperand().evaluate(program)).getDirectionValue())),null);
		}
		if ( getRightOperand().evaluate(program) instanceof Null && (getLeftOperand().evaluate(program)) instanceof Null) {
			return new Boolean(false, null);
		}
		return new Boolean(((getLeftOperand().evaluate(program)) != (getRightOperand().evaluate(program))),null);
	}

}
