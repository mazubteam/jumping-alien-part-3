package jumpingalien.model;

import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import jumpingalien.util.Sprite;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 * 
 * @version 1.0
 * 
 * A class of Sharks extending the abstract type WorldObject.
 * @invar	...
 * 			| isValidVerticalVelocity(getVerticalVelocity())
 * @invar	...
 * 			| isValidHorizontalVelocity(getHorizontalVelocity())
 * @invar	...
 * 			| isValidVerticalAcceleration(getVerticalAcceleration())
 * @invar	...
 * 			| isValidHorizontalAcceleration(getHorizontalAcceleration())
 */
public class Shark extends WorldObject {
	
	/**
	 * @param 	sprite
	 * 			...
	 * @effect 	...
	 * 			| setImages(sprite)
	 * @effect 	...
	 * 			| setMaxHorizontalVelocity(4.0)
	 * @effect	...
	 * 			| setSpriteIndex(0)
	 * @effect	...
	 * 			| setNbHitPoints(100)
	 * @effect	...
	 * 			| setVerticalAcceleration(0.0)
	 */
	public Shark(Sprite[] sprite) {
		setMaxHorizontalVelocity(4.0);
		setImages(sprite);
		setSpriteIndex(0);
		setNbHitPoints(100);
		setVerticalAcceleration(0.0);
	}
	
	/**
	 * @return	...
	 * 			| maxNbHitPoints			
	 */
	@Override
	@Basic
	@Immutable
	protected int getMaxNbHitPoints() {
		return maxNbHitPoints;
	}
	private final static int maxNbHitPoints = 100;
	
	/**
	 * @param 	velocity
	 * 			...
	 * @return 	...	
	 * 			| (velocity <= 4.0) && (velocity > 0)
	 */
	@Override
	protected boolean isValidHorizontalVelocity(double velocity) {
		if (velocity == Double.POSITIVE_INFINITY || velocity == Double.NEGATIVE_INFINITY || velocity == Double.NaN) {
			 return false;
		}
		return (velocity <= 4.0) && (velocity >= 0);
	}
	
	/**
	 * @param 	velocity
	 * 			...
	 * @return 	...	
	 * 			| true
	 */
	@Override
	protected boolean isValidVerticalVelocity(double velocity) {
		if (velocity == Double.POSITIVE_INFINITY || velocity == Double.NEGATIVE_INFINITY || velocity == Double.NaN) {
			 return false;
		}
		return true;
	}
	
	/**
	 * @param 	acc
	 * 			...
	 * @return 	...	
	 * 			| ((acc == 1.5) || (acc == 0.0))
	 */
	@Override
	protected boolean isValidHorizontalAcceleration(double acc) {
		if (acc == Double.POSITIVE_INFINITY || acc == Double.NEGATIVE_INFINITY || acc == Double.NaN) {
			 return false;
		}
		return ((acc == 1.5) || (acc == 0.0));
	}

	/**
	 * @param 	acc
	 * 			...
	 * @return 	...	
	 * 			| ((acc == -10.0) || (acc == 0.0) || (acc >= -0.2 && acc <= 0.2) )
	 */
	@Override
	protected boolean isValidVerticalAcceleration(double acc) {
		if (acc == Double.POSITIVE_INFINITY || acc == Double.NEGATIVE_INFINITY || acc == Double.NaN) {
			 return false;
		}
		return ((acc == -10.0) || (acc == 0.0) || (acc >= -0.2 && acc <= 0.2) );
	}

	/**
	 * @effect	...
	 * 			| setHorizontalAcceleration(1.5)
	 * @effect	...
	 * 			| setHorizontalVelocity(0.0)
	 * @effect	... 
	 * 			| setOrientation(Orientation.LEFT)
	 * @effect 	...
	 * 			| new.getSpriteIndex() == 0
	 * @effect	...
	 * 			| diveOrRise()
	 * 
	 */
	void moveLeft() {
		setMoveCount(getMoveCount() + 1);
		Random r = new Random();
		setMovementTime(1.0 + (4.0 - 1.0) * r.nextDouble());
		diveOrRise();
		setOrientation(Orientation.LEFT);
		setHorizontalAcceleration(1.5);
		setHorizontalVelocity(0.0);
		setSpriteIndex(0);
	}
	
	/**
	 * @effect	...
	 * 			| setOrientation(Orientation.RIGHT)
	 * @effect	...
	 * 			| setHorizontalAcceleration(1.5)
	 * @effect	...
	 * 			| setHorizontalVelocity(0.0)
	 * @effect 	...
	 * 			| new.getSpriteIndex() == 1
	 * @effect	...
	 * 			| diveOrRise()
	 * 
	 */
	void moveRight() {
		setMoveCount(getMoveCount()+1);
		Random r = new Random();
		setMovementTime(1.0 + (4.0 - 1.0) * r.nextDouble());
				
		diveOrRise();	
		setOrientation(Orientation.RIGHT);
		setHorizontalAcceleration(1.5);
		setHorizontalVelocity(0.0);
		setSpriteIndex(1);
	}
	
	void stopMoveH() {
		setHorizontalAcceleration(0.0);
		setHorizontalVelocity(0.0);
	}
	
	void stopMoveV() {
		setVerticalAcceleration(0.0);
		setVerticalVelocity(0.0);
	}
	
	/**
	 * @return	...
	 * 			moveCount
	 */
	@Basic
	private int getMoveCount() {
		return moveCount;
	}
	
	/**
	 * @param 	cnt
	 * 			...
	 * @post	new.getMoveCount() == cnt
	 */
	private void setMoveCount(int cnt) {
		moveCount = cnt;
	}
	private int moveCount;
	
	/**
	 * @return	...
	 * 			movementTime
	 */
	@Basic
	private double getMovementTime() {
		return movementTime;
	}
	
	/**
	 * @param 	time
	 * 			...
	 * @post 	new.getMovementTime() = time
	 */
	private void setMovementTime(double time)
	{
		movementTime = time;
	}
	private double movementTime;
	
	/**
	 * @effect	...
	 * 			| if (decision) {
	 *			|		if (! featureOverlapsRight(1)) {
	 *			|			then moveRight();
	 *			|		else moveLeft();
	 *			| else if (! featureOverlapsLeft(1))
	 *			|		then moveLeft();
	 *			| else moveRight()		
	 */
	private void nextMove() {
		Random r = new Random();
		boolean decision = r.nextBoolean();
		if (decision) {
			if (! featureOverlapsRight(1)) {
				moveRight();
			}
			else moveLeft();
		}
		else if (! featureOverlapsLeft(1)) {
			moveLeft();
		}
		else moveRight();
	}
	
	/**
	 * @effect	...
	 * 			| if (featureOverlapsBottom(1) || featureOverlapsBottom(2) ) {
	 *			|	then setVerticalVelocity(2.0);
	 *			|		 setVerticalAcceleration(-10.0)
	 */
	public void startJump() {
		if (featureOverlapsBottom(1) || featureOverlapsBottom(2) ) {
			setVerticalVelocity(2.0);
			setVerticalAcceleration(-10.0);
		}
	}
	
	/**
	 * @effect	...
	 * 			| setVerticalVelocity(0.0)		
	 */
	public void endJump() {
		setVerticalVelocity(0.0);
	}

	/**
	 * @effect	...
	 * 			| if ( (randAcc <= 0 && !featureOverlapsBottom(1)) || (randAcc >= 0 && !featureOverlapsTop(1) && !featureOverlapsRight(0)) ) {
	 *					then setVerticalAcceleration(randAcc);
	 */
	private void diveOrRise() {
		Random r = new Random();
		double randAcc = -0.2 + (0.4 - -0.2) * r.nextDouble();
		if ( (randAcc <= 0 && !featureOverlapsBottom(1)) || (randAcc >= 0 && !featureOverlapsTop(1) && !featureOverlapsRight(0)) ) {
			setVerticalAcceleration(randAcc);
		}
	}

	/**
	 * @param 	dt
	 * 			...
	 * @effect	...
	 * 			| if (! isTerminated()) 
	 *			| 	 then airDamage(dt);
	 *			|		  updateXPosition(dt)
	 *			|		  updateYPosition(dt)
	 *			|		  updateXVelocity(dt)
	 *			|		  new.getVerticalVelocity() == getVerticalVelocity() + getVerticalAcceleration()*dt	  
	 * @effect	...
	 * 			| if (getVerticalAcceleration() == -10.0 && featureOverlapsTop(2))
	 * 			|		then setVerticalAcceleration(0.0)
	 * 			|			 setVerticalVelocity(0.0);	
	 * @effect	...
	 * 			| if (getMoveCount() > 4 && featureOverlapsTop(0) && getWorld().getFeatureAtPixel(getXPosition(), getYPosition() + getHeight()) == 2)
	 * 			|		then startJump()
	 * @effect	...
	 * 			| if (getMovementTimer() >= getMovementTime()) 
	 * 			| 		then endJump()
	 * 			|		if ( !featureOverlapsRight(0) )
	 * 			|			then setVerticalAcceleration(0.0);
	 * 			|				 nextMove()
	 */
	@Override
	protected void advanceTime(double dt) throws IllegalXPositionException, IllegalYPositionException {
		setMovementTimer(getMovementTimer() + dt);
		if (! isTerminated()) {
			airDamage(dt);
			updateXPosition(dt);
			updateYPosition(dt);
			updateXVelocity(dt);
			setVerticalVelocity(getVerticalVelocity() + getVerticalAcceleration()*dt);
		}
		if (getVerticalAcceleration() == -10.0 && featureOverlapsTop(2)) {
			setVerticalAcceleration(0.0);
			setVerticalVelocity(0.0);
		}
		
		if (! hasProgram()) {
			if (getMoveCount() > 4 && featureOverlapsTop(0) && getWorld().getFeatureAtPixel(getXPosition(), getYPosition() + getHeight()) == 2) {
				startJump();
				setMoveCount(0);
			}
			if (getMovementTimer() >= getMovementTime()) {
				endJump();		
				if ( !featureOverlapsRight(0) ) {
					setVerticalAcceleration(0.0);
					nextMove();
					setMovementTimer(0);
				}
			}
		}
		
	}
	
	/**
	 * 
	 * @return	...
	 * 			| movementTimer
	 */
	@Basic
	private double getMovementTimer() {
		return movementTimer;
	}
	
	/**
	 * @param 	t
	 * 			...
	 * @post	...
	 * 			| new.getMovementTimer() == t
	 */
	private void setMovementTimer(double t) {
		movementTimer = t;
	}
	private double movementTimer;

	// subfuncties van advanceTime()
	/**
	 * @param 	dt
	 * 			...
	 * @effect 	...	
	 *			| if ( ((getHorizontalVelocity() + getHorizontalAcceleration() * dt) <= getMaxHorizontalVelocity()) && (!(featureOverlapsRight(1)||featureOverlapsLeft(1)) ) )
	 *			|	then setHorizontalVelocity(getHorizontalVelocity() + getHorizontalAcceleration() * dt)
	 *			| else if ( ((getHorizontalVelocity() + getHorizontalAcceleration() * dt) > getMaxHorizontalVelocity()) && (!(featureOverlapsRight(1)||featureOverlapsLeft(1))) )
	 *			|	then setHorizontalVelocity(getMaxHorizontalVelocity())
	 */
	private void updateXVelocity(double dt) {
		if ( ((getHorizontalVelocity() + getHorizontalAcceleration() * dt) <= getMaxHorizontalVelocity()) && (!(featureOverlapsRight(1)||featureOverlapsLeft(1)) ) ) {
			setHorizontalVelocity(getHorizontalVelocity() + getHorizontalAcceleration() * dt);
		}
		else if ( ((getHorizontalVelocity() + getHorizontalAcceleration() * dt) > getMaxHorizontalVelocity()) && (!(featureOverlapsRight(1)||featureOverlapsLeft(1))) ) {
			setHorizontalVelocity(getMaxHorizontalVelocity());
		}
	}
	
	
	/**
	 * @param 	dt
	 * 			...
	 * @throws 	IllegalYPositionException
	 * 			...
	 * 			| if (! isValidYPosition(getYPositionDouble() + distanceTravelled))
	 * @effect	...
	 * 			| if ( featureOverlapsTop(1) && distanceTravelled < 0)
	 * 			|	then	setYPosition(getYPositionDouble() + 100 * getVertDistTravelled(dt));
	 *			| else if (featureOverlapsBottom(1) && distanceTravelled > 0)
	 *			|	then	setYPosition(getYPositionDouble() + distanceTravelled);
	 * 			| else if ( ! (featureOverlapsTop(1) || featureOverlapsBottom(1) || (featureOverlapsTop(0) && (getVerticalAcceleration()!=-10.0))  ))
	 * 			|   then	setYPosition( getYPositionDouble() + 100 * getVertDistTravelled(dt););
	 */
	private void updateYPosition(double dt) throws IllegalYPositionException {
		
		double distanceTravelled = (100 * getVertDistTravelled(dt));
		if ( featureOverlapsTop(1) && distanceTravelled < 0) {
			setYPosition(getYPositionDouble() + distanceTravelled);
		}
		else if (featureOverlapsBottom(1) && distanceTravelled > 0) {
			setYPosition(getYPositionDouble() + distanceTravelled);
		}
		else if ( ! (featureOverlapsTop(1) || featureOverlapsBottom(1) || (featureOverlapsTop(0) && (getVerticalAcceleration()!=-10.0))  ))  {
			setYPosition(getYPositionDouble() + distanceTravelled);
		}
	}
	
	/**
	 *
	 * @param	deltaT
	 * 			...
	 * @throws 	IllegalXPositionException
	 * 			...
	 * 			| if (! isValidXPosition(getXPositionDouble() + distanceTravelled))
	 * @effect 	...
	 * 			| if ((getOrientation() == Orientation.RIGHT))
	 * 			| 	then setXPosition(getXPositionDouble() + 100 * getHorizDistTravelled(deltaT))
	 * @effect	...
	 * 			| if ((getOrientation() == Orientation.LEFT))
	 * 			| 	then setXPosition(getXPositionDouble() - 100 * getHorizDistTravelled(deltaT))
	 */
	private void updateXPosition(double deltaT) throws IllegalXPositionException {
		if (getOrientation() == Orientation.RIGHT) {
			double distanceTravelled = (100 * getHorizDistTravelled(deltaT));
			if (!isTerminated() && !featureOverlapsRight(1) ) {
				setXPosition(getXPositionDouble() + distanceTravelled);
			} 
		}
		else if (getOrientation() == Orientation.LEFT) {
			double distanceTravelled = - 100 * getHorizDistTravelled(deltaT);
			if (!isTerminated() && !featureOverlapsLeft(1) ) {
				setXPosition( getXPositionDouble() + distanceTravelled);
			}
		}	
	}
	
	/**
	 * @param 	deltaT
	 * 			...
	 * @return	...
	 * 			| (getHorizontalVelocity() * deltaT + 0.5 * getHorizontalAcceleration() * Math.pow(deltaT, 2))	
	 */
	private double getHorizDistTravelled(double deltaT) { 
		return (getHorizontalVelocity() * deltaT + 0.5 * getHorizontalAcceleration() * Math.pow(deltaT,2));
	}
	
	/**
	 * @param 	deltaT
	 * 			...
	 * @return	...
	 * 			| (getVerticalVelocity() * deltaT + 0.5 * getVerticalAcceleration() * Math.pow(deltaT,2) )
	 * 			
	 */
	private double getVertDistTravelled(double deltaT) { 
		return (getVerticalVelocity() * deltaT + 0.5 * getVerticalAcceleration() * Math.pow(deltaT,2) );
	}
	
	/**
	 * @param 	dt
	 * 			...
	 * @effect	...	
	 * 			| if (!isTerminated() && objectOverlapsWithAir())
	 * 			|	then updateTimeInAir(dt);
	 * 			| if (getTimeInAir() >= 0.2)
	 * 			|	then	addHP(-6);
	 *			|			zeroHpKills();
	 *			|			setTimeInAir(0.0);
	 */
	private void airDamage(double dt) {
		if (!isTerminated() && objectOverlapsWithAir()) {
			updateTimeInAir(dt);
			if (getTimeInAir() >= 0.2) {
				addHP(-6);
				zeroHpKills();
				setTimeInAir(0.0);
			}
		}
	}
	
	/**
	 * @return	...
	 * 			| this.timeInAir
	 */
	@Basic
	private double getTimeInAir(){
		return this.timeInAir;
	}
	
	/**
	 * @param	time
	 * 			...
	 * @pre		...
	 * 			| if (time >= 0)
	 * @post	...
	 *			| this.timeInAir = time
	 */
	private void setTimeInAir(double time){
		assert( time >= 0);
		this.timeInAir = time;
	}
	
	/**
	 *
	 * @param 	time
	 * 			...
	 * @pre		...
	 * 			| if (time >= 0)
	 * @post	...
	 *			| this.timeInAir += time
	 */
	private void updateTimeInAir(double time){
		assert( time >= 0);
		setTimeInAir(getTimeInAir() + time);
	}
	private double timeInAir;
}