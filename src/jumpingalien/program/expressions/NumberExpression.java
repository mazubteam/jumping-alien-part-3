package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A abstract class of NumberExpressions.
*/
public abstract class NumberExpression extends Expression {
	
	public NumberExpression(double value, SourceLocation location) {
		super(location);
		this.value = value;
	}

	public double getValue() {
		return this.value;
	}
	private final double value;
}
