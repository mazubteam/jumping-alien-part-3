package jumpingalien.model;

import java.util.Random;

public enum Orientation {
	RIGHT, LEFT;

	public static Orientation randomOrientation(){
	    Orientation[] orientation = Orientation.values();
	    Random generator = new Random();
	    return orientation[generator.nextInt(orientation.length)];
	    
	}
}
