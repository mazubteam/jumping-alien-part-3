package jumpingalien.part2.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.School;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.part2.facade.Facade;
import jumpingalien.part2.facade.IFacadePart2;

import org.junit.Before;
import org.junit.Test;

public class SchoolTest {
	
	private Slime testSlime1;
	private Slime testSlime2;
	private Slime testSlime3;
	private Slime testSlime4;
	private School testSchool = new School();
	private School testSchool2 = new School();
	private IFacadePart2 facade;
	private World world;

	@Before
	public void setUp() throws Exception {
		facade = new Facade();
		world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		
		facade.setGeologicalFeature(world, 0, 0, 1);
		testSlime1 = facade.createSlime(200, 499, spriteArrayForSize(3, 3), testSchool);
		testSlime2 = facade.createSlime(0, 499, spriteArrayForSize(3, 3), testSchool);
		testSlime3 = facade.createSlime(100, 499, spriteArrayForSize(3, 3), testSchool);
		testSlime4 = facade.createSlime(150, 499, spriteArrayForSize(3, 3), testSchool2);
		
	}

	@Test
	public void testNbSlimes() {
		
		assertTrue(testSchool.getNbSlimes() == 3);
		assertTrue(testSchool2.getNbSlimes() == 1);
	}
	
	@Test
	public void addSlime() {
		facade.addSlime(world, testSlime1);
		facade.addSlime(world, testSlime2);
		facade.addSlime(world, testSlime3);
		facade.addSlime(world, testSlime4);
		
		assertTrue(testSchool.hasAsSlime(testSlime1));
		assertTrue(testSchool.hasAsSlime(testSlime2));
		assertTrue(testSchool.hasAsSlime(testSlime3));
		assertTrue(testSlime1.hasAsSchool(testSchool));
		assertTrue(testSlime2.hasAsSchool(testSchool));
		assertTrue(testSlime3.hasAsSchool(testSchool));
		
		assertTrue(testSchool2.hasAsSlime(testSlime4));
		assertTrue(testSlime4.hasAsSchool(testSchool2));
	}
	
	@Test
	public void changeSchool() {
		testSlime3.changeToSchool(testSchool2);
		assertTrue(testSchool.getNbSlimes() == 2);
		assertTrue(testSchool2.getNbSlimes() == 2);
		assertFalse(testSchool.hasAsSlime(testSlime3));
		assertTrue(testSchool2.hasAsSlime(testSlime3));
		assertTrue(testSlime3.hasAsSchool(testSchool2));
		assertFalse(testSlime3.hasAsSchool(testSchool));
	}
	
	@Test
	public void hasProperSlimes() {
		assertTrue(testSchool.hasProperSlimes());
		assertTrue(testSchool2.hasProperSlimes());
	}
	
	

}
