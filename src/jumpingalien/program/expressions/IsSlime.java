package jumpingalien.program.expressions;

import jumpingalien.model.Slime;
import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of IsSlimes extending the abstract class UnaryOperator.
*/
public class IsSlime extends UnaryOperator {

	public IsSlime(Expression operand, SourceLocation location) {
		super(operand, location);
	}

	@Override
	public Expression evaluate(Program program) {
		return new Boolean(((WorldObject) (getOperand().evaluate(program))) instanceof Slime,null);
	}
}
