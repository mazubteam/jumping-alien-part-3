package jumpingalien.program.expressions;

import jumpingalien.model.World;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of GetTiles extending the abstract class BinaryOperator.
*/
public class GetTile extends BinaryOperator {

	public GetTile(Expression left, Expression right, SourceLocation location) {
		super(left, right, location);
	}
	
	@Override
	public Object evaluate(Program program) {
		World world = program.getWorldObject().getWorld();
		return world.getTileAtPixel((int)((NumberExpression)getLeftOperand().evaluate(program)).getValue(), (int)((NumberExpression)getRightOperand().evaluate(program)).getValue());
	}
}
