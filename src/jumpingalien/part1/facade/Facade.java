package jumpingalien.part1.facade;

import jumpingalien.model.IllegalXPositionException;
import jumpingalien.model.IllegalYPositionException;
import jumpingalien.model.Mazub;
import jumpingalien.util.ModelException;
import jumpingalien.util.Sprite;

public class Facade implements IFacade {
		
	public Facade() {		
	}
	
	public Mazub createMazub(int pixelLeftX, int pixelBottomY, Sprite[] sprites) throws ModelException  {
			Mazub alien = new Mazub(sprites);
			alien.setInitialXPosition(pixelLeftX);
			alien.setInitialYPosition(pixelBottomY);
			return alien;
	}

	public int[] getLocation(Mazub alien) {
		int[] pos = { alien.getXPosition(), alien.getYPosition() };
		return pos;
	}

	public double[] getVelocity(Mazub alien) {
		double[] vel = { alien.getHorizontalVelocity(), alien.getVerticalVelocity() };
		return vel;
	}

	public double[] getAcceleration(Mazub alien) {
		double[] acc = { alien.getHorizontalAcceleration(), alien.getVerticalAcceleration() };
		return acc;
	}

	public int[] getSize(Mazub alien) {
		int[] siz = { alien.getWidth(), alien.getHeight() };
		return siz;
	}

	public Sprite getCurrentSprite(Mazub alien) {
		return alien.getCurrentSprite();
	}

	public void startJump(Mazub alien) {
		try {
			alien.startJump();
		} catch (Exception e) {
			//
		}
	}

	public void endJump(Mazub alien) {
		try {
			alien.endJump();
		}
		catch (Exception e) {
		}
	}

	public void startMoveLeft(Mazub alien) {
		if (alien.isMovingRight()) {
			alien.endMoveRight();
		}
		alien.startMoveLeft();		
	}

	public void endMoveLeft(Mazub alien) {
		if (! alien.isMovingRight()) {
			alien.endMoveLeft();	
		}
	}

	public void startMoveRight(Mazub alien) {
		if (alien.isMovingLeft()) {
			alien.endMoveLeft();
		}
		alien.startMoveRight();	
	}

	public void endMoveRight(Mazub alien) {
		if (! alien.isMovingLeft()) {
			alien.endMoveRight();	
		}
	}

	public void startDuck(Mazub alien) {
		try { 
			alien.startDuck();
		}
		catch (Exception e) {
		}
	}
	
	public void endDuck(Mazub alien) {
		try {
			alien.endDuck();
		}
		catch (Exception e) {	
		}
	}

	public void advanceTime(Mazub alien, double dt) {
		try {
			alien.advanceTime(dt);
		} catch (IllegalXPositionException | IllegalYPositionException e) {
		}

	}
}
