package jumpingalien.program.statements;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
import jumpingalien.program.expressions.Direction;
import jumpingalien.program.expressions.Expression;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of StopRun extending the abstract type Statement.
 * 
 */

public class StopRun extends Statement {

	public StopRun(SourceLocation loc, Expression direction) {
		super(loc);
		this.direction = direction;
	}

	@Override
	public double executeStatement(Program program, double time) {
		program.getWorldObject().stopRun((Direction) getDirection().evaluate(program));
		this.setFinished(true);
		return time - 0.001;
	}

	private Expression getDirection() {
		return direction;
	}

	private final Expression direction;

	@Override
	public void reset() {
		setFinished(false);		
	}


}
