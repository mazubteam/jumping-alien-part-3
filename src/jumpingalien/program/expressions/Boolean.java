package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of Booleans.
*/
public class Boolean extends Expression {

	public Boolean(boolean bool, SourceLocation location) {
		super(location);
		this.booleanValue = bool;
	}

	public boolean getBooleanValue() {
		return this.booleanValue;
	}
	private final boolean booleanValue;

	@Override
	public Expression evaluate(Program program) {
		return this;
	}
	
	@Override
	public String toString() {
		if (getBooleanValue()) {
			return "true";
		}
		return "false";
	}
}
