package jumpingalien.part2.tests;
import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.IllegalXPositionException;
import jumpingalien.model.IllegalYPositionException;
import jumpingalien.model.Mazub;
import jumpingalien.model.World;
import jumpingalien.part2.facade.Facade;
import jumpingalien.part2.facade.IFacadePart2;
import jumpingalien.util.Sprite;
import org.junit.Before;
import org.junit.Test;


/**
 * 
 * @author Lemmens U. & Schuermans J.
 * @version 1.0
 */
public class MazubTest {
	
	private Mazub testAlien_1;

	@Before
	public void setUp() throws Exception {
		IFacadePart2 facade = new Facade();
		
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		testAlien_1 = facade.createMazub(0, 499, spriteArrayForSize(3, 3));
		facade.setMazub(world, testAlien_1);
	}
	
	@Test
	public void constructor() {
		Sprite[] sprites = spriteArrayForSize(3,3);
		
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		Mazub alien = facade.createMazub(0, 499, sprites);
		facade.setMazub(world, alien);
		
		assertSame(sprites, alien.getImages());
		assertEquals(0, alien.getSpriteIndex());
		assertTrue(alien.getVerticalAcceleration() == 0.0);
		assertTrue(alien.getHorizontalAcceleration() == 0.0);
	}
	
	@Test
	public void getDimensions() {
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		Sprite[] sprites = spriteArrayForSize(66, 92, 30);
		Mazub alien = facade.createMazub(0, 499, sprites);

		
		facade.setMazub(world, alien);
		assertEquals(66, alien.getWidth());
		assertEquals(92, alien.getHeight());
	}
	
	@Test
	public void isMovingRight() {
		testAlien_1.startMoveRight();
		assertTrue(testAlien_1.isMovingRight());
		testAlien_1.endMoveRight();
	}
	
	@Test
	public void isMovingLeft() {
		testAlien_1.startMoveLeft();
		assertTrue(testAlien_1.isMovingLeft());
		testAlien_1.endMoveLeft();
	}
	
	@Test
	public void startRight() {
		testAlien_1.startMoveRight();
		assertTrue(testAlien_1.getHorizontalVelocity() == 1.0);
		assertTrue(testAlien_1.getHorizontalAcceleration() == 0.9);
		assertEquals(8, testAlien_1.getSpriteIndex());
		testAlien_1.endMoveRight();
	}
	
	@Test
	public void startLeft() {
		testAlien_1.startMoveLeft();
		assertTrue(testAlien_1.getHorizontalVelocity() == 1.0);
		assertTrue(testAlien_1.getHorizontalAcceleration() == 0.9);
		assertEquals(19, testAlien_1.getSpriteIndex());	
		testAlien_1.endMoveLeft();
	}
	
	@Test
	public void endRight() throws IllegalXPositionException, IllegalYPositionException {
		testAlien_1.startMoveRight();
		testAlien_1.advanceTime(0.1);
		testAlien_1.endMoveRight();
		assertTrue(testAlien_1.getHorizontalVelocity() == 0.0);
		assertTrue(testAlien_1.getHorizontalAcceleration() == 0.0);
	}
	@Test
	public void endLeft() throws IllegalXPositionException, IllegalYPositionException {
		testAlien_1.startMoveLeft();
		testAlien_1.advanceTime(0.1);
		testAlien_1.endMoveLeft();
		assertTrue(testAlien_1.getHorizontalVelocity() == 0.0);
		assertTrue(testAlien_1.getHorizontalAcceleration() == 0.0);
	}	

	@Test
	public void startJump() throws Exception {
		testAlien_1.startJump();
		assertTrue(testAlien_1.getVerticalVelocity() == 8.0);
		assertTrue(testAlien_1.getVerticalAcceleration() == -10.0);
	}
	
	@Test
	public void endJump() throws Exception {
		testAlien_1.startJump();
		testAlien_1.getWorld().advanceTime(0.1);
		testAlien_1.endJump();
		assertTrue(testAlien_1.getVerticalVelocity() == 0.0);
		assertTrue(testAlien_1.getVerticalAcceleration() == -10.0);
	}

	@Test
	public void getSprite() throws Exception {
		Sprite[] sprites = spriteArrayForSize(7,7,30);
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		Mazub alien = facade.createMazub(0, 499, sprites);
		facade.setMazub(world, alien);
		
		assertSame(sprites[0], alien.getCurrentSprite());
		alien.startJump();
		for (int i = 0; i < 10; i++) {
			facade.advanceTime(world, 0.1);
		}
		assertSame(sprites[0], alien.getCurrentSprite());
		alien.startMoveRight();
		for (int i = 0; i < 5; i++) {
			facade.advanceTime(world, 0.1);
		}
		assertSame(sprites[4], alien.getCurrentSprite());
		alien.endMoveRight();
		for (int i = 0; i < 5; i++) {
			facade.advanceTime(world, 0.1);
		}
		assertTrue(alien.getYPosition() == 499);
		assertSame(sprites[2], alien.getCurrentSprite());
		alien.startMoveLeft();
		assertSame(sprites[9+alien.calculateM()], alien.getCurrentSprite());
		for (int i = 0; i < 5; i++) {
			facade.advanceTime(world, 0.075);
		}
		assertSame(sprites[9+alien.calculateM()+5], alien.getCurrentSprite());
		alien.startDuck();
		
		for (int i = 0; i < 5; i++) {
			facade.advanceTime(world, 0.075);
		}
		assertSame(sprites[7], alien.getCurrentSprite());
		alien.startMoveRight();
		assertSame(sprites[6], alien.getCurrentSprite());
	}
	
	@Test
	public void advanceTime_jump() throws Exception {
		Sprite[] sprites = spriteArrayForSize(3,3);
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		Mazub alien = facade.createMazub(0, 499, sprites);
		facade.setMazub(world, alien);
		
		alien.startJump();
		for (int i = 0; i < 10; i++) {
			facade.advanceTime(world, 0.1);
		}
		assertTrue(alien.getXPosition() == 0.0);
		assertTrue(alien.getYPosition() > 499 + (int) 100 * (8 * 1 - 0.5 * 10 * Math.pow(1, 2)) - 5 );
		assertTrue(alien.getYPosition() < 499 + (int) 100 * (8 * 1 - 0.5 * 10 * Math.pow(1, 2)) + 5 );
		assertTrue(alien.getHorizontalVelocity() == 0.0);
		assertTrue(alien.getHorizontalAcceleration() == 0.0);
		assertTrue(alien.getVerticalVelocity() < (8.0 - 10 * 1.0) + 0.1 );
		assertTrue(alien.getVerticalVelocity() > (8.0 - 10 * 1.0) - 0.1 );
		assertTrue(alien.getVerticalAcceleration() == -10.0);
		assertTrue(alien.getSpriteIndex() == 0);
	}
	
	public void advanceTime_run() throws Exception {
		Sprite[] sprites = spriteArrayForSize(3,3);
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		Mazub alien = facade.createMazub(0, 499, sprites);
		facade.setMazub(world, alien);
		alien.startMoveRight();
		for (int i = 0; i < 10; i++) {
			facade.advanceTime(world, 0.1);
		}
		assertTrue(alien.getYPosition() == 0.0);
		assertTrue(alien.getYPosition() == (int) (1 * 1 + 0.5 * 0.9 * Math.pow(1,2) ));
		assertTrue(alien.getHorizontalVelocity() == 1.9);
		assertTrue(alien.getHorizontalAcceleration() == 0.9);
		assertTrue(alien.getVerticalVelocity() == 0.0);
		assertTrue(alien.getVerticalAcceleration() == 0.0);
	}
}
