package jumpingalien.part2.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.School;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.part2.facade.Facade;
import jumpingalien.part2.facade.IFacadePart2;

import org.junit.Before;
import org.junit.Test;

public class WorldTest {

	private World world;
	private Mazub alien;
	private Slime slime;
	private Plant plant;
	private IFacadePart2 facade;
	private School school = new School();
	
	@Before
	public void setUp() throws Exception {
		facade = new Facade();
		
		world = facade.createWorld(20, 1, 2, 1, 1, 0, 0);
		facade.setGeologicalFeature(world, 0, 0, 1);
		facade.setGeologicalFeature(world, 0, 1, 0);
		alien = facade.createMazub(11, 19, spriteArrayForSize(7, 7));
		plant = facade.createPlant(12, 21, spriteArrayForSize(7, 7));
		slime = facade.createSlime(13, 19, spriteArrayForSize(3, 3), school);
		facade.setMazub(world, alien);
		facade.addSlime(world, slime);
		facade.addPlant(world, plant);
	}
	
	@Test
	public void getDimensions() {
		assertTrue(world.getXDimension() == 20);
		assertTrue(world.getYDimension() == 20 * 2);
		assertTrue(world.getTileLength() == 20);
		assertTrue(world.getTargetTileX() == 0);
		assertTrue(world.getTargetTileY() == 0);
	}
	
	@Test
	public void getWindowDim() {
		assertTrue(world.getWindowBottom() == 0);
		assertTrue(world.getWindowLeft() == 0);
		assertTrue(world.getVisibleWindowHeight() == 1);
		assertTrue(world.getVisibleWindowWidth() == 1);	
	}
	
	@Test
	public void getFtAtPixel() {
		assertEquals(world.getFeatureAtPixel(0,0),1);
		assertEquals(world.getFeatureAtPixel(12,20),0);
	}

	@Test
	public void setFtAtTile() {
		
		world.setFeatureAtTile(0, 0, 2);
		world.setFeatureAtTile(0, 1, 3);
		
		assertEquals(world.getFeatureAtPixel(0,0),2);
		assertEquals(world.getFeatureAtPixel(12,31),3);
		
		world.setFeatureAtTile(0, 0, 1);
		world.setFeatureAtTile(0, 1, 0);
	}
	
	@Test
	public void getTile() {
		assertTrue(world.getTile(0,0)[0] == new int[] {0,0}[0]);
		assertTrue(world.getTile(0,0)[1] == new int[] {0,0}[1]); 
		assertTrue(world.getTile(12,20)[0] == new int[] {0,1}[0]);
		assertTrue(world.getTile(12,20)[1] == new int[] {0,1}[1]);
	}
	
	@Test
	public void getNbMazubs() {
		assertTrue(world.getNbMazubs() == 1);
		alien.terminate();
		assertTrue(world.getNbMazubs() == 0);
		alien = facade.createMazub(0, 19, spriteArrayForSize(3, 3));
		facade.setMazub(world, alien);
		assertTrue(world.getNbMazubs() == 1);
	}
	
	@Test
	public void advanceTime() {
		slime.terminate();
		for (int i = 0; i < 50; i++) {
			world.advanceTime(0.1);
		}
		assertTrue(alien.getNbHitPoints() == 150);
		assertTrue(plant.isTerminated());	
	}
	
}
