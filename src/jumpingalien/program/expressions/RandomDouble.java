package jumpingalien.program.expressions;

import java.util.Random;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of RandomDoubles extending the abstract class NumberExpression.
*/
public class RandomDouble extends NumberExpression {

	public RandomDouble(Expression value, SourceLocation location) {
		super(((NumberExpression) value.evaluate(null)).getValue(), location);
	}

	@Override
	public Expression evaluate(Program program) {
		Random r = new Random();
		return new DoubleConstant(Math.abs(getValue()*r.nextDouble()),null);
	}

}
