package jumpingalien.part2.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*; 
import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.World;
import jumpingalien.part2.facade.Facade;
import jumpingalien.part2.facade.IFacadePart2;
import jumpingalien.util.Sprite;

import org.junit.Before;
import org.junit.Test;

public class PlantTest {
	
	private Plant testPlant;

	@Before
	public void setUp() throws Exception {
		IFacadePart2 facade = new Facade();
		
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		testPlant = facade.createPlant(0, 499, spriteArrayForSize(3, 3));
		facade.addPlant(world, testPlant);
	}
	
	@Test
	public void constructor() {
		Sprite[] sprites = spriteArrayForSize(3,3);
		
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		Plant plant = facade.createPlant(0, 499, sprites);
		facade.addPlant(world, plant);
		
		assertSame(sprites, plant.getImages());
		assertEquals(0, plant.getSpriteIndex());
		assertTrue(plant.getVerticalAcceleration() == 0.0);
		assertTrue(plant.getHorizontalAcceleration() == 0.0);
	}
	
	@Test
	public void advanceTime_moving() throws Exception {
		
		
		
		Sprite[] sprites = spriteArrayForSize(3,3);
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		Mazub alien = facade.createMazub(0, 0, spriteArrayForSize(3, 3));
		facade.setMazub(world, alien);
		facade.setGeologicalFeature(world, 0, 0, 1);
		Plant plant = facade.createPlant(50, 499, sprites);
		facade.addPlant(world, plant);
		for (int i = 0; i < 5 ; i++) {
			world.advanceTime(0.1);
		}
		assertTrue(plant.getYPosition() == 499);
		assertTrue(plant.getXPosition() == 25);
		assertTrue(plant.getHorizontalVelocity() == 0.5);
		assertTrue(plant.getHorizontalAcceleration() == 0.0);
		assertTrue(plant.getVerticalVelocity() == 0.0);
		assertTrue(plant.getVerticalAcceleration() == 0.0);
	}
}
	
	


	