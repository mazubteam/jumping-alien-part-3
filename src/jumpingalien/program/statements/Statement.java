package jumpingalien.program.statements;

import jumpingalien.model.BreakException;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Statements.
 * 
 */

public abstract class Statement {

	public Statement(SourceLocation loc) {
		this.sourceLocation = loc;
	}
	
	public boolean isWellFormed() {
		return !forLoopContainsAction() && !containsIllegalBreak();
	}
	
	private boolean containsIllegalBreak() {
		if (this instanceof While || this instanceof ForEach) {
			return false;
		}
		if (this instanceof SequenceOfStatements) {
			for (Statement st : ((SequenceOfStatements) this).getList()) {
				if (st.containsIllegalBreak()) {
					return true;
				}
			}
		}
		if (this instanceof Break) {
			return true;
		}
		return false;
	}

	private boolean isActionStmt() {
		if (this instanceof StartRun || this instanceof StopRun || this instanceof StartJump || this instanceof StopJump || this instanceof StartDuck || this instanceof StopDuck) {
			return true;
		}
		else return false;
	}

	private boolean forLoopContainsAction() {
		if ( this instanceof ForEach) {
			if ( ((ForEach)this).getBody().containsAction()) {
				return true;
			}
			else return false;
		}
		else if (this instanceof SequenceOfStatements) {
			for (Statement st : ((SequenceOfStatements)this).getList()) {
				if ( st.forLoopContainsAction()) {
					return true;
				}
			}
		}
		else if (this instanceof While) {
			if (((While)this).getBody().forLoopContainsAction()) {
				return true;
			}
		}
		else if (this instanceof IfThenElse) {
			if (((IfThenElse)this).getBody1().forLoopContainsAction()) {
				return true;
			}
			if (((IfThenElse)this).getBody2() != null && ((IfThenElse)this).getBody2().forLoopContainsAction()) {
				return true;
			}
		}
		
		return false;	
	}
	
	private boolean containsAction() {
		if (isActionStmt()) {
			return true;
		}
		else if (this instanceof SequenceOfStatements){
			for (Statement st : ((SequenceOfStatements)this).getList()) {
				if ( st.containsAction()) {
					return true;
				}
			}
		}
		else if (this instanceof While) {
			if (((While)this).getBody().containsAction()) {
				return true;
			}
		}
		else if (this instanceof IfThenElse) {
			if (((IfThenElse)this).getBody1().containsAction()) {
				return true;
			}
			if (((IfThenElse)this).getBody2() != null && ((IfThenElse)this).getBody2().containsAction()) {
				return true;
			}
		}
		return false;
	}
	
	public abstract double executeStatement(Program program, double time) throws BreakException;
	
	public SourceLocation getSourceLocation() {
		return sourceLocation;
	}
	
	private final SourceLocation sourceLocation;
	
	private boolean isFinished;

	public boolean isFinished() {
		return isFinished;
	}

	public void setFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}

	public abstract void reset();	
	
}
