package jumpingalien.program.expressions;

import jumpingalien.model.Mazub;
import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of IsMazubs extending the abstract class UnaryOperator.
*/
public class IsMazub extends UnaryOperator {

	public IsMazub(Expression operand, SourceLocation location) {
		super(operand, location);
	}

	@Override
	public Expression evaluate(Program program) {
		return new Boolean(((WorldObject) (getOperand().evaluate(program))) instanceof Mazub,null);
	}

}
