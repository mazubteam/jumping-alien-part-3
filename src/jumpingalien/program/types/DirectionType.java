package jumpingalien.program.types;

import jumpingalien.part3.programs.IProgramFactory.Direction;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of DirectionType extending the abstract type MyType.
 * 
 */

public class DirectionType extends MyType {
	
	public DirectionType(Direction d) {
		setDirection(d);
	}
	
	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	private Direction direction;
}
