package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A  class of DoubleConstants.
*/
public class DoubleConstant extends NumberExpression {

	public DoubleConstant(double value, SourceLocation location) {
		super(value,location);
	}

	@Override
	public Expression evaluate(Program program) {
		return this;
	}
	
	@Override
	public String toString() {
		return Double.toString(getValue());
	}

}
