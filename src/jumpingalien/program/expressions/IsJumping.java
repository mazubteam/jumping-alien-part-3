package jumpingalien.program.expressions;

import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of IsJumpings extending the abstract class UnaryOperator.
*/
public class IsJumping extends UnaryOperator {

	public IsJumping(Expression operand, SourceLocation location) {
		super(operand, location);
	}

	@Override
	public Expression evaluate(Program program) {
		if (getOperand().evaluate(program) instanceof Self) {
			WorldObject obj = program.getWorldObject();
			return new Boolean( obj.getVerticalVelocity()!=0,null);
		}
		return new Boolean( ((WorldObject) (getOperand().evaluate(program))).getVerticalVelocity()!=0,null);
	}

}
