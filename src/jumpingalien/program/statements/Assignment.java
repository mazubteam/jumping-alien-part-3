package jumpingalien.program.statements;

import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
import jumpingalien.program.expressions.Expression;
import jumpingalien.program.expressions.Null;
import jumpingalien.program.types.MyType;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Assignment extending the abstract type Statement.
 * 
 */
public class Assignment extends Statement {

	public Assignment(String variableName, MyType variableType, Expression value, SourceLocation sourceLocation) {
		super(sourceLocation);
		setValue(value);
		this.varName = variableName;
	}

	private void setValue(Object value) {
		this.value = value;
	}
	
	private Object getValue() {
		return this.value;
	}
	private Object value;
	
	private String getVarName() {
		return varName;
	}
	private final String varName;

	@Override
	public double executeStatement(Program program, double time) {
		if (time >= 0.001) {
			if (!( getValue() instanceof Null || getValue() instanceof WorldObject)) {
				program.getGlobalVars().get(getVarName()).setValue(((Expression) getValue()).evaluate(program));
			}
			else if ( getValue() instanceof Null){
				
				program.getGlobalVars().get(getVarName()).setValue(new Null(null));
			}
			else {
				program.getGlobalVars().get(getVarName()).setValue(getValue());
			}
			this.setFinished(true);
			return time - 0.001;
		}
		else return time;
	}

	@Override
	public void reset() {
		this.setFinished(false);
	}
}
