package jumpingalien.program.expressions;

import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of GetY's extending the abstract class UnaryOperator.
*/
public class GetY extends UnaryOperator {

	public GetY(Expression operand, SourceLocation location) {
		super(operand, location);
	}

	@Override
	public Expression evaluate(Program program) {
		if (getOperand().evaluate(program) instanceof Self) {
			WorldObject obj = program.getWorldObject();
			return new DoubleConstant(obj.getYPosition(),null);
		}
		return new DoubleConstant(((WorldObject) (getOperand().evaluate(program))).getYPosition(),null);
	}
}
