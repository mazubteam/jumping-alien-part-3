package jumpingalien.program.expressions;

import jumpingalien.model.Tile;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of IsTerrains extending the abstract class UnaryOperator.
*/
public class IsTerrain extends UnaryOperator {

	public IsTerrain(Expression exp, SourceLocation location) {
		super(exp, location);
	}
	
	@Override
	public Object evaluate(Program program) {		
		return new Boolean( getOperand().evaluate(program) instanceof Tile ,null);
	}
}
