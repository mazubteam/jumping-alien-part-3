package jumpingalien.program;

import java.util.HashMap;
import java.util.Map;

import jumpingalien.model.BreakException;
import jumpingalien.model.WorldObject;
import jumpingalien.program.expressions.DoubleConstant;
import jumpingalien.program.expressions.Variable;
import jumpingalien.program.statements.Statement;
import jumpingalien.program.types.BoolType;
import jumpingalien.program.types.DirectionType;
import jumpingalien.program.types.DoubleType;
import jumpingalien.program.types.MyType;
import jumpingalien.program.types.ObjType;
import jumpingalien.util.ModelException;

/* 
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of Programs.
**/
public class Program {
	
	public Program(Statement stmt, Map<String, MyType> globalVars) {
		setMainStmt(stmt);
		for (String name : globalVars.keySet()) {
			if (globalVars.get(name) instanceof ObjType) {
				Variable var = new Variable(name,new ObjType(null),null);
				var.setValue(null);
				globalVarr.put(name, var);
			}
			else if (globalVars.get(name) instanceof BoolType) {
				Variable var = new Variable(name,new BoolType(false),null);
				var.setValue(null);
				globalVarr.put(name,var);
			}
			else if (globalVars.get(name) instanceof DoubleType) {
				Variable var = new Variable(name,new DoubleType(0.0),null);
				var.setValue(new DoubleConstant(0.0, null));
				globalVarr.put(name, var);
			}
			else {
				Variable var = new Variable(name,new DirectionType(null),null);
				var.setValue(null);
				globalVarr.put(name, var);
			}			
		}
	}

	private void setMainStmt(Statement stmt) {
		this.mainStmt = stmt;
	}
	
	private Statement mainStmt;
	
	private Statement getMainStmt() {
		return this.mainStmt;
	}
	
	public WorldObject getWorldObject() {
		return this.obj;
	}
	private WorldObject obj;
	
	public void setWorldObject(WorldObject obj) {
		this.obj = obj;
	}

	public Map<String, Variable> getGlobalVars() {
		return new HashMap<String, Variable> (globalVarr);
	}
	private Map<String, Variable> globalVarr = new HashMap<String, Variable>();

	public void run(double deltaT) {
		while (deltaT >= 0.001) {
			try {
				deltaT = getMainStmt().executeStatement(this, deltaT);
			} catch (BreakException e) {
				throw new ModelException("program is not well formed!");
			}
		}
		if (getMainStmt().isFinished()) {
			getMainStmt().reset();
		}
	}
	
	public boolean isWellFormed() {
		return getMainStmt().isWellFormed();
	}

}
