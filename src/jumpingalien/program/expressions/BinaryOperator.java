package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A abstract class of BinaryOperators extending the class Operator.
*/
public abstract class BinaryOperator extends Operator {
	
	public BinaryOperator(Expression left, Expression right, SourceLocation location) {
		super(location);
		setLeftOperand(left);
		setRightOperand(right);
	}
	
	@Override
	protected int getNbOperands() {
		return 2;
	}
	
	protected Expression getLeftOperand() {
		return leftOperand;
	}
	private Expression leftOperand;

	private void setLeftOperand(Expression left) {
		this.leftOperand = left;
	}
	
	protected Expression getRightOperand() {
		return rightOperand;
	}
	private Expression rightOperand;
	
	private void setRightOperand(Expression right) {
		this.rightOperand = right;
	}
}