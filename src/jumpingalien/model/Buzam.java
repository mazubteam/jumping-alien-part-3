package jumpingalien.model;

import jumpingalien.util.Sprite;
/**
 * A class of Buzams extending the type Alien
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 * 
 * @version 1.0
 */
public class Buzam extends Alien {

	public Buzam(Sprite[] sprite) {
		super(sprite);
		this.setNbHitPoints(500);
	}
}
