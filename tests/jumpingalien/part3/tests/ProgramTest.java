package jumpingalien.part3.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;
import jumpingalien.model.Orientation;
import jumpingalien.model.Plant;
import jumpingalien.model.School;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.part3.facade.Facade;
import jumpingalien.part3.facade.IFacadePart3;
import jumpingalien.part3.programs.ParseOutcome;
import jumpingalien.program.Program;

import org.junit.Before;
import org.junit.Test;

public class ProgramTest {

	private IFacadePart3 facade;
	private World world;
	private Plant plant;
	private School school = new School();
	
	@Before
	public void setUp() throws Exception {
		facade = new Facade();
		world = facade.createWorld(200, 1, 2, 1, 1, 0, 0);
		facade.setGeologicalFeature(world, 0, 0, 1);
		facade.setGeologicalFeature(world, 0, 1, 0);
	}
	
	@Test
	public void testGlobarVars() {
		ParseOutcome<?> outcome = facade.parse("double d := 1.0; "
											+  "direction dir := left;"
											+ "while d < 3 do "
												+ "d := 2.0;"
												+ "start_run dir;"
												+ "if random 2 <= 1 "
													+ "then break; "
												+ "fi "
											+ "done");
		
		
		Program program = (Program) outcome.getResult();
		assertTrue(program.getGlobalVars().size() == 2);
	}
	
	@Test
	public void testWellformed() {
		ParseOutcome<?> outcome = facade.parse("double d := 1.0; "
											+  "direction dir := left;"
											+ "while d < 3 do "
												+ "d := 2.0;"
												+ "start_run dir;"
												+ "if random 2 <= 1 "
													+ "then break; "
												+ "fi "
											+ "done");
		assumeTrue(outcome.isSuccess());
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testWellformed2() {
		ParseOutcome<?> outcome = facade.parse("double d := 1.0; "
											+  "direction dir := left;"
											+ "foreach(any,o) where (isslime o) do "
												+ "d := 2.0;"
												+ "start_run dir;"
												+ "if random 2 <= 1 "
													+ "then break; "
												+ "fi "
											+ "done");
		assumeTrue(outcome.isSuccess());
		assertFalse(facade.isWellFormed((Program) outcome.getResult()));
	}
	
	@Test
	public void testWellformed3() {
		IFacadePart3 facade = new Facade();
		ParseOutcome<?> outcome = facade.parse("double d := 1.0; "
											+  "direction dir := left;"
											+ "foreach(any,o) where (isslime o) do "
												+ "d := 2.0;"
												+ "if random 2 <= 1 "
													+ " then start_run dir;"
												+ "fi "
											+ "done");
		assumeTrue(outcome.isSuccess());
		assertFalse(facade.isWellFormed((Program) outcome.getResult()));
	}

	@Test
	public void testGetWorldObject() {
		ParseOutcome<?> outcome = facade.parse("while true do start_run right; wait (0.5 - 0.001); stop_run right; start_run left; wait (0.5 - 0.001); stop_run left; done");


		Program program = (Program) outcome.getResult();
		plant = facade.createPlantWithProgram(13, 19, spriteArrayForSize(3, 3), program);
		facade.addPlant(world, plant);
		assertTrue(plant == program.getWorldObject());
	}

	@Test
	public void testRun() {
		ParseOutcome<?> outcome = facade.parse(
				"while true do "
					+ "start_run right; "
					+ "wait (0.5 - 0.001); "
					+ "stop_run right; "
					+ "start_run left; "
					+ "wait (0.5 - 0.001); "
					+ "stop_run left; "
				+ "done");
		Program program = (Program) outcome.getResult();
		plant = facade.createPlantWithProgram(13, 19, spriteArrayForSize(3, 3), program);
		facade.addPlant(world, plant);
		
		program.run(10.0);
		assertTrue(plant.getHorizontalVelocity() == 0.5);
	}
	
	@Test
	public void testRun2() {
		ParseOutcome<?> outcome = facade.parse(
			"object o;"
			+ "foreach (any, o) where (isslime(o)) do "
				+ "if ! (isdead (o)) then "
					+ "print o;"
				+ "fi "
			+ "done "
				);
		assumeTrue(outcome.isSuccess());
		assertTrue(facade.isWellFormed((Program) outcome.getResult()));
		Program program = (Program) outcome.getResult();
		plant = facade.createPlantWithProgram(130, 220, spriteArrayForSize(3, 3), program);
		Slime slime = facade.createSlime(120, 220, spriteArrayForSize(3, 3), school);
		Slime slime2 = facade.createSlime(150, 220, spriteArrayForSize(3, 3), school);
		Slime slime3 = facade.createSlime(110, 220, spriteArrayForSize(3, 3), school);
		facade.addPlant(world, plant);
		facade.addSlime(world, slime);
		facade.addSlime(world, slime2);
		facade.addSlime(world, slime3);
		program.run(0.05);

	}

	@Test
	public void testRun3() {
		
		ParseOutcome<?> outcome = facade.parse(
				"object o := null; "
				+ "while (! isdead (self)) do "
					+ "o := searchobj ( left ); "
					+ "if (isslime(o)) then  "
						+ "start_run left; "
						+ "wait(0.1);  "
					+ "else "
						+ "start_duck(); "
						+ "start_run (right); "
					+ "fi "
				+ "done");
		Program program = (Program) outcome.getResult();
		plant = facade.createPlantWithProgram(130, 220, spriteArrayForSize(3, 3), program);
		Slime slime = facade.createSlime(120, 220, spriteArrayForSize(3, 3), school);
		facade.addPlant(world, plant);
		facade.addSlime(world, slime);
		
		program.run(0.01);
		assertTrue(plant.getHorizontalVelocity() == 0.5 && plant.getOrientation() == Orientation.LEFT);
	}
	
	
	@Test
	public void testRun4() {
		ParseOutcome<?> outcome = facade.parse(
				

		"object nearestSS := null;"

		+"	double distanceToNearestSS;"
		+"double x;"
	+"	double y;"
		+"double ox;"
	+"	double oy;"
	+"	double distance;"
	+"	object o;"
	+" x := getx self;"
		+"  y := gety self;"
		+"  foreach (any, o) where (isshark o || isslime o) do"
		+"    if (! isdead (o)) then"
		+"      ox := getx o;"
		 +"     oy := gety o;"
		   +"   distance := sqrt((((x - ox) * (x - ox)) + ((y - oy) * (y - oy))));"     
		    +"  if (nearestSS == null) then"
		    +"    nearestSS := o;"
		   +"     distanceToNearestSS := distance;"
		   +"   else"
		   +"     if (distance < distanceToNearestSS) then"
		   +"       nearestSS := o;"
		          
		  +"        distanceToNearestSS := distance;"
		 +"       fi"
		  +"    fi"
		  +"  fi"
		 +" done");
		Program program = (Program) outcome.getResult();
		plant = facade.createPlantWithProgram(130, 220, spriteArrayForSize(3, 3), program);
		Slime slime = facade.createSlime(120, 220, spriteArrayForSize(3, 3), school);
		Slime slime2 = facade.createSlime(150, 220, spriteArrayForSize(3, 3), school);
		Slime slime3 = facade.createSlime(110, 220, spriteArrayForSize(3, 3), school);
		facade.addPlant(world, plant);
		facade.addSlime(world, slime);
		facade.addSlime(world, slime2);
		facade.addSlime(world, slime3);
		
		program.run(0.01);
		System.out.println(slime);
		System.out.println(program.getGlobalVars().get("nearestSS").getValue());
		
		assertTrue(slime == program.getGlobalVars().get("nearestSS").getValue());		
	}
	
}
