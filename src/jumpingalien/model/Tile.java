package jumpingalien.model;

public class Tile extends WorldObject{
	
	public Tile(int ft, int x, int y) {
		setFeature(ft);
		setXPosition(x);
		setYPosition(y);
	}
	
	private int feature;

	public int getFeature() {
		return feature;
	}

	public void setFeature(int feature) {
		this.feature = feature;
	}

	@Override
	protected boolean isValidHorizontalVelocity(double velocity) {
		return false;
	}

	@Override
	protected boolean isValidVerticalVelocity(double velocity) {
		return false;
	}

	@Override
	protected boolean isValidHorizontalAcceleration(double acc) {
		return false;
	}

	@Override
	protected boolean isValidVerticalAcceleration(double acc) {
		return false;
	}
	
	protected void setXPosition(int xPos) { // voor een tile zijn ALLE posities geldig (voorlopig) TODO
		this.xPosition = xPos;
	}
	
	protected void setYPosition(int yPos) { // voor een tile zijn ALLE posities geldig (voorlopig) TODO
		this.yPosition = yPos;
	}
	
	public int getXPosition() {
		return (int) xPosition;
	}
	
	public int getYPosition() {
		return (int) yPosition;	
	}
	
	protected boolean isValidXPosition(double xPos) {
		return true;
	}

	@Override
	protected void advanceTime(double deltaT) throws IllegalXPositionException,
			IllegalYPositionException {		
	}

	@Override
	protected int getMaxNbHitPoints() {
		return 0;
	}

}
