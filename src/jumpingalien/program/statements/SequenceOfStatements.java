package jumpingalien.program.statements;

import java.util.ArrayList;
import java.util.List;

import jumpingalien.model.BreakException;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of SequenceOfStatements extending the abstract type Statement.
 * 
 */

public class SequenceOfStatements extends Statement {

	public SequenceOfStatements(SourceLocation loc, List<Statement> list) {
		super(loc);
		this.sequence = list;
	}
	
	
	private final List<Statement> sequence;


	public List<Statement> getList() {
		return new ArrayList<Statement>(sequence);
	}

	@Override
	public double executeStatement(Program program, double time) throws BreakException {
		for (int i=0; i<getList().size(); i++ ) {
			Statement stmt = getList().get(i);
			if (! stmt.isFinished() ) {
				time = stmt.executeStatement(program, time);
				
				if (! stmt.isFinished()) {
					this.setFinished(false);
					return time;
				}
			}
		}
		this.setFinished(true);
		return time;
	}

	@Override
	public void reset() {
		setFinished(false);
		for (Statement i :getList()) {
			i.reset();
		}	
	}
	
}
