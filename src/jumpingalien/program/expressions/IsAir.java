package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of IsAirs extending the abstract class UnaryOperator.
*/
public class IsAir extends UnaryOperator {

	public IsAir(Expression exp, SourceLocation location) {
		super(exp, location);
	}
	
	@Override
	public Object evaluate(Program program) {
		return new Boolean(((Boolean) (new IsTerrain(getOperand(), null)).evaluate(program)).getBooleanValue() && 0==((jumpingalien.model.Tile) (Object) getOperand()).getFeature(),null);
	}

}
