package jumpingalien.part2.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.Mazub;
import jumpingalien.model.Slime;
import jumpingalien.model.School;
import jumpingalien.model.World;
import jumpingalien.part2.facade.Facade;
import jumpingalien.part2.facade.IFacadePart2;
import jumpingalien.util.Sprite;
import org.junit.Before;
import org.junit.Test;

public class SlimeTest {
	
	private Slime testSlime;
	private School testSchool = new School();
	
	@Before
	public void setUp() throws Exception {
		IFacadePart2 facade = new Facade();
		
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		testSlime = facade.createSlime(0, 499, spriteArrayForSize(3, 3), testSchool);
		facade.addSlime(world, testSlime);
	}
	
	@Test
	public void constructor() {
		Sprite[] sprites = spriteArrayForSize(3,3);
		
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 1);
		Slime slime = facade.createSlime(0, 499, sprites, testSchool);
		facade.addSlime(world, slime);
		
		assertSame(sprites, slime.getImages());
		assertEquals(0, slime.getSpriteIndex());
		assertTrue(slime.getVerticalAcceleration() == 0.0);
		assertTrue(slime.getHorizontalAcceleration() == 0.0);
	}
	
	@Test
	public void advanceTime_moving() throws Exception {
		
		Sprite[] sprites = spriteArrayForSize(3,3);
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 1, 2, 1, 1, 1, 1);
		Mazub alien = facade.createMazub(250, 999, spriteArrayForSize(3, 3));
		facade.setMazub(world, alien);
		facade.setGeologicalFeature(world, 0, 0, 1);
		School school = new School();
		Slime slime = facade.createSlime(100, 499, sprites, school);
		facade.addSlime(world, slime);
		
		for (int i = 0; i < 5; i++) {
			world.advanceTime(0.1);
		}
		assertTrue(slime.getYPosition() == 499);
		assertTrue(slime.getHorizontalAcceleration() == 0.7);
		assertTrue(slime.getHorizontalVelocity() <= 2.5 && slime.getHorizontalVelocity() >= 0);
		assertTrue(slime.getVerticalVelocity() == 0.0);
		assertTrue(slime.getVerticalAcceleration() == 0.0);
	}

}
