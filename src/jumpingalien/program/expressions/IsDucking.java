package jumpingalien.program.expressions;

import jumpingalien.model.Alien;
import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of IsDucking's extending the abstract class UnaryOperator.
*/
public class IsDucking extends UnaryOperator {

	public IsDucking(Expression operand, SourceLocation location) {
		super(operand, location);
	}

	@Override
	public Expression evaluate(Program program) {
		if ((WorldObject) (getOperand().evaluate(program)) instanceof Alien) {
			return new Boolean( ((Alien) (getOperand().evaluate(program))).isDucking(),null);
		}
		else return new Boolean(false, null);
	}


}
