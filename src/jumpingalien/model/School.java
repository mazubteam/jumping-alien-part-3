package jumpingalien.model;

import java.util.ArrayList;
import java.util.List;

import be.kuleuven.cs.som.annotate.Raw;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 * 
 * @version 1.0
 * 
 * A class of Schools.
 * 
 * @invar	...
 * 			| hasProperSlimes()
 *
 */
public class School {

	public School() {
	}

	//relaties met Slimes
	/**
	 * @param 	slime
	 * 			...
	 * @return	...
	 * 			| slimes.contains(slime)
	 */
	public boolean hasAsSlime(Slime slime) {
		return getAllSlimes().contains(slime);
	}
	private final List<Slime> slimes = new ArrayList<Slime>();

	/**
	 * @param 	slime
	 * 			...
	 * @pre		...	
	 * 			| canHaveAsSlime(slime);
	 * @pre		...	
	 * 			| slime.hasAsSchool(this);
	 * @effect	...	
	 * 			| slimes.add(slime)
	 */
	public void addAsSlime(@Raw Slime slime) {
		assert canHaveAsSlime(slime);
		assert slime.hasAsSchool(this);
		slimes.add(slime);
	}

	/**
	 * @param 	slime
	 * 			...
	 * @pre 	...
	 * 			| slime.hasAsSchool(this)
	 * @effect	...
	 * 			| slimes.remove(slime)
	 */
	public void removeAsSlime(@Raw Slime slime) {
		assert slime.hasAsSchool(this);
			slimes.remove(slime);
	}

	/**
	 * @param 	slime
	 * 			...
	 * @return	...
	 * 			| ( (!slime.isTerminated()) && (slime != null) && (getNbSlimes() <= 10) )
	 */
	public boolean canHaveAsSlime(Slime slime) {
		return ( (!slime.isTerminated()) && (slime != null) && (getNbSlimes() <= 10) );
	}

	/**
	 * @return	...
	 * 			| for each slime in getAllSlimes()
	 * 			| 	if ((!canHaveAsSlime(slime)) || (! slime.hasAsSchool(this)))
	 * 			|		return false
	 * @return	...
	 * 			| if ! ((!canHaveAsSlime(slime)) || (! slime.hasAsSchool(this)))
	 * 			|	for each slime in getAllSlimes() 	
	 * 			|		return true
	 */
	public boolean hasProperSlimes() {
		for (Slime slime: getAllSlimes()) {
			if ( (!canHaveAsSlime(slime)) || (! slime.hasAsSchool(this)) )
				return false;
		}
		return true;
	}

	/**
	 * @return	...
	 * 			| new ArrayList<Slime>(slimes)
	 */
	public List<Slime> getAllSlimes() {
		return new ArrayList<Slime>(slimes);
	}

	/**
	 * @return	...
	 * 			| slimes.size()
	 */
	public int getNbSlimes(){
		return getAllSlimes().size();
	}

	/**
	 * @param 	other
	 * 			...
	 * @effect	...
	 * 			| if (slime != other && slime.getSchool() == other.getSchool())
	 * 			|	then slime.setNbHitPoints(slime.getNbHitPoints() - 1);
	 * 			|		 other.setNbHitPoints(other.getNbHitPoints() + 1);
	 */
	void addHpNewSchool(Slime other){
		for(Slime slime: getAllSlimes()){
			if(slime != other && slime.getSchool() == other.getSchool())
				slime.setNbHitPoints(slime.getNbHitPoints() - 1);
			other.setNbHitPoints(other.getNbHitPoints() + 1);
		}
	}

	/**
	 * @param	other
	 * 			...
	 * @effect	...
	 * 			| if (slime != other && slime.getSchool() == other.getSchool())
	 * 			|	then slime.setNbHitPoints(slime.getNbHitPoints() + 1);
	 * 			|		 other.setNbHitPoints(other.getNbHitPoints() - 1);
	 */
	void addHpOldSchool(Slime other){
		for(Slime slime: getAllSlimes()){
			if(slime != other && slime.getSchool() == other.getSchool()){
				slime.setNbHitPoints(slime.getNbHitPoints() + 1);
				other.setNbHitPoints(other.getNbHitPoints() - 1);
			}
		}
	}
}
