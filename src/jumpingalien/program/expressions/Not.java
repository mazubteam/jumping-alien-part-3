package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

public class Not extends UnaryOperator {

	public Not(Expression operand,SourceLocation location) {
		super(operand, location);
	}

	@Override
	public Expression evaluate(Program program) {
		return new Boolean(! ((Boolean) getOperand().evaluate(program)).getBooleanValue(),null);
	}

}
