package jumpingalien.model;

import jumpingalien.util.Sprite;
import be.kuleuven.cs.som.annotate.Basic;

/**
 * A class of Aliens extending the abstract type WorldObject
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 * 
 * @version 1.0
 * 
 * @invar	...
 * 			| isValidVerticalVelocity(getVerticalVelocity())
 * @invar	...
 * 			| isValidHorizontalVelocity(getHorizontalVelocity())
 * @invar	...
 * 			| isValidVerticalAcceleration(getVerticalAcceleration())
 * @invar	...
 * 			| isValidHorizontalAcceleration(getHorizontalAcceleration())
 */
public abstract class Alien extends WorldObject {

	// Constructor
	/**
	 * Initialize this new Alien with the given array of Sprites.
	 * @post	The sprite index of this new Alien is equal to zero.
	 * 			| new.getSpriteIndex() == 0
	 * @post	The maximal horizontal velocity of this new Alien is equal to 3.0
	 * 			| new.getMaxHorizontalVelocity() == 3.0
	 * @post	The initial horizontal velocity of this new Alien is equal to 1.0
	 * 			| new.getInitialHorizontalVelocity() == 1.0
	 * @post	The horizontal acceleration of this new Alien is equal to 0.0
	 * 			| new.getHorizontalAcceleration() == 0.0
	 * @post	The vertical acceleration of this new Alien is equal to 0.0
	 * 			| new.getVerticalAcceleration() == 0.0
	 * @post 	The images of this new Alien are equal to sprite
	 * 			| new.getImages() == sprite
	 * @post 	The HP of this new Alien is equal to 100.
	 * 			| new.getNbHitPoints() == 100 
	 */
	public Alien(Sprite[] sprite) {
		this.setImages(sprite);
		this.setSpriteIndex(0);
		this.setMaxHorizontalVelocity(3.0);
		this.setInitialHorizontalVelocity(1.0);
		this.setHorizontalAcceleration(0.0);
		this.setVerticalAcceleration(0.0);
	}

	// Checkers
	/**
	 * @param 	velocity
	 * 			The horizontal velocity to check.
	 * @return 	True if and only if the velocity is higher than the initial horizontal velocity, and smaller than the maximal horizontal velocity of this Alien, or if the velocity is equal to zero.
	 * 			| ( (velocity >= getInitialHorizontalVelocity()) && (velocity <= getMaxHorizontalVelocity()) || (velocity == 0.0) )
	 */
	@Override
	protected boolean isValidHorizontalVelocity(double velocity) {
		if (velocity == Double.POSITIVE_INFINITY || velocity == Double.NEGATIVE_INFINITY || velocity == Double.NaN) {
			return false;
		}
		return ( ((velocity >= getInitialHorizontalVelocity()) && (velocity <= getMaxHorizontalVelocity())) || (velocity == 0.0) );	
	}

	/**
	 * @param 	velocity
	 * 			The vertical velocity to check.
	 * @return 	True	
	 * 			true
	 */
	@Override
	protected boolean isValidVerticalVelocity(double velocity) {
		if (velocity == Double.POSITIVE_INFINITY || velocity == Double.NEGATIVE_INFINITY || velocity == Double.NaN) {
			return false;
		}
		return true;
	}

	/**
	 * @param 	acc
	 * 			The horizontal acceleration to check.
	 * @return 	True if and only if the acceleration is equal to 0.9 or 0.0
	 * 			| ((acc == 0.9) || (acc == 0.0))
	 */
	@Override
	protected boolean isValidHorizontalAcceleration(double acc) {
		if (acc == Double.POSITIVE_INFINITY || acc == Double.NEGATIVE_INFINITY || acc == Double.NaN) {
			return false;
		}
		return ((acc == 0.9) || (acc == 0.0));
	}

	/**
	 * @param 	acc
	 * 			The vertical acceleration to check.
	 * @return 	True if and only if the acceleration is equal to -10.0 or 0.0
	 * 			| ((acc == -10.0) || (acc == 0.0))
	 */
	@Override
	protected boolean isValidVerticalAcceleration(double acc) {
		if (acc == Double.POSITIVE_INFINITY || acc == Double.NEGATIVE_INFINITY || acc == Double.NaN) {
			return false;
		}
		return ((acc == -10.0) || (acc == 0.0));
	}

	/**
	 * @return 	True if and only if the Alien is jumping.
	 * 			|  ( (getVerticalVelocity() != 0))
	 */
	protected boolean isJumping() {
		return ( (getVerticalVelocity() != 0) );
	}

	/**
	 * @return 	True if and only if the Alien is moving to the right.
	 * 			| ( (getOrientation() == Orientation.RIGHT) && (getHorizontalVelocity() > 0) )
	 */
	public boolean isMovingRight() {
		return  ( (getOrientation() == Orientation.RIGHT) && (getHorizontalVelocity() > 0) );
	}

	/**
	 * @return 	True if and only if the Alien is moving to the left.
	 * 			|  ( (getOrientation() == Orientation.LEFT) && (getHorizontalVelocity() > 0) )
	 */
	public boolean isMovingLeft() {
		return ( (getOrientation() == Orientation.LEFT) && (getHorizontalVelocity() > 0) );
	}

	/**
	 * @return 	True if and only if the Alien is ducking.
	 * 			| this.isDucking
	 */
	@Basic
	public boolean isDucking() {
		return this.isDucking;
	}
	protected boolean isDucking;

	/**
	 * @return 	True if and only if the Alien has moved right within the last second.
	 * 			| ( (this.rightTimer < 1.0) && (getOrientation() == Orientation.RIGHT) )
	 */
	protected boolean hasMovedRightWithinTheLastSecond() {
		return ( (this.rightTimer < 1.0) && (getOrientation() == Orientation.RIGHT) );
	}
	protected double rightTimer;

	/**
	 * @return 	True if and only if the Alien has moved left within the last second.
	 * 			| ( (this.leftTimer < 1.0) && (getOrientation() == Orientation.LEFT) )
	 */
	protected boolean hasMovedLeftWithinTheLastSecond() {
		return ( (this.leftTimer < 1.0) && (getOrientation() == Orientation.LEFT) );
	}
	protected double leftTimer;

	// Running
	/**
	 * @pre 	This Alien may not already be moving left.
	 * 			| (! this.isMovingLeft() )
	 * @post	The horizontal acceleration of this Alien is equal to 0.9
	 * 			| new.getHorizontalAcceleration() == 0.9
	 * @post	The horizontal velocity of this Alien is equal to the initial horizontal velocity
	 * 			| new.getHorizontalVelocity() == 1.0
	 * @effect 	If this Alien is ducking while it starts moving left, its sprite index is set to 7.
	 * 			| if (this.isDucking())
	 * 			| 	then this.setSpriteIndex(7)
	 * @effect 	If this Alien is jumping while it starts moving left, its sprite index is set to 5.
	 * 			| if (this.isJumping())
	 * 			| 	then this.setSpriteIndex(5)
	 * @effect 	If this Alien is not ducking and not jumping while it starts moving left, its sprite index is set to 9+calculateM().
	 * 			| if ((!this.isDucking()) && (!this.isJumping()))
	 * 			| 	then this.setSpriteIndex(9+calculateM())
	 */
	public void startMoveLeft() {	
		assert (! this.isMovingLeft() );
		setOrientation(Orientation.LEFT);
		setHorizontalAcceleration(0.9);
		setHorizontalVelocity(getInitialHorizontalVelocity());
		setSpriteIndex(9+calculateM());
		if (this.isJumping()) {
			this.setSpriteIndex(5);
		}
		else if (this.isDucking()) {
			this.setSpriteIndex(7);
		}
		else setSpriteIndex(9+calculateM());
	}

	/**
	 * @pre 	This Alien may not already be moving right, but can be jumping or ducking.
	 * 			| (! this.isMovingRight() )
	 * @post	The horizontal acceleration of this Alien is equal to 0.9
	 * 			| new.getHorizontalAcceleration() == 0.9
	 * @post	The horizontal velocity of this Alien is equal to the initial horizontal velocity
	 * 			| new.getHorizontalVelocity() == 1.0
	 * @effect 	If this Alien is ducking while it starts moving right, its sprite index is set to 6.
	 * 			| if (this.isDucking())
	 * 			| 	then this.setSpriteIndex(6)
	 * @effect 	If this Alien is jumping while it starts moving right, its sprite index is set to 4.
	 * 			| if (this.isJumping())
	 * 			| 	then this.setSpriteIndex(4)
	 * @effect 	If this Alien is not ducking and not jumping while it starts moving right, its sprite index is set to 8.
	 * 			| if ((!this.isDucking()) && (!this.isJumping()))
	 * 			| 	then this.setSpriteIndex(8)
	 */
	public void startMoveRight() {
		assert (! this.isMovingRight() );
		setOrientation(Orientation.RIGHT);
		setHorizontalAcceleration(0.9);
		setHorizontalVelocity(getInitialHorizontalVelocity());
		if (this.isJumping()) {
			this.setSpriteIndex(4);
		}
		else if (this.isDucking()) {
			this.setSpriteIndex(6);
		}
		else setSpriteIndex(8);
	}

	/**
	 * @pre		This Alien must be moving left.
	 * 			| ( this.isMovingLeft() )
	 * @post 	The horizontal velocity of this Alien is equal to zero.
	 * 			| new.getHorizontalVelocity() == 0
	 * @post 	The horizontal acceleration of this Alien is equal to zero.
	 * 			| new.getHorizontalAcceleration() == 0
	 * @effect 	If this Alien is ducking while it stops moving left, its sprite index is set to 7.
	 * 			| if (this.isDucking())
	 * 			| 	then this.setSpriteIndex(7)
	 * @effect 	If this Alien is not ducking while it stops moving left, its sprite index is set to 3.
	 * 			| if (!this.isDucking()) 
	 * 			| 	then this.setSpriteIndex(3)
	 */
	public void endMoveLeft() {
		if (this.isDucking()) {
			this.setSpriteIndex(7);
		}
		else setSpriteIndex(3);
		assert ( this.isMovingLeft() );
		this.leftTimer = 0.0;
		setHorizontalVelocity(0.0);
		this.setHorizontalAcceleration(0.0);
	}

	/**
	 * @pre		This Alien must be moving right.
	 * 			| (this.isMovingRight())
	 * @post 	The horizontal velocity of this Alien is equal to zero.
	 * 			| new.getHorizontalVelocity() == 0
	 * @post 	The horizontal acceleration of this Alien is equal to zero.
	 * 			| new.getHorizontalAcceleration() == 0
	 * @effect 	If this Alien is ducking while it stops moving right, its sprite index is set to 6.
	 * 			| if (this.isDucking())
	 * 			| 	then this.setSpriteIndex(6)
	 * @effect 	If this Alien is not ducking while it stops moving right, its sprite index is set to 2.
	 * 			| if (!this.isDucking())
	 * 			| 	then this.setSpriteIndex(2)
	 */
	public void endMoveRight() {
		if (this.isDucking()) {
			this.setSpriteIndex(6);
		}
		else setSpriteIndex(2);

		assert ( this.isMovingRight() );
		this.rightTimer = 0.0;
		setHorizontalVelocity(0.0);
		this.setHorizontalAcceleration(0.0);
	}

	// Jumping
	/**
	 * @post	The new vertical velocity of this Alien is equal to 8.0 m/s
	 * 			| new.getVerticalVelocity() == 8.0
	 * @post	The new vertical acceleration of this Alien is equal to -10.0 m/s^2
	 * 			| new.getVerticalAcceleration() == -10.0
	 * @throws 	Exception
	 * 			This Alien is jumping.
	 * 			| (this.isJumping())
	 * @throws	Exception
	 * 			This Alien is ducking.
	 * 			| (this.isDucking())	
	 */
	public void startJump() throws Exception {
		if (this.isJumping())
			throw new Exception();
		if (isDucking())
			throw new Exception();
		this.setVerticalVelocity(initialVerticalVelocity);
		this.setVerticalAcceleration(-10.0);
	}     
	public final double initialVerticalVelocity = 8.0;

	/**
	 * @post	The new vertical velocity of this Alien is equal to 0.0 m/s
	 * 			| new.getVerticalVelocity() == 0
	 * @throws	Exception
	 * 			This Alien has a negative vertical velocity.
	 * 			| (this.getVerticalVelocity() <= 0)
	 * @throws 	Exception
	 * 			This Alien is not jumping
	 * 			| (! this.isJumping() )
	 */
	public void endJump() throws Exception{ 
		if (this.getVerticalVelocity() <= 0) {
			throw new Exception();
		}
		if (! this.isJumping()) {
			throw new Exception();
		}
		this.setVerticalVelocity(0.0);
	}

	// Ducking
	/**
	 * @post 	The new maximal horizontal velocity of this Alien is equal to 1.0
	 * 			| new.getMaxHorizontalVelocity() == 1.0
	 * @post	This Alien is ducking.
	 * 			| new.isDucking()
	 * @post 	If the speed of this Alien is greater than 1.0 m/s, the speed of the new Alien is equal to 1.0 m/s
	 * 			| if (this.getHorizontalVelocity() > 1)
	 * 			| 	then new.getHorizontalVelocity() == 1
	 * @throws 	Exception
	 * 			This Alien is jumping.
	 * 			| this.isJumping()
	 * @throws 	Exception
	 * 			This Alien is ducking.
	 * 			| this.isDucking()
	 */
	public void startDuck() throws Exception{
		if (isJumping() || isDucking()) {
			throw new Exception();
		}
		this.isDucking = true;
		setMaxHorizontalVelocity(1.0);
		if (getHorizontalVelocity() > 1.0) {
			setHorizontalVelocity(1.0);
		}
	}

	/**
	 * @post 	This Alien is no longer ducking.
	 * 			| ! new.isDucking()
	 * @post 	The new maximal horizontal velocity of this Alien is equal to 3.0 if its upper periphery does not overlap with an impassable terrain feature.
	 * 			| if ( !featureOverlapsTop(1))
	 * 			| 	then new.getMaxHorizontalVelocity() == 3.0
	 * @effect	If this Alien is moving right while it stops ducking and its upper periphery does not overlap with an impassable terrain feature, its sprite index is set to 8.
	 * 			| if (getSpriteIndex() == 6 && !featureOverlapsTop(1))
	 * 			| 	then setSpriteIndex(8)
	 * @effect	If this Alien is moving left while it stops ducking and its upper periphery does not overlap with an impassable terrain feature, its sprite index is set to 9 + calculateM().
	 * 			| if (getSpriteIndex() == 7 && !featureOverlapsTop(1))
	 * 			| 	then setSpriteIndex(9 + calculateM())
	 * @post 	If this Alien's upper periphery overlaps with an impassable terrain feature, ableToEndDuck is set to false.
	 * 			| if ( featureOverlapsTop(1) )
	 * 			|	then ableToEndDuck = false
	 * @throws	Exception
	 * 			This Alien is not ducking
	 * 			| (! this.isDucking() )
	 */
	public void endDuck() throws Exception {
		if (! this.isDucking() ) {
			throw new Exception();
		}
		if ( featureOverlapsTop(1) ) {
			ableToEndDuck = false;
		}
		else {
			this.isDucking = false;
			setMaxHorizontalVelocity(3.0);
			if (getSpriteIndex() == 7) {
				setSpriteIndex(9+calculateM());
			}
			if (getSpriteIndex() == 6) {
				setSpriteIndex(8);
			}
		}
	}
	protected boolean ableToEndDuck = true;

	// Sprites and Sprite Indices
	/**
	 * @return 	0 if this Alien is not moving horizontally, has not moved horizontally within the last second of in-game-time and is not ducking.
	 * 			| if (getHorizontalVelocity() == 0  && ! hasMovedRightWithinTheLastSecond() && 
					|	! hasMovedLeftWithinTheLastSecond() && !isDucking() )
	 * 			| 		then 0
	 * @return 	1 if and only if this Alien is not moving horizontally, has not moved horizontally within the last second of in-game-time and is ducking.
	 * 			| if (getHorizontalVelocity() == 0  && ! hasMovedRightWithinTheLastSecond() && 
					|	! hasMovedLeftWithinTheLastSecond() && isDucking() )
	 * 			| 		then 1
	 * @return 	2 if and only if this Alien is not moving horizontally but its last horizontal movement was to the right (within 1s), and the character is not ducking.
	 * 			| if (getHorizontalVelocity() == 0  && hasMovedRightWithinTheLastSecond() && !isDucking() )
	 * 			| 	then 2
	 * @return 	3 if and only if this Alien is not moving horizontally but its last horizontal movement was to the left (within 1s), and the character is not ducking.
	 * 			| if (getHorizontalVelocity() == 0  && hasMovedLeftWithinTheLastSecond() && !isDucking() )
	 * 			| 	then 3
	 * @return 	4 if and only if this Alien is moving to the right and jumping and not ducking.
	 * 			| if ( isMovingRight() && isJumping() && ! isDucking())
	 * 			| 	then 4
	 * @return 	5 if and only if this Alien is moving to the left and jumping and not ducking.
	 * 			| if ( isMovingLeft() && isJumping() && ! isDucking())
	 * 			| 	then 5
	 * @return 	6 if and only if this Alien is ducking and moving to the right or was moving to the right (within 1s).
	 * 			| if ( isDucking() && (isMovingRight() || (hasMovedRightWithinTheLastSecond())) )
	 * 			| 	then 6
	 * @return 	7 if and only if this Alien is ducking and moving to the left or was moving to the left (within 1s).
	 * 			| if  ( isDucking() && (isMovingLeft() || (hasMovedLeftWithinTheLastSecond())) )
	 * 			| 	then 7
	 * @return 	8 if and only if this Alien is neither ducking nor jumping and moving to the right.
	 * 			| if ( isMovingRight() && ! isDucking() && ! isJumping() )
	 * 			| 	then 8
	 * @return 	 (9 + calculateM()) if and only if this Alien is neither ducking nor jumping and moving to the left.
	 * 			| if ( isMovingLeft() && ! isDucking() && ! isJumping() )
	 * 			| 	then (9 + calculateM())
	 */
	protected int getNextSpriteIndex() {
		if (getHorizontalVelocity() == 0  && ! hasMovedRightWithinTheLastSecond() && 
				! hasMovedLeftWithinTheLastSecond() && !isDucking() ) {
			return 0;
		}
		if (getHorizontalVelocity() == 0  && ! hasMovedRightWithinTheLastSecond() && 
				! hasMovedLeftWithinTheLastSecond() && isDucking() ) {
			return 1;
		}
		if (getHorizontalVelocity() == 0  && hasMovedRightWithinTheLastSecond() && !isDucking() ) {
			return 2;
		}
		if (getHorizontalVelocity() == 0  && hasMovedLeftWithinTheLastSecond() && !isDucking() ) {
			return 3;
		}
		if ( isMovingRight() && isJumping() && ! isDucking()) {
			return 4;
		}
		if ( isMovingLeft() && isJumping() && ! isDucking()) {
			return 5;
		}
		if ( isDucking() && (isMovingRight() || (hasMovedRightWithinTheLastSecond())) ) {
			return 6;
		}
		if ( isDucking() && (isMovingLeft() || (hasMovedLeftWithinTheLastSecond())) ) {
			return 7;
		}
		if ( isMovingRight() && ! isDucking() && ! isJumping() ) {
			return 8;	
		}
		if ( isMovingLeft() && ! isDucking() && ! isJumping() ) {
			return ( 9+calculateM() );
		}
		return 0;
	}

	// get Travelled Distance
	/**
	 * @param 	deltaT
	 * 			A given time duration in seconds
	 * @return	the horizontal distance travelled by this Alien during the given time duration
	 * 			| if ( this.getHorizontalVelocity() == this.getMaxHorizontalVelocity() )
	 * 			| 	then (this.horizontalVelocity * deltaT) 
	 * 			| if (( this.getHorizontalVelocity() < this.getMaxHorizontalVelocity() ) && ( this.getHorizontalVelocity() > 0 ))
	 * 			| 	then (this.horizontalVelocity * deltaT + 0.5 * getHorizontalAcceleration() * Math.pow(deltaT,2) ) 
	 * 			| else 0.0
	 */
	protected double getHorizDistTravelled(double deltaT) {
		if ( getHorizontalVelocity() == getMaxHorizontalVelocity() ) {
			return (getHorizontalVelocity() * deltaT);
		}
		if (( getHorizontalVelocity() < getMaxHorizontalVelocity() ) && getHorizontalVelocity() > 0 ) {
			return (getHorizontalVelocity() * deltaT + 0.5 * getHorizontalAcceleration() * Math.pow(deltaT,2) );
		}
		return 0.0;	
	}

	/**
	 * @param 	deltaT
	 * 			A given time duration in seconds
	 * @return	The vertical distance travelled by this Alien during the given time duration, if the given time duration is greater than 0.0 and smaller than or equal to 0.2.
	 * 			| if ((deltaT <= 0.2) && (deltaT > 0))
	 * 			| 	then (getVerticalVelocity() * deltaT + 0.5 * getVerticalAcceleration() * Math.pow(deltaT,2)
	 * 			| else 0.0
	 */
	protected double getVertDistTravelled(double deltaT) {
		if ((deltaT <= 0.2) && (deltaT > 0)) {
			return (getVerticalVelocity() * deltaT + 0.5 * getVerticalAcceleration() * Math.pow(deltaT,2) );
		}
		return 0.0;
	}

	// Advance Time
	

	// Updaten van Timers, SpriteIndex, XPosition, YPosition, horizontalVelocity, verticalVelocity. (submethodes van advanceTime)
	/**
	 * @param 	deltaT
	 * 			A given time duration in seconds.
	 * @post	The timer counting the time since this Alien stopped moving right is incremented with deltaT.
	 * 			| new.rightTimer = this.rightTimer + deltaT
	 * @post	The timer counting the time since this Alien stopped moving left is incremented with deltaT.
	 * 			| new.leftTimer = this.leftTimer + deltaT
	 * @post	The timer counting the time since this Alien's image started displaying is incremented with deltaT.
	 * 			| new.displaySuccessiveImagesTimer = this.displaySuccessiveImagesTimer + deltaT
	 */
	protected void updateTimers(double deltaT) {
		this.rightTimer += deltaT;
		this.leftTimer += deltaT;
		this.displaySuccessiveImagesTimer += deltaT;
	}

	/**
	 * @param 	deltaT
	 * 			A given time duration in seconds.
	 * @effect 	If this Alien is moving right and does not overlap with any feature on its right side, 
	 * 				its position is incremented by 100 times the horizontal distance it travelled during the time interval deltaT.
	 * 			| if ((getOrientation() == Orientation.RIGHT) && ! featureOverlapsRight(1))
	 * 			| 	then setXPosition(getXPositionDouble() + 100 * getHorizDistTravelled(deltaT))
	 * @effect	If this Alien is moving left and does not overlap with any feature on its left side, 
	 * 				its position is decremented by 100 times the horizontal distance it travelled during the time interval deltaT.
	 * 			| if ((getOrientation() == Orientation.LEFT) && ! featureOverlapsLeft(1))
	 * 			| 	then setXPosition(getXPositionDouble() - 100 * getHorizDistTravelled(deltaT))
	 * @effect	If this Alien's calculated new horizontal velocity is smaller than or equal to its maximum horizontal velocity,
	 * 				it is set as its current horizontal velocity. Otherwise, the maximum horizontal velocity is set as its current velocity.
	 * 			| if ( isValidHorizontalVelocity(getHorizontalVelocity()) && (getHorizontalVelocity() + getHorizontalAcceleration() * deltaT) <= getMaxHorizontalVelocity())
	 * 			| 	then setHorizontalVelocity(getHorizontalVelocity() + getHorizontalAcceleration() * deltaT)
	 * 			| else
	 * 			| 	setHorizontalVelocity(getMaxHorizontalVelocity())
	 */
	protected void updateXPositionAndVelocity(double deltaT) throws IllegalXPositionException {

		if (getOrientation() == Orientation.RIGHT) {
			double distanceTravelled = (100 * getHorizDistTravelled(deltaT));
			if (! featureOverlapsRight(1)) {
				setXPosition(getXPositionDouble() + distanceTravelled);
			}
		}
		if (getOrientation() == Orientation.LEFT) {
			double distanceTravelled = (- 100 * getHorizDistTravelled(deltaT));
			if (!featureOverlapsLeft(1)) {
				setXPosition( getXPositionDouble() + distanceTravelled);
			}
		}	
		if ( isValidHorizontalVelocity(getHorizontalVelocity()) ) {
			if ( (getHorizontalVelocity() + getHorizontalAcceleration() * deltaT) <= getMaxHorizontalVelocity() )
				setHorizontalVelocity(getHorizontalVelocity() + getHorizontalAcceleration() * deltaT);
			else {
				setHorizontalVelocity(getMaxHorizontalVelocity());
			}
		}
	}

	/**
	 * @param 	deltaT
	 * 			A given time duration in seconds.
	 * @effect	If this Alien does not overlap with impassable terrain or an other WorldObject (while moving down) on his bottom periperhy,
	 * 				its vertical acceleration is set to -10 and its vertical velocity is decremented by -10 * deltaT.
	 * 			In case the Alien does overlap, its vertical acceleration is set to 0.0;
	 * 			| if (!featureOverlapsBottom(1) && ! (objectOverlapsBottom && getVerticalVelocity() <= 0) )
	 * 			|	then	setVerticalAcceleration(-10.0)
	 *			|			setVerticalVelocity(getVerticalVelocity() + getVerticalAcceleration() * deltaT)
	 *			| else setVerticalAcceleration(0.0)
	 * @effect	If this Alien has landed on impassable terrain while moving left or right, its vertical velocity is set to 0.0, and
	 * 				its sprite index is adjusted according to the direction it is moving in.
	 * 			| if (featureOverlapsBottom(1) && getVerticalVelocity() < 0 && isMovingRight() )
	 * 			|	then	setVerticalVelocity(0.0)
	 * 			|			setSpriteIndex(8);
	 * 			| if (featureOverlapsBottom(1) && getVerticalVelocity() < 0 && isMovingLeft() )
	 * 			|	then 	setVerticalVelocity(0.0)
	 * 			|			setSpriteIndex(9+calculateM())
	 * @effect	If this Alien is not hindered by any impassable features in the direction it is moving, 
	 * 				its position is increased (or decreased)  by the distance it has travelled during deltaT.
	 * 			| if (getVerticalVelocity() > 0 && ! featureOverlapsTop(1))
	 * 			|	then setYPosition(getYPositionDouble() + (100 * getVertDistTravelled(deltaT)))
	 *			| if (getVerticalVelocity() <= 0 && ! featureOverlapsBottom(1))
	 *			|	then setYPosition(getYPositionDouble() + (100 * getVertDistTravelled(deltaT)))
	 * @effect	If this Alien is moving up, and its upper periphery overlaps with impassable terrain, its vertical velocity is set to 0.0.
	 * 			| if (featureOverlapsTop(1) && (100 * getVertDistTravelled(deltaT)) > 0 )
	 * 			| then setVerticalVelocity(0.0);
	 */
	protected void updateYPositionAndVelocity(double deltaT) throws IllegalYPositionException {

		if (!featureOverlapsBottom(1) && ! (objectOverlapsBottom && getVerticalVelocity() <= 0) ) {
			setVerticalAcceleration(-10.0);
			setVerticalVelocity(getVerticalVelocity() + getVerticalAcceleration() * deltaT);
		}
		else {
			setVerticalAcceleration(0.0);
		}

		if (featureOverlapsBottom(1) && getVerticalVelocity() < 0) {
			setVerticalVelocity(0.0);
			if (isMovingRight()) {
				setSpriteIndex(8);
			}
			else if (isMovingLeft()){
				setSpriteIndex(9+calculateM());
			}
		}

		double distanceTravelled = (100 * getVertDistTravelled(deltaT));
		if (getVerticalVelocity() > 0) {
			if (! featureOverlapsTop(1)) {
				setYPosition(getYPositionDouble() + distanceTravelled);
			}
		}
		else if (!featureOverlapsBottom(1)) {
			setYPosition( getYPositionDouble() + distanceTravelled);
		}

		if (featureOverlapsTop(1) && distanceTravelled > 0 ) {
			setVerticalVelocity(0.0);
		}
	}
	public boolean objectOverlapsBottom;

	/**
	 * @param 	timePerImage
	 * 			The given duration of time indicating how long each image in a sequence of images should be displayed.
	 * @effect	The sprite index is set to be 8, if this Alien is at the end of its sequence while moving right.
	 * 			| if ((ind == 8) && (displaySuccessiveImagesTimer >= timePerImage) && (getSpriteIndex() == 8 + calculateM()))
	 * 			|	then setSpriteIndex(8)
	 * @effect	The sprite index is incremented with 1, while this Alien is not at the end of its sequence when moving.
	 * 			| (((ind == 8) && (displaySuccessiveImagesTimer >= timePerImage) && (getSpriteIndex() != 8 + calculateM())) || ( (ind == (9 + calculateM())) && (displaySuccessiveImagesTimer >= timePerImage) && (getSpriteIndex() != 9 + 2 * calculateM())) )
	 * 			| 	then setSpriteIndex(getSpriteIndex() + 1)
	 * @effect	The sprite index is set to 9 + calculateM(), if this Alien is at the end of its sequence while moving left.
	 * 			| if ( (ind == (9 + calculateM())) && (displaySuccessiveImagesTimer >= timePerImage) && (getSpriteIndex() == 9 + 2 * calculateM()) )
	 * 			|	then setSpriteIndex(9 + calculateM())
	 * @post	displaySuccessiveImagesTimer is equal to 0.0, if the sprite index is changed.
	 * 			| (if ((ind == 8) && (displaySuccessiveImagesTimer >= timePerImage)) || ( (ind == (9 + calculateM())) && (displaySuccessiveImagesTimer >= timePerImage) ))
	 * 			|	then new.displaySuccessiveImagesTimer == 0.0
	 */
	protected void displaySuccessiveImages(double timePerImage) {
		int ind = getNextSpriteIndex();
		if ((ind == 8) && (displaySuccessiveImagesTimer >= timePerImage)) {

			displaySuccessiveImagesTimer = 0.0;
			if (getSpriteIndex() == 8 + calculateM()) {
				setSpriteIndex(8);
			}
			else {
				setSpriteIndex(getSpriteIndex() + 1);
			}
		}
		if ( (ind == (9 + calculateM())) && (displaySuccessiveImagesTimer >= timePerImage) ) {
			displaySuccessiveImagesTimer = 0.0;
			if (getSpriteIndex() == 9 + 2 * calculateM()) {
				setSpriteIndex(9 + calculateM());
			}
			else {
				setSpriteIndex(getSpriteIndex() + 1);
			}
		}
	}
	protected double displaySuccessiveImagesTimer;

	/**
	 * @return	The integer m, indicating the amount of alternating images to be used while running to the left or the right.
	 * 			| ((this.getImages().length - 8 ) / 2) - 1
	 */
	public int calculateM() {
		return ((this.getImages().length - 8 ) / 2) - 1;
	}

	/**
	 * @return 	the maximum number of hitpoints of all Aliens.
	 * 			| MaxNbHitPoints;
	 */
	@Override
	@Basic
	public int getMaxNbHitPoints() {
		return MaxNbHitPoints;
	}

	protected static final int MaxNbHitPoints = 500;

	/**
	 * @param	deltaT
	 * 			A given time duration in seconds
	 * @throws 	IllegalXPositionException, IllegalYPositionException
	 * 			never
	 * 			| if ( (! isValidXPosition(0)) || (! isValidXPosition(getWorld().getXDimension()-1)) || (! isValidYPosition(getWorld().getYDimension()-1)) )
	 * @effect 	 The x-position and the horizontal velocity are changed to correctly represent this Alien after a period of deltaT seconds has passed.
	 * 			| updateXPositionAndVelocity(double deltaT)
	 * @effect  The y-position and the vertical velocity are changed to correctly represent this Alien after a period of deltaT seconds has passed.
	 * 			| updateYPositionAndVelocity(double deltaT)
	 * @effect	If necessary, the sprite index is changed according to the state this Alien is in.
	 * 			| updateSpriteIndex()
	 * @effect	The 3 timers are all incremented with deltaT
	 * 			| updateTimers()
	 *
	 */
	public void advanceTime(double deltaT) throws IllegalXPositionException, IllegalYPositionException {
		if (! isTerminated()) {
			try{
				updateTimers(deltaT);
				updateXPositionAndVelocity(deltaT);
				updateYPositionAndVelocity(deltaT);

				if (getNextSpriteIndex() < 8) {
					setSpriteIndex(getNextSpriteIndex());
				}
				else {
					displaySuccessiveImages(0.075);
				}

				if ( ! ableToEndDuck && (!featureOverlapsTop(1)) && featureOverlapsBottom(1)) {
					try {
						endDuck();
						ableToEndDuck = true;
					} catch (Exception e) {
					}
				}
			}
			catch (IllegalXPositionException | IllegalYPositionException exc) {
				if (getXPosition() < 0) {
					setXPosition(0);
					setHorizontalVelocity(0.0);
				}
				else if (getXPosition() >= getWorld().getXDimension()) {
					setXPosition(getWorld().getXDimension()-1);
					setHorizontalVelocity(0.0);
				}
				if (getYPosition() >= getWorld().getYDimension()) {
					setYPosition(getWorld().getYDimension()-1);
					setVerticalVelocity(0.0);
				}
			}
		}
	}
}


