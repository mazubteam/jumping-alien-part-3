package jumpingalien.program.expressions;

import jumpingalien.model.Tile;
import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of SearchObjects extending the abstract type Expression.
 * 
 */

public class SearchObject extends Expression {
	
	public SearchObject(Expression d, SourceLocation location) {
		super(location);
		direction = (Direction) d;
	}
	private final Direction direction;

	private Direction getDirection() {
		return direction;
	}
	
	@Override
	public Object evaluate(Program program) {
		return getNearestObject(program);
	}
	
	private Object getNearestObject(Program program) {
		WorldObject self =  program.getWorldObject();
		
		Object[] tileAndTilePos = getNearestTile(program);

		WorldObject worldObj = getNearestWorldObject(program);
	
		if (getDirection().getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT || getDirection().getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT) {
		
			if ((worldObj == null && tileAndTilePos != null) || ( tileAndTilePos != null && Math.abs((int) ((int[]) tileAndTilePos[1])[0] - self.getXPosition()) < Math.abs(worldObj.getXPosition() - self.getXPosition() ))) {
				return tileAndTilePos[0];
			}
			else {
				return worldObj;
			}
		}
		else {
			if ((worldObj == null && tileAndTilePos != null) || ( tileAndTilePos != null && Math.abs((int) ((int[]) tileAndTilePos[1])[1] - self.getYPosition()) < Math.abs(worldObj.getYPosition() - self.getYPosition() ))) {
				return tileAndTilePos[0];
			}
			else {
				return worldObj;
			}
		}
	}
	
	private Object[] getNearestTile(Program program) {
		if ( getDirection().getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT ) {
			Tile t;
			int[] tilePos;
			int y = program.getWorldObject().getYPosition() + 1;
			for (int x=program.getWorldObject().getXPosition(); x<=program.getWorldObject().getWorld().getXDimension(); x++) {
				t = program.getWorldObject().getWorld().getTileAtPixel(x, y);
				tilePos = new int[] {x,y};
				if (t.getFeature() == 1) {
					return new Object[] {t,tilePos}; 
				}
			}
		} 
		else if (getDirection().getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT) {
			Tile t;
			int[] tilePos;
			int y = program.getWorldObject().getYPosition() + 1;
			for (int x=program.getWorldObject().getXPosition(); x>=0; x--) {
				t = program.getWorldObject().getWorld().getTileAtPixel(x, y);
				tilePos = new int[] {x,y};
				if (t.getFeature() == 1) {
					return new Object[] {t,tilePos}; 
				}
			}
		}
		else if (getDirection().getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.UP) {
			Tile t;
			int[] tilePos;
			int x = program.getWorldObject().getXPosition();
			for (int y=program.getWorldObject().getYPosition(); y<=program.getWorldObject().getWorld().getYDimension(); y++) {
				t = program.getWorldObject().getWorld().getTileAtPixel(x, y);
				tilePos = new int[] {x,y};
				if (t.getFeature() == 1) {
					return new Object[] {t,tilePos}; 
				}
			}
		}
		else {
			Tile t;
			int[] tilePos;
			int x = program.getWorldObject().getXPosition();
			for (int y=program.getWorldObject().getYPosition(); y>=0; y--) {
				t = program.getWorldObject().getWorld().getTileAtPixel(x, y);
				tilePos = new int[] {x,y};
				if (t.getFeature() == 1) {
					return new Object[] {t,tilePos}; 
				}
			}
		}
		return null;
	}
	
	private WorldObject getNearestWorldObject(Program program) {
		double smallestDist = Double.MAX_VALUE;
		WorldObject nearestObj = null;
		
	
		for (WorldObject obj : program.getWorldObject().getWorld().getAllWorldObjects()) {
			if (obj != program.getWorldObject()) {
				if ( getDirection().getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.RIGHT ) {
					double dist = obj.getXPosition() - program.getWorldObject().getXPosition();
					if (dist > 0 && dist < smallestDist) {
						smallestDist = dist;
						nearestObj = obj;
					}
				}
				else if ( getDirection().getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.LEFT ) {
					double dist = program.getWorldObject().getXPosition()- obj.getXPosition();
					if (dist > 0 && dist < smallestDist) {
						smallestDist = dist;
						nearestObj = obj;
					}
				}
				else if ( getDirection().getDirectionValue() == jumpingalien.part3.programs.IProgramFactory.Direction.DOWN ) {
					double dist = program.getWorldObject().getYPosition()- obj.getYPosition();
					if (dist > 0 && dist < smallestDist) {
						smallestDist = dist;
						nearestObj = obj;
					}
				}
				else {
					double dist = obj.getYPosition() - program.getWorldObject().getYPosition();
					if (dist > 0 && dist < smallestDist) {
						smallestDist = dist;
						nearestObj = obj;
					}
				}
			}
		}
		return nearestObj;
	}
}