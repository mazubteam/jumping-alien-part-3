package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Sqrt extending the abstract type UnaryOperator.
 * 
 */

public class Sqrt extends UnaryOperator {

	public Sqrt(Expression operand, SourceLocation location) {
		super(operand, location);
	}

	@Override
	public Expression evaluate(Program program) {
		return new DoubleConstant(Math.sqrt(((NumberExpression) getOperand().evaluate(program)).getValue()),null);
	}

}
