package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Direction extending the abstract type Expression.
 * 
 */
public class Direction extends Expression {

	public Direction(jumpingalien.part3.programs.IProgramFactory.Direction direction, SourceLocation location) {
		super(location);
		this.direction = direction;
	}
	
	private final jumpingalien.part3.programs.IProgramFactory.Direction direction;
	
	public jumpingalien.part3.programs.IProgramFactory.Direction getDirectionValue() {
		return direction;
	}

	@Override
	public Expression evaluate(Program program) {
		return this;
	}
	
	@Override
	public String toString() {
		return this.direction.toString();

	}

}
