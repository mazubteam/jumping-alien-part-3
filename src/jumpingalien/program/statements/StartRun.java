package jumpingalien.program.statements;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
import jumpingalien.program.expressions.Direction;
import jumpingalien.program.expressions.Expression;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of StartRuns extending the abstract type Statement.
 * 
 */

public class StartRun extends Statement {

	public StartRun(SourceLocation loc, Expression direction) {
		super(loc);
		this.direction = direction;
	}

	@Override
	public double executeStatement(Program program, double time) {
		if (! (program.getWorldObject() == null) && getDirection().evaluate(program) instanceof Direction) {
			program.getWorldObject().startRun((Direction) getDirection().evaluate(program));
			this.setFinished(true);
			return time-0.001;
		}
		if (program.getWorldObject() == null) {
		}
		return time;
	}

	private Expression getDirection() {
		return this.direction;
	}
	private final Expression direction;
	
	@Override
	public void reset() {
		setFinished(false);		
	}
	
}
