package jumpingalien.model;

import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import jumpingalien.util.Sprite;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 * 
 * @version 1.0
 * 
 * A class of Slimes extending the abstract type WorldObject.
 * 
 * @invar	...
 * 			| isValidVerticalVelocity(getVerticalVelocity())
 * @invar	...
 * 			| isValidHorizontalVelocity(getHorizontalVelocity())
 * @invar	...
 * 			| isValidVerticalAcceleration(getVerticalAcceleration())
 * @invar	...
 * 			| isValidHorizontalAcceleration(getHorizontalAcceleration())
 */
public class Slime extends WorldObject {
	
	/**	
	 * @param 	sprite
	 * 			...
	 * @effect 	...
	 * 			| setImages(sprite)
	 * @effect 	...
	 * 			| setNbHitPoints(100)
	 * @effect	...
	 * 			| setMaxHorizontalVelocity(2.5)
	 */
	public Slime(Sprite[] sprite) {
		setImages(sprite);
		setNbHitPoints(100);
		setMaxHorizontalVelocity(2.5);
	}
	
	/**
	 * @param 	velocity
	 * 			...
	 * @return 	...	
	 * 			| (velocity <= 2.5) && (velocity > 0)
	 */
	@Override
	protected boolean isValidHorizontalVelocity(double velocity) {
		if (velocity == Double.POSITIVE_INFINITY || velocity == Double.NEGATIVE_INFINITY || velocity == Double.NaN) {
			 return false;
		}
		return (velocity <= 2.5) && (velocity >= 0);
	}
	
	/**
	 * @param 	velocity
	 * 			...
	 * @return 	...	
	 * 			| velocity <= 0.0
	 */
	@Override
	protected boolean isValidVerticalVelocity(double velocity) {
		if (velocity == Double.POSITIVE_INFINITY || velocity == Double.NEGATIVE_INFINITY || velocity == Double.NaN) {
			 return false;
		}
		return velocity <= 0.0;
	}
	
	/**
	 * @param 	acc
	 * 			...
	 * @return 	...	
	 * 			| ((acc == 0.7) || (acc == 0.0))
	 */
	@Override
	protected boolean isValidHorizontalAcceleration(double acc) {
		if (acc == Double.POSITIVE_INFINITY || acc == Double.NEGATIVE_INFINITY || acc == Double.NaN) {
			 return false;
		}
		return ((acc == 0.7) || (acc == 0.0));
	}

	/**
	 * @param 	acc
	 * 			...
	 * @return 	...	
	 * 			| (acc == 0.0 || acc == -10.0)
	 */
	@Override
	protected boolean isValidVerticalAcceleration(double acc) {
		if (acc == Double.POSITIVE_INFINITY || acc == Double.NEGATIVE_INFINITY || acc == Double.NaN) {
			 return false;
		}
		return (acc == 0.0 || acc == -10.0);
	}
	
	/**
	 * @return 	...
	 * 			| orientation[generator.nextInt(orientation.length)];
	 */
	public static Orientation randomOrientation(){
	    Orientation[] orientation = Orientation.values();
	    Random generator = new Random();
	    return orientation[generator.nextInt(orientation.length)];
	}
	
	/**
	 * @effect	...
	 * 			| setOrientation(Orientation.randomOrientation())
	 * @effect	...
	 * 			| setHorizontalAcceleration(0.7)
	 * @effect	...
	 * 			| setHorizontalVelocity(0.0)
	 * @effect 	...
	 * 			| if (getOrientation() == Orientation.LEFT)
	 *			|	then setSpriteIndex(0);
	 * @effect 	If this Mazub is jumping while it starts moving right, its sprite index is set to 4.
	 * 			| if (this.isJumping())
	 * 			| 	then this.setSpriteIndex(0)
	 * 			| else this.setSpriteIndex(1)
	 */
	private void move() {
		Random r = new Random();
		setMovementTime(2.0 + (6.0 - 2.0) * r.nextDouble());
		setOrientation(Orientation.randomOrientation());
		setHorizontalAcceleration(0.7);
		setHorizontalVelocity(0.0);
		if (getOrientation() == Orientation.LEFT) {
			setSpriteIndex(0);
		} else {
			setSpriteIndex(1);
		}		
	}
	
	void moveLeft() {
		setSpriteIndex(0);
		setHorizontalAcceleration(0.7);
		setHorizontalVelocity(0.0);
		Random r = new Random();
		setMovementTime(2.0 + (6.0 - 2.0) * r.nextDouble());
		setOrientation(Orientation.LEFT);
	}
	
	
	void moveRight() {
		setSpriteIndex(1);
		setHorizontalAcceleration(0.7);
		setHorizontalVelocity(0.0);
		Random r = new Random();
		setMovementTime(2.0 + (6.0 - 2.0) * r.nextDouble());
		setOrientation(Orientation.RIGHT);
	}
	
	void stopMove() {
		setHorizontalAcceleration(0.0);
		setHorizontalVelocity(0.0);
	}
	
	/**
	 * @return	...
	 * 			movementTime
	 */
	@Basic
	private double getMovementTime() {
		return movementTime;
	}
	
	/**
	 * @param 	time
	 * 			...
	 * @post 	new.getMovementTime() = time
	 */
	private void setMovementTime(double time)
	{
		movementTime = time;
	}
	private double movementTime;
	
	
	/**
	 * @param	dt
	 * 			...
	 * @effect	...
	 * 			| if ( ! isTerminated()) {
	 *			|	then timer += dt;
	 *			|	if (timer >= movementTime) 
	 *			|		then move();
	 *			|			 timer = 0;
	 * @effect 	...
	 * 			| updateXPosition(dt)
	 * @effect 	...
	 * 			| updateYPositionAndVelocity(dt)
	 * @effect	...
	 * 			| if ((getHorizontalVelocity() + getHorizontalAcceleration() * dt) <= getMaxHorizontalVelocity())
	 * 			| 	then setHorizontalVelocity(getHorizontalVelocity() + getHorizontalAcceleration() * dt)
	 * 			| else
	 * 			| 	setHorizontalVelocity(getMaxHorizontalVelocity())
	 */
	protected void advanceTime(double dt) throws IllegalXPositionException, IllegalYPositionException {
		if ( ! isTerminated()) {
			
			if (! hasProgram() ) {
				setTimer(getTimer() + dt);
				if (getTimer() >= getMovementTime()) {
					move();
					setTimer(0.0);
				}
			}
			
			updateXPosition(dt);
			updateYPositionAndVelocity(dt);
			
			if ( (getHorizontalVelocity() + getHorizontalAcceleration() * dt) <= getMaxHorizontalVelocity() ) {
				setHorizontalVelocity(getHorizontalVelocity() + getHorizontalAcceleration() * dt);
			}
			else if ( (getHorizontalVelocity() + getHorizontalAcceleration() * dt) > getMaxHorizontalVelocity() ) {
				setHorizontalVelocity(getMaxHorizontalVelocity());
			}
		}
	}
	
	/**
	 * @return	...
	 * 			| timer
	 */
	@Basic
	private double getTimer() {
		return timer;
	}
	
	/**
	 * @param 	t
	 * 			...
	 * @post	...
	 * 			| new.getTimer() == t
	 */
	private void setTimer(double t) {
		timer = t;
	}
	private double timer;
		
	
	/**
	 * @param 	deltaT
	 * 			...
	 * @throws 	IllegalXPositionException
	 * 			...
	 * 			| if (! isValidXPosition(getXPositionDouble() + distanceTravelled))
	 * @effect 	...
	 * 			| if ((getOrientation() == Orientation.RIGHT))
	 * 			| 	then setXPosition(getXPositionDouble() + 100 * getHorizDistTravelled(deltaT))
	 * @effect	...
	 * 			| if ((getOrientation() == Orientation.LEFT))
	 * 			| 	then setXPosition(getXPositionDouble() - 100 * getHorizDistTravelled(deltaT))
	 */
	private void updateXPosition(double deltaT) throws IllegalXPositionException {

		if (getOrientation() == Orientation.RIGHT) {
			double distanceTravelled = (100 * getHorizDistTravelled(deltaT));
			if (!isTerminated() && ! featureOverlapsRight(1) ) {
				setXPosition(getXPositionDouble() + distanceTravelled);
			}
		}
		if (getOrientation() == Orientation.LEFT) {
			double distanceTravelled = - 100 * getHorizDistTravelled(deltaT);
			if (!isTerminated() && !featureOverlapsLeft(1) ) {
				setXPosition( getXPositionDouble() + distanceTravelled);
			}
		}
	}
	
	/**
	 * @param 	deltaT
	 * 			...
	 * @throws 	IllegalYPositionException
	 * 			...
	 * 			|  if (! isValidYPosition(getYPositionDouble() + distanceTravelled))
	 * @effect	...
	 * 			| if (!featureOverlapsBottom(1) )
	 * 			|	then	setVerticalAcceleration(-10.0)
	 *			|			setVerticalVelocity(getVerticalVelocity() + getVerticalAcceleration() * deltaT)
	 *			| else setVerticalAcceleration(0.0)
	 *			|	   setVerticalVelocity(0.0)
	 * @effect	...
	 * 			| if (featureOverlapsBottom(1) && getVerticalVelocity() < 0 )
	 * 			|	then	setVerticalVelocity(0.0)
	 * @effect	...
	 * 			| if (!featureOverlapsBottom(1) && Math.round(distanceTravelled) == -1)
	 * 			|   then	setYPosition( getYPositionDouble() + 100 * getVertDistTravelled(deltaT););
	 */
	private void updateYPositionAndVelocity(double deltaT) throws IllegalYPositionException {
		
		if (! featureOverlapsBottom(1)) {
			setVerticalAcceleration(-10.0);
			setVerticalVelocity(getVerticalVelocity() + getVerticalAcceleration() * deltaT);
		}
		else {
			setVerticalAcceleration(0.0);
			setVerticalVelocity(0.0);
		}
		
		if (featureOverlapsBottom(1) && getVerticalVelocity() < 0) {
			setVerticalVelocity(0.0);
		}
	
		double distanceTravelled = 100 * getVertDistTravelled(deltaT);
		if (!featureOverlapsBottom(1) && Math.round(distanceTravelled) == -1) {
			setYPosition( getYPositionDouble() + distanceTravelled);
		}
	}
	
	/**
	 * @param 	deltaT
	 * 			...
	 * @return	...
	 * 			| (getHorizontalVelocity() * deltaT + 0.5 * getHorizontalAcceleration() * Math.pow(deltaT, 2))
	 */
	private double getHorizDistTravelled(double deltaT) { 
		return (getHorizontalVelocity() * deltaT + 0.5 * getHorizontalAcceleration() * Math.pow(deltaT, 2));
	}
	
	/**
	 * @param 	deltaT
	 * 			...
	 * @return	...
	 * 			| (getVerticalVelocity() * deltaT + 0.5 * getVerticalAcceleration() * Math.pow(deltaT,2) )
	 */
	private double getVertDistTravelled(double deltaT) {
		return (getVerticalVelocity() * deltaT + 0.5 * getVerticalAcceleration() * Math.pow(deltaT,2) );
	}
	
	/**
	 * @return	...
	 * 			| maxNbHitPoints
	 */
	@Override
	@Basic
	@Immutable
	protected int getMaxNbHitPoints() {
		return maxNbHitPoints;
	}
	private final static int maxNbHitPoints = 100;

	// binding met School
	/**
	 * @return	...
	 * 			| this.school
	 */
	@Basic
	public School getSchool() {
		return this.school;
	}
	private School school;

	/**
	 * @param 	school
	 * 			...
	 * @pre 	...
	 * 			| canHaveAsSchool(school)
	 * @post	...
	 * 			| new.getSchool() == school
	 */
	@Raw
	public void setSchool(School school) {
		assert canHaveAsSchool(school);
		this.school = school;

	}

	/**
	 * @param 	school
	 * 			...
	 * @return	...
	 * 			| this.getSchool() == school
	 */
	public boolean hasAsSchool(School school) {
		return this.getSchool() == school;
	}

	/**
	 * @param 	school
	 * 			...
	 * @return	...
	 * 			| if (this.isTerminated())
	 *			|	then school == null
	 *			| else 
	 *			|	then school != null
	 * 			
	 */
	@Raw
	public boolean canHaveAsSchool(School school) {
		if (this.isTerminated()) {
			return (school == null);
		}
		else 
			return (school != null);
	}

	/**
	 * @return	...
	 * 			| (this.canHaveAsSchool(this.getSchool()) && this.getSchool().hasAsSlime(this))
	 */
	public boolean hasProperSchool() {
		return (this.canHaveAsSchool(this.getSchool()) && this.getSchool().hasAsSlime(this));
	}

	/**
	 * @param 	school
	 * 			...
	 * @effect	...
	 * 			| if (canHaveAsSchool(school) && school.canHaveAsSlime(this))
	 * 			| 	then getSchool().removeAsSlime(this);
	 *			|		 setSchool(school);
	 *			|		 school.addAsSlime(this);
	 */
	public void changeToSchool(School school) {
		if (canHaveAsSchool(school) && school.canHaveAsSlime(this)) {
			getSchool().removeAsSlime(this);
			setSchool(school);
			school.addAsSlime(this);
		}
	}
}
