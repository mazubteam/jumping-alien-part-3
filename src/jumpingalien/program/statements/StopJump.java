package jumpingalien.program.statements;

import jumpingalien.model.Alien;
import jumpingalien.model.Shark;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of StopJumps extending the abstract type Statement.
 * 
 */
public class StopJump extends Statement {

	public StopJump(SourceLocation loc) {
		super(loc);
	}

	@Override
	public double executeStatement(Program program, double time) {
		if (program.getWorldObject() instanceof Alien ) { 
			try {
				((Alien) program.getWorldObject()).endJump();
			} catch (Exception e) {
				//
			}
		}
		else if (program.getWorldObject() instanceof Shark ) { 
			try {
				((Shark) program.getWorldObject()).endJump();
			} catch (Exception e) {
				//
			}
		}
		setFinished(true);
		return time-0.001;
	}

	@Override
	public void reset() {
		setFinished(false);		
	}

}
