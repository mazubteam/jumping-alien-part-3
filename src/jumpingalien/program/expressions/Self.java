package jumpingalien.program.expressions;

import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Self's extending the abstract type Expression.
 * 
 */

public class Self extends Expression {
	public Self(SourceLocation location) {
		super(location);
	}
	
	@Override
	public WorldObject evaluate(Program program) {
		return program.getWorldObject();
	}
	
	@Override
	public String toString() {
		return "this";
	}
}
