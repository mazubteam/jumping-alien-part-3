package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/** 
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of Divisions extending the abstract class BinaryOperator.
*/
public class Division extends BinaryOperator {

	public Division(Expression left, Expression right, SourceLocation location) {
		super(left, right, location);
	}

	@Override
	public Expression evaluate(Program program) {
		return new DoubleConstant((((NumberExpression) getLeftOperand().evaluate(program)).getValue() / ((NumberExpression) getRightOperand().evaluate(program)).getValue()),null);
	}

}
