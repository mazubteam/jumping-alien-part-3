package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
import jumpingalien.program.types.MyType;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Variable extending the abstract type Expression.
 * 
 */

public class Variable extends Expression {

	public Variable(String name, MyType type, SourceLocation location) {
		super(location);
		this.varName = name;
		this.type = type;
	}
	
	public Object getValue() {
		return this.value;
	}
	private Object value;
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	@Override
	public Object evaluate(Program program) {
		return program.getGlobalVars().get(getVarName()).getValue();
	}
	
	public MyType getType() {
		return type;
	}
	private final MyType type;
	
	public String getVarName() {
		return varName;
	}
	private final String varName;
}
