package jumpingalien.model;

import jumpingalien.util.Sprite;

/**
 * A class of Mazubs extending the type Alien
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 * 
 * @version 3.0
 */
public class Mazub extends Alien {

	public Mazub(Sprite[] sprite) {
		super(sprite);
		this.setNbHitPoints(100);
	}
	
	
}
	
	