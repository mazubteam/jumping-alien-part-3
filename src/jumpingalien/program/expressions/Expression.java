package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Expressions.
 * 
 */
public abstract class Expression {
	
	protected Expression(SourceLocation location) {
		setSourceLocation(location);
	}
	
	protected SourceLocation getSourceLocation() {
		return sourceLocation;
	}
	
	private void setSourceLocation(SourceLocation sourceLocation) {
		this.sourceLocation = sourceLocation;
	}
	private SourceLocation sourceLocation;
	
	public abstract Object evaluate(Program program);
}
