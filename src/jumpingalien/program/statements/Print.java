package jumpingalien.program.statements;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
import jumpingalien.program.expressions.Expression;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Print extending the abstract type Statement.
 * 
 */

public class Print extends Statement {

	public Print(SourceLocation loc, Expression exp) {
		super(loc);
		this.expression = exp;
	}
	
	private Expression getExpression() {
		return expression;
	}
	private final Expression expression;

	@Override
	public double executeStatement(Program program, double time) {
		if (time >= 0.001) {
			System.out.println(getExpression().evaluate(program).toString());
			this.setFinished(true);
			return time-0.001;
		}
		else return time;
	}

	@Override
	public void reset() {
		setFinished(false);		
	}

}
