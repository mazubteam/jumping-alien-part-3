package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of Ors extending the class BinaryOperator.
*/
public class Or extends BinaryOperator {

	public Or(Expression left, Expression right, SourceLocation location) {
		super(left, right, location);
	}

	@Override
	public Expression evaluate(Program program) {
		return new Boolean((((Boolean) getLeftOperand().evaluate(program)).getBooleanValue() || ((Boolean) getRightOperand().evaluate(program)).getBooleanValue()),null);
	}

}
