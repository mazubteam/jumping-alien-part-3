package jumpingalien.program.statements;

import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;
import jumpingalien.program.expressions.Expression;
import jumpingalien.program.expressions.NumberExpression;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Wait extending the abstract type Statement.
 * 
 */

public class Wait extends Statement {

	public Wait(Expression duration, SourceLocation loc) {
		super(loc);		
		this.waitingTime = duration;		
	}

	private Expression getWaitingTime() {
		return waitingTime;
	}
	private final Expression waitingTime;
	
	private double getWaitingTimeDouble() {
		return waitingTimeDouble;
	}

	private void setWaitingTimeDouble(double waitingTimeDouble) {
		this.waitingTimeDouble = waitingTimeDouble;
	}
	private double waitingTimeDouble;
	
	private int getNbOfCycles() {
		return nbOfCycles;
	}

	private void setNbOfCycles(int nb) {
		this.nbOfCycles = nb;
	}
	private int nbOfCycles;
	
	@Override
	public double executeStatement(Program program, double time) {
		if (getNbOfCycles() == 0) {
			setWaitingTimeDouble(((NumberExpression) getWaitingTime().evaluate(program)).getValue());
			setNbOfCycles((int) (getWaitingTimeDouble()/0.001));
		}
		
		if (getWaitingTimeDouble() == 0.0)	{
			setFinished(true);
		}
		
		if (getWaitingTimeDouble()-time < 0.0) {
			setWaitingTimeDouble(0.0);
		}
		else {
			setWaitingTimeDouble(getWaitingTimeDouble()-time);
		}
		return 0.0;
	}

	@Override
	public void reset() {
		setNbOfCycles(0);
		setFinished(false);		
	}
}
