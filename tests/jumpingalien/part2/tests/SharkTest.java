package jumpingalien.part2.tests;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static org.junit.Assert.*;
import jumpingalien.model.Mazub;
import jumpingalien.model.Shark;
import jumpingalien.model.World;
import jumpingalien.part2.facade.Facade;
import jumpingalien.part2.facade.IFacadePart2;
import jumpingalien.util.Sprite;

import org.junit.Before;
import org.junit.Test;


public class SharkTest {

	private Shark testShark;
	
	@Before
	public void setUp() throws Exception {
		IFacadePart2 facade = new Facade();
		
		World world = facade.createWorld(500, 2, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 2);
		facade.setGeologicalFeature(world, 1, 0, 2);
		testShark = facade.createShark(250, 250, spriteArrayForSize(3, 3));
		facade.addShark(world, testShark);
	}

	@Test
	public void constructor() {
		Sprite[] sprites = spriteArrayForSize(3,3);
		
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 2, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 2);
		facade.setGeologicalFeature(world, 1, 0, 2);
		Shark shark = facade.createShark(250, 250, sprites);
		facade.addShark(world, shark);
		
		assertSame(sprites, shark.getImages());
		assertEquals(0, shark.getSpriteIndex());
		assertTrue(shark.getVerticalAcceleration() == 0.0);
		assertTrue(shark.getHorizontalAcceleration() == 0.0);
	}
	
	@Test
	public void getDimensions() {
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 2, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 2);
		facade.setGeologicalFeature(world, 1, 0, 2);
		Sprite[] sprites = spriteArrayForSize(66, 92, 30);
		Shark shark = facade.createShark(250, 250, sprites);

		
		facade.addShark(world, shark);
		assertEquals(66, shark.getWidth());
		assertEquals(92, shark.getHeight());
	}
	
	@Test
	public void advanceObject_divingRising() throws Exception {
		Sprite[] sprites = spriteArrayForSize(3,3);
		IFacadePart2 facade = new Facade();
		World world = facade.createWorld(500, 2, 2, 1, 1, 1, 1);
		facade.setGeologicalFeature(world, 0, 0, 2);
		facade.setGeologicalFeature(world, 1, 0, 2);
		Mazub alien = facade.createMazub(250, 750, spriteArrayForSize(3, 3));
		facade.setMazub(world, alien);
		Shark shark = facade.createShark(250, 250, sprites);
		facade.addShark(world, shark);
	
		for (int i = 0; i < 10; i++) {
			world.advanceTime(0.1);
		}
		assertTrue(Math.round(shark.getHorizontalVelocity() * 2) == 3);
		assertTrue(shark.getHorizontalAcceleration() == 1.5);
		assertTrue(shark.getVerticalAcceleration() >= -0.2);
		assertTrue(shark.getVerticalAcceleration() <= 0.2);
	}

}
