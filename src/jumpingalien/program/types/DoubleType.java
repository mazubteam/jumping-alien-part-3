package jumpingalien.program.types;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of DoubleType extending the abstract type MyType.
 * 
 */

public class DoubleType extends MyType {

	public DoubleType(double d) {
		this.setValue(d);
	}
	
	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	private double value;
	
}
