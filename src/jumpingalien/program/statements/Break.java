package jumpingalien.program.statements;

import jumpingalien.model.BreakException;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of Breaks extending the abstract type Statement.
 * 
 */
public class Break extends Statement {

	public Break(SourceLocation loc) {
		super(loc);
	}

	@Override
	public double executeStatement(Program program, double time) throws BreakException {
		this.setFinished(true);
		throw new BreakException();
	}

	@Override
	public void reset() {
		setFinished(false);
	}
}
