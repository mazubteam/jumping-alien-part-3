package jumpingalien.program.expressions;

import jumpingalien.model.Plant;
import jumpingalien.model.WorldObject;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A class of IsPlants extending the abstract class UnaryOperator.
*/
public class IsPlant extends UnaryOperator {

	public IsPlant(Expression operand, SourceLocation location) {
		super(operand, location);
	}

	@Override
	public Expression evaluate(Program program) {
		return new Boolean(((WorldObject) (getOperand().evaluate(program))) instanceof Plant,null);
	}

}
