package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;

/**
* @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
* @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
*
* @version 1.0
* 
* A abstract class of Operators.
*/
public abstract class Operator extends Expression {
	
	public Operator(SourceLocation location) {
		super(location);
	}
	
	protected abstract int getNbOperands();
	
}
