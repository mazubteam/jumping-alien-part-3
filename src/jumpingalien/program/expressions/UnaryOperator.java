package jumpingalien.program.expressions;

import jumpingalien.part3.programs.SourceLocation;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A abstract class of UnaryOperator extending the abstract type Operator.
 * 
 */

public abstract class UnaryOperator extends Operator {
	
	public UnaryOperator(Expression operand, SourceLocation location) {
		super(location);
		this.operand = operand;
	}

	@Override
	protected int getNbOperands() {
		return 1;
	}
	
	protected Expression getOperand() {
		return operand;
	}

	private final Expression operand;
	
}
