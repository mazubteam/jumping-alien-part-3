package jumpingalien.program.types;


/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of BoolType extending the abstract type MyType.
 * 
 */

public class BoolType extends MyType {

	public BoolType(boolean b) {
		setBool(b);
	}
	
	public boolean getBool() {
		return bool;
	}

	public void setBool(boolean bool) {
		this.bool = bool;
	}

	private boolean bool;
}
