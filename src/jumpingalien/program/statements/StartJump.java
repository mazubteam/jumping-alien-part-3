package jumpingalien.program.statements;

import jumpingalien.model.Buzam;
import jumpingalien.model.Shark;
import jumpingalien.part3.programs.SourceLocation;
import jumpingalien.program.Program;

/**
 * 
 * @author Lemmens Urbaan - Computerwetenschappen & Elektrotechniek
 * @author Schuermans Julien - Computerwetenschappen & Elektrotechniek
 *
 * @version 1.0
 * 
 * A class of StartJump extending the abstract type Statement.
 * 
 */

public class StartJump extends Statement {

	public StartJump(SourceLocation loc) {
		super(loc);
	}
	
	@Override
	public double executeStatement(Program program, double time) {
		if (program.getWorldObject() instanceof Buzam) { 
			try {
				((Buzam) program.getWorldObject()).startJump();
			} catch (Exception e) {
				//
			}
		}
		else if (program.getWorldObject() instanceof Shark) { 
			try {
				((Shark) program.getWorldObject()).startJump();
			} catch (Exception e) {
				//
			}
		}
		
		this.setFinished(true);
		return time-0.001;
	}

	@Override
	public void reset() {
		setFinished(false);		
	}

}
